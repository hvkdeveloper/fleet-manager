<?php
/**
 * The Gallery template to display posts
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

$tantum_template_args = get_query_var( 'tantum_template_args' );
if ( is_array( $tantum_template_args ) ) {
	$tantum_columns    = empty( $tantum_template_args['columns'] ) ? 2 : max( 1, $tantum_template_args['columns'] );
	$tantum_blog_style = array( $tantum_template_args['type'], $tantum_columns );
} else {
	$tantum_blog_style = explode( '_', tantum_get_theme_option( 'blog_style' ) );
	$tantum_columns    = empty( $tantum_blog_style[1] ) ? 2 : max( 1, $tantum_blog_style[1] );
}
$tantum_post_format = get_post_format();
$tantum_post_format = empty( $tantum_post_format ) ? 'standard' : str_replace( 'post-format-', '', $tantum_post_format );
$tantum_image       = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );

?><div class="
<?php
if ( ! empty( $tantum_template_args['slider'] ) ) {
	echo ' slider-slide swiper-slide';
} else {
	echo 'masonry_item masonry_item-1_' . esc_attr( $tantum_columns );
}
?>
"><article id="post-<?php the_ID(); ?>" 
	<?php
	post_class(
		'post_item post_format_' . esc_attr( $tantum_post_format )
		. ' post_layout_portfolio'
		. ' post_layout_portfolio_' . esc_attr( $tantum_columns )
		. ' post_layout_gallery'
		. ' post_layout_gallery_' . esc_attr( $tantum_columns )
	);
	tantum_add_blog_animation( $tantum_template_args );
	?>
	data-size="
		<?php
		if ( ! empty( $tantum_image[1] ) && ! empty( $tantum_image[2] ) ) {
			echo intval( $tantum_image[1] ) . 'x' . intval( $tantum_image[2] );}
		?>
	"
	data-src="
		<?php
		if ( ! empty( $tantum_image[0] ) ) {
			echo esc_url( $tantum_image[0] );}
		?>
	"
>
<?php

	// Sticky label
if ( is_sticky() && ! is_paged() ) {
	?>
		<span class="post_label label_sticky"></span>
		<?php
}

	// Featured image
	$tantum_image_hover = 'icon';  // !empty($tantum_template_args['hover']) && !tantum_is_inherit($tantum_template_args['hover']) ? $tantum_template_args['hover'] : tantum_get_theme_option('image_hover');
if ( in_array( $tantum_image_hover, array( 'icons', 'zoom' ) ) ) {
	$tantum_image_hover = 'dots';
}
$tantum_components = tantum_array_get_keys_by_value( tantum_get_theme_option( 'meta_parts' ) );
tantum_show_post_featured(
	array(
		'hover'         => $tantum_image_hover,
		'no_links'      => ! empty( $tantum_template_args['no_links'] ),
		'thumb_size'    => tantum_get_thumb_size( strpos( tantum_get_theme_option( 'body_style' ), 'full' ) !== false || $tantum_columns < 3 ? 'masonry-big' : 'masonry' ),
		'thumb_only'    => true,
		'show_no_image' => true,
		'post_info'     => '<div class="post_details">'
						. '<h2 class="post_title">'
							. ( empty( $tantum_template_args['no_links'] )
								? '<a href="' . esc_url( get_permalink() ) . '">' . esc_html( get_the_title() ) . '</a>'
								: esc_html( get_the_title() )
								)
						. '</h2>'
						. '<div class="post_description">'
							. ( ! empty( $tantum_components )
								? tantum_show_post_meta(
									apply_filters(
										'tantum_filter_post_meta_args', array(
											'components' => $tantum_components,
											'seo'      => false,
											'echo'     => false,
										), $tantum_blog_style[0], $tantum_columns
									)
								)
								: ''
								)
							. ( empty( $tantum_template_args['hide_excerpt'] )
								? '<div class="post_description_content">' . get_the_excerpt() . '</div>'
								: ''
								)
							. ( empty( $tantum_template_args['no_links'] )
								? '<a href="' . esc_url( get_permalink() ) . '" class="theme_button post_readmore"><span class="post_readmore_label">' . esc_html__( 'Learn more', 'tantum' ) . '</span></a>'
								: ''
								)
						. '</div>'
					. '</div>',
	)
);
?>
</article></div><?php
// Need opening PHP-tag above, because <article> is a inline-block element (used as column)!
