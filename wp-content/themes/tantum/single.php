<?php
/**
 * The template to display single post
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

get_header();

while ( have_posts() ) {
	the_post();

	// Prepare theme-specific vars:

	// Full post loading
	$full_post_loading        = tantum_get_value_gp( 'action' ) == 'full_post_loading';

	// Prev post loading
	$prev_post_loading        = tantum_get_value_gp( 'action' ) == 'prev_post_loading';

	// Position of the related posts
	$tantum_related_position = tantum_get_theme_option( 'related_position' );

	// Type of the prev/next posts navigation
	$tantum_posts_navigation = tantum_get_theme_option( 'posts_navigation' );
	$tantum_prev_post        = false;

	if ( 'scroll' == $tantum_posts_navigation ) {
		$tantum_prev_post = get_previous_post( true );         // Get post from same category
		if ( ! $tantum_prev_post ) {
			$tantum_prev_post = get_previous_post( false );    // Get post from any category
			if ( ! $tantum_prev_post ) {
				$tantum_posts_navigation = 'links';
			}
		}
	}

	// Override some theme options to display featured image, title and post meta in the dynamic loaded posts
	if ( $full_post_loading || ( $prev_post_loading && $tantum_prev_post ) ) {
		tantum_storage_set_array( 'options_meta', 'post_thumbnail_type', 'default' );
		if ( tantum_get_theme_option( 'post_header_position' ) != 'below' ) {
			tantum_storage_set_array( 'options_meta', 'post_header_position', 'above' );
		}
		tantum_sc_layouts_showed( 'featured', false );
		tantum_sc_layouts_showed( 'title', false );
		tantum_sc_layouts_showed( 'postmeta', false );
	}

	// If related posts should be inside the content
	if ( strpos( $tantum_related_position, 'inside' ) === 0 ) {
		ob_start();
	}

	// Display post's content
	get_template_part( apply_filters( 'tantum_filter_get_template_part', 'content', get_post_format() ), get_post_format() );

	// If related posts should be inside the content
	if ( strpos( $tantum_related_position, 'inside' ) === 0 ) {
		$tantum_content = ob_get_contents();
		ob_end_clean();

		ob_start();
		do_action( 'tantum_action_related_posts' );
		$tantum_related_content = ob_get_contents();
		ob_end_clean();

		$tantum_related_position_inside = max( 0, min( 9, tantum_get_theme_option( 'related_position_inside' ) ) );
		if ( 0 == $tantum_related_position_inside ) {
			$tantum_related_position_inside = mt_rand( 1, 9 );
		}
		
		$tantum_p_number = 0;
		$tantum_related_inserted = false;
		for ( $i = 0; $i < strlen( $tantum_content ) - 3; $i++ ) {
			if ( $tantum_content[ $i ] == '<' && $tantum_content[ $i + 1 ] == 'p' && in_array( $tantum_content[ $i + 2 ], array( '>', ' ' ) ) ) {
				$tantum_p_number++;
				if ( $tantum_related_position_inside == $tantum_p_number ) {
					$tantum_related_inserted = true;
					$tantum_content = ( $i > 0 ? substr( $tantum_content, 0, $i ) : '' )
										. $tantum_related_content
										. substr( $tantum_content, $i );
				}
			}
		}
		if ( ! $tantum_related_inserted ) {
			$tantum_content .= $tantum_related_content;
		}

		tantum_show_layout( $tantum_content );
	}

	// Author bio
	if ( tantum_get_theme_option( 'show_author_info' ) == 1
		&& ! is_attachment()
		&& get_the_author_meta( 'description' )
		&& ( 'scroll' != $tantum_posts_navigation || tantum_get_theme_option( 'posts_navigation_scroll_hide_author' ) == 0 )
		&& ( ! $full_post_loading || tantum_get_theme_option( 'open_full_post_hide_author' ) == 0 )
	) {
		do_action( 'tantum_action_before_post_author' );
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/author-bio' ) );
		do_action( 'tantum_action_after_post_author' );
	}

	// Previous/next post navigation.
	if ( 'links' == $tantum_posts_navigation && ! $full_post_loading ) {
		do_action( 'tantum_action_before_post_navigation' );
		?>
		<div class="nav-links-single<?php
			if ( ! tantum_is_off( tantum_get_theme_option( 'posts_navigation_fixed' ) ) ) {
				echo ' nav-links-fixed fixed';
			}
		?>">
			<?php
			the_post_navigation(
				array(
					'next_text' => '<span class="nav-arrow"></span>'
						. '<span class="screen-reader-text">' . esc_html__( 'Next post:', 'tantum' ) . '</span> '
						. '<h6 class="post-title">%title</h6>'
						. '<span class="post_date">%date</span>',
					'prev_text' => '<span class="nav-arrow"></span>'
						. '<span class="screen-reader-text">' . esc_html__( 'Previous post:', 'tantum' ) . '</span> '
						. '<h6 class="post-title">%title</h6>'
						. '<span class="post_date">%date</span>',
				)
			);
			?>
		</div>
		<?php
		do_action( 'tantum_action_after_post_navigation' );
	}

	// Related posts
    $post_type = get_post_type();
	if ( 'below_content' == $tantum_related_position
		&& ( 'scroll' != $tantum_posts_navigation || tantum_get_theme_option( 'posts_navigation_scroll_hide_related' ) == 0 )
		&& ( ! $full_post_loading || tantum_get_theme_option( 'open_full_post_hide_related' ) == 0 )
        && ( $post_type != 'car_rental_item')
	) {
		do_action( 'tantum_action_related_posts' );
	}

	// If comments are open or we have at least one comment, load up the comment template.
	$tantum_comments_number = get_comments_number();
	if ( comments_open() || $tantum_comments_number > 0 ) {
		if ( tantum_get_value_gp( 'show_comments' ) == 1 || ( ! $full_post_loading && ( 'scroll' != $tantum_posts_navigation || tantum_get_theme_option( 'posts_navigation_scroll_hide_comments' ) == 0 || tantum_check_url( '#comment' ) ) ) ) {
			do_action( 'tantum_action_before_comments' );
			comments_template();
			do_action( 'tantum_action_after_comments' );
		} else {
			?>
			<div class="show_comments_single">
				<a href="<?php echo esc_url( add_query_arg( array( 'show_comments' => 1 ), get_comments_link() ) ); ?>" class="theme_button show_comments_button">
					<?php
					if ( $tantum_comments_number > 0 ) {
						echo esc_html( sprintf( _n( 'Show comment', 'Show comments ( %d )', $tantum_comments_number, 'tantum' ), $tantum_comments_number ) );
					} else {
						esc_html_e( 'Leave a comment', 'tantum' );
					}
					?>
				</a>
			</div>
			<?php
		}
	}

	if ( 'scroll' == $tantum_posts_navigation && ! $full_post_loading ) {
		?>
		<div class="nav-links-single-scroll"
			data-post-id="<?php echo esc_attr( get_the_ID( $tantum_prev_post ) ); ?>"
			data-post-link="<?php echo esc_attr( get_permalink( $tantum_prev_post ) ); ?>"
			data-post-title="<?php the_title_attribute( array( 'post' => $tantum_prev_post ) ); ?>">
		</div>
		<?php
	}
}

get_footer();
