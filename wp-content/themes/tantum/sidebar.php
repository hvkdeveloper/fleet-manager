<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

if ( tantum_sidebar_present() ) {
	ob_start();
	$tantum_sidebar_name = tantum_get_theme_option( 'sidebar_widgets' );
	tantum_storage_set( 'current_sidebar', 'sidebar' );
	if ( is_active_sidebar( $tantum_sidebar_name ) ) {
		dynamic_sidebar( $tantum_sidebar_name );
	}
	$tantum_out = trim( ob_get_contents() );
	ob_end_clean();
	if ( ! empty( $tantum_out ) ) {
		$tantum_sidebar_position    = tantum_get_theme_option( 'sidebar_position' );
		$tantum_sidebar_position_ss = tantum_get_theme_option( 'sidebar_position_ss' );
		?>
		<div class="sidebar widget_area
			<?php
			echo ' ' . esc_attr( $tantum_sidebar_position );
			echo ' sidebar_' . esc_attr( $tantum_sidebar_position_ss );

			if ( 'float' == $tantum_sidebar_position_ss ) {
				echo ' sidebar_float';
			}
			$tantum_sidebar_scheme = tantum_get_theme_option( 'sidebar_scheme' );
			if ( ! empty( $tantum_sidebar_scheme ) && ! tantum_is_inherit( $tantum_sidebar_scheme ) ) {
				echo ' scheme_' . esc_attr( $tantum_sidebar_scheme );
			}
			?>
		" role="complementary">
			<?php
			// Skip link anchor to fast access to the sidebar from keyboard
			?>
			<a id="sidebar_skip_link_anchor" class="tantum_skip_link_anchor" href="#"></a>
			<?php
			// Single posts banner before sidebar
			tantum_show_post_banner( 'sidebar' );
			// Button to show/hide sidebar on mobile
			if ( in_array( $tantum_sidebar_position_ss, array( 'above', 'float' ) ) ) {
				$tantum_title = apply_filters( 'tantum_filter_sidebar_control_title', 'float' == $tantum_sidebar_position_ss ? __( 'Show Sidebar', 'tantum' ) : '' );
				$tantum_text  = apply_filters( 'tantum_filter_sidebar_control_text', 'above' == $tantum_sidebar_position_ss ? __( 'Show Sidebar', 'tantum' ) : '' );
				?>
				<a href="#" class="sidebar_control" title="<?php echo esc_html( $tantum_title ); ?>"><?php echo esc_html( $tantum_text ); ?></a>
				<?php
			}
			?>
			<div class="sidebar_inner">
				<?php
				do_action( 'tantum_action_before_sidebar' );
				tantum_show_layout( preg_replace( "/<\/aside>[\r\n\s]*<aside/", '</aside><aside', $tantum_out ) );
				do_action( 'tantum_action_after_sidebar' );
				?>
			</div><!-- /.sidebar_inner -->
		</div><!-- /.sidebar -->
		<div class="clearfix"></div>
		<?php
	}
}
