<?php
/**
 * Theme storage manipulations
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) {
	exit; }

// Get theme variable
if ( ! function_exists( 'tantum_storage_get' ) ) {
	function tantum_storage_get( $var_name, $default = '' ) {
		global $TANTUM_STORAGE;
		return isset( $TANTUM_STORAGE[ $var_name ] ) ? $TANTUM_STORAGE[ $var_name ] : $default;
	}
}

// Set theme variable
if ( ! function_exists( 'tantum_storage_set' ) ) {
	function tantum_storage_set( $var_name, $value ) {
		global $TANTUM_STORAGE;
		$TANTUM_STORAGE[ $var_name ] = $value;
	}
}

// Check if theme variable is empty
if ( ! function_exists( 'tantum_storage_empty' ) ) {
	function tantum_storage_empty( $var_name, $key = '', $key2 = '' ) {
		global $TANTUM_STORAGE;
		if ( ! empty( $key ) && ! empty( $key2 ) ) {
			return empty( $TANTUM_STORAGE[ $var_name ][ $key ][ $key2 ] );
		} elseif ( ! empty( $key ) ) {
			return empty( $TANTUM_STORAGE[ $var_name ][ $key ] );
		} else {
			return empty( $TANTUM_STORAGE[ $var_name ] );
		}
	}
}

// Check if theme variable is set
if ( ! function_exists( 'tantum_storage_isset' ) ) {
	function tantum_storage_isset( $var_name, $key = '', $key2 = '' ) {
		global $TANTUM_STORAGE;
		if ( ! empty( $key ) && ! empty( $key2 ) ) {
			return isset( $TANTUM_STORAGE[ $var_name ][ $key ][ $key2 ] );
		} elseif ( ! empty( $key ) ) {
			return isset( $TANTUM_STORAGE[ $var_name ][ $key ] );
		} else {
			return isset( $TANTUM_STORAGE[ $var_name ] );
		}
	}
}

// Inc/Dec theme variable with specified value
if ( ! function_exists( 'tantum_storage_inc' ) ) {
	function tantum_storage_inc( $var_name, $value = 1 ) {
		global $TANTUM_STORAGE;
		if ( empty( $TANTUM_STORAGE[ $var_name ] ) ) {
			$TANTUM_STORAGE[ $var_name ] = 0;
		}
		$TANTUM_STORAGE[ $var_name ] += $value;
	}
}

// Concatenate theme variable with specified value
if ( ! function_exists( 'tantum_storage_concat' ) ) {
	function tantum_storage_concat( $var_name, $value ) {
		global $TANTUM_STORAGE;
		if ( empty( $TANTUM_STORAGE[ $var_name ] ) ) {
			$TANTUM_STORAGE[ $var_name ] = '';
		}
		$TANTUM_STORAGE[ $var_name ] .= $value;
	}
}

// Get array (one or two dim) element
if ( ! function_exists( 'tantum_storage_get_array' ) ) {
	function tantum_storage_get_array( $var_name, $key, $key2 = '', $default = '' ) {
		global $TANTUM_STORAGE;
		if ( empty( $key2 ) ) {
			return ! empty( $var_name ) && ! empty( $key ) && isset( $TANTUM_STORAGE[ $var_name ][ $key ] ) ? $TANTUM_STORAGE[ $var_name ][ $key ] : $default;
		} else {
			return ! empty( $var_name ) && ! empty( $key ) && isset( $TANTUM_STORAGE[ $var_name ][ $key ][ $key2 ] ) ? $TANTUM_STORAGE[ $var_name ][ $key ][ $key2 ] : $default;
		}
	}
}

// Set array element
if ( ! function_exists( 'tantum_storage_set_array' ) ) {
	function tantum_storage_set_array( $var_name, $key, $value ) {
		global $TANTUM_STORAGE;
		if ( ! isset( $TANTUM_STORAGE[ $var_name ] ) ) {
			$TANTUM_STORAGE[ $var_name ] = array();
		}
		if ( '' === $key ) {
			$TANTUM_STORAGE[ $var_name ][] = $value;
		} else {
			$TANTUM_STORAGE[ $var_name ][ $key ] = $value;
		}
	}
}

// Set two-dim array element
if ( ! function_exists( 'tantum_storage_set_array2' ) ) {
	function tantum_storage_set_array2( $var_name, $key, $key2, $value ) {
		global $TANTUM_STORAGE;
		if ( ! isset( $TANTUM_STORAGE[ $var_name ] ) ) {
			$TANTUM_STORAGE[ $var_name ] = array();
		}
		if ( ! isset( $TANTUM_STORAGE[ $var_name ][ $key ] ) ) {
			$TANTUM_STORAGE[ $var_name ][ $key ] = array();
		}
		if ( '' === $key2 ) {
			$TANTUM_STORAGE[ $var_name ][ $key ][] = $value;
		} else {
			$TANTUM_STORAGE[ $var_name ][ $key ][ $key2 ] = $value;
		}
	}
}

// Merge array elements
if ( ! function_exists( 'tantum_storage_merge_array' ) ) {
	function tantum_storage_merge_array( $var_name, $key, $value ) {
		global $TANTUM_STORAGE;
		if ( ! isset( $TANTUM_STORAGE[ $var_name ] ) ) {
			$TANTUM_STORAGE[ $var_name ] = array();
		}
		if ( '' === $key ) {
			$TANTUM_STORAGE[ $var_name ] = array_merge( $TANTUM_STORAGE[ $var_name ], $value );
		} else {
			$TANTUM_STORAGE[ $var_name ][ $key ] = array_merge( $TANTUM_STORAGE[ $var_name ][ $key ], $value );
		}
	}
}

// Add array element after the key
if ( ! function_exists( 'tantum_storage_set_array_after' ) ) {
	function tantum_storage_set_array_after( $var_name, $after, $key, $value = '' ) {
		global $TANTUM_STORAGE;
		if ( ! isset( $TANTUM_STORAGE[ $var_name ] ) ) {
			$TANTUM_STORAGE[ $var_name ] = array();
		}
		if ( is_array( $key ) ) {
			tantum_array_insert_after( $TANTUM_STORAGE[ $var_name ], $after, $key );
		} else {
			tantum_array_insert_after( $TANTUM_STORAGE[ $var_name ], $after, array( $key => $value ) );
		}
	}
}

// Add array element before the key
if ( ! function_exists( 'tantum_storage_set_array_before' ) ) {
	function tantum_storage_set_array_before( $var_name, $before, $key, $value = '' ) {
		global $TANTUM_STORAGE;
		if ( ! isset( $TANTUM_STORAGE[ $var_name ] ) ) {
			$TANTUM_STORAGE[ $var_name ] = array();
		}
		if ( is_array( $key ) ) {
			tantum_array_insert_before( $TANTUM_STORAGE[ $var_name ], $before, $key );
		} else {
			tantum_array_insert_before( $TANTUM_STORAGE[ $var_name ], $before, array( $key => $value ) );
		}
	}
}

// Push element into array
if ( ! function_exists( 'tantum_storage_push_array' ) ) {
	function tantum_storage_push_array( $var_name, $key, $value ) {
		global $TANTUM_STORAGE;
		if ( ! isset( $TANTUM_STORAGE[ $var_name ] ) ) {
			$TANTUM_STORAGE[ $var_name ] = array();
		}
		if ( '' === $key ) {
			array_push( $TANTUM_STORAGE[ $var_name ], $value );
		} else {
			if ( ! isset( $TANTUM_STORAGE[ $var_name ][ $key ] ) ) {
				$TANTUM_STORAGE[ $var_name ][ $key ] = array();
			}
			array_push( $TANTUM_STORAGE[ $var_name ][ $key ], $value );
		}
	}
}

// Pop element from array
if ( ! function_exists( 'tantum_storage_pop_array' ) ) {
	function tantum_storage_pop_array( $var_name, $key = '', $defa = '' ) {
		global $TANTUM_STORAGE;
		$rez = $defa;
		if ( '' === $key ) {
			if ( isset( $TANTUM_STORAGE[ $var_name ] ) && is_array( $TANTUM_STORAGE[ $var_name ] ) && count( $TANTUM_STORAGE[ $var_name ] ) > 0 ) {
				$rez = array_pop( $TANTUM_STORAGE[ $var_name ] );
			}
		} else {
			if ( isset( $TANTUM_STORAGE[ $var_name ][ $key ] ) && is_array( $TANTUM_STORAGE[ $var_name ][ $key ] ) && count( $TANTUM_STORAGE[ $var_name ][ $key ] ) > 0 ) {
				$rez = array_pop( $TANTUM_STORAGE[ $var_name ][ $key ] );
			}
		}
		return $rez;
	}
}

// Inc/Dec array element with specified value
if ( ! function_exists( 'tantum_storage_inc_array' ) ) {
	function tantum_storage_inc_array( $var_name, $key, $value = 1 ) {
		global $TANTUM_STORAGE;
		if ( ! isset( $TANTUM_STORAGE[ $var_name ] ) ) {
			$TANTUM_STORAGE[ $var_name ] = array();
		}
		if ( empty( $TANTUM_STORAGE[ $var_name ][ $key ] ) ) {
			$TANTUM_STORAGE[ $var_name ][ $key ] = 0;
		}
		$TANTUM_STORAGE[ $var_name ][ $key ] += $value;
	}
}

// Concatenate array element with specified value
if ( ! function_exists( 'tantum_storage_concat_array' ) ) {
	function tantum_storage_concat_array( $var_name, $key, $value ) {
		global $TANTUM_STORAGE;
		if ( ! isset( $TANTUM_STORAGE[ $var_name ] ) ) {
			$TANTUM_STORAGE[ $var_name ] = array();
		}
		if ( empty( $TANTUM_STORAGE[ $var_name ][ $key ] ) ) {
			$TANTUM_STORAGE[ $var_name ][ $key ] = '';
		}
		$TANTUM_STORAGE[ $var_name ][ $key ] .= $value;
	}
}

// Call object's method
if ( ! function_exists( 'tantum_storage_call_obj_method' ) ) {
	function tantum_storage_call_obj_method( $var_name, $method, $param = null ) {
		global $TANTUM_STORAGE;
		if ( null === $param ) {
			return ! empty( $var_name ) && ! empty( $method ) && isset( $TANTUM_STORAGE[ $var_name ] ) ? $TANTUM_STORAGE[ $var_name ]->$method() : '';
		} else {
			return ! empty( $var_name ) && ! empty( $method ) && isset( $TANTUM_STORAGE[ $var_name ] ) ? $TANTUM_STORAGE[ $var_name ]->$method( $param ) : '';
		}
	}
}

// Get object's property
if ( ! function_exists( 'tantum_storage_get_obj_property' ) ) {
	function tantum_storage_get_obj_property( $var_name, $prop, $default = '' ) {
		global $TANTUM_STORAGE;
		return ! empty( $var_name ) && ! empty( $prop ) && isset( $TANTUM_STORAGE[ $var_name ]->$prop ) ? $TANTUM_STORAGE[ $var_name ]->$prop : $default;
	}
}
