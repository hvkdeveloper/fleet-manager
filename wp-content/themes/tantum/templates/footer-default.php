<?php
/**
 * The template to display default site footer
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.10
 */

?>
<footer class="footer_wrap footer_default
<?php
$tantum_footer_scheme = tantum_get_theme_option( 'footer_scheme' );
if ( ! empty( $tantum_footer_scheme ) && ! tantum_is_inherit( $tantum_footer_scheme  ) ) {
	echo ' scheme_' . esc_attr( $tantum_footer_scheme );
}
?>
				">
	<?php

	// Footer widgets area
	get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/footer-widgets' ) );

	// Logo
	get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/footer-logo' ) );

	// Socials
	get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/footer-socials' ) );

	// Menu
	get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/footer-menu' ) );

	// Copyright area
	get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/footer-copyright' ) );

	?>
</footer><!-- /.footer_wrap -->
