<?php
/**
 * The template to display default site footer
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.10
 */

$tantum_footer_id = tantum_get_custom_footer_id();
$tantum_footer_meta = get_post_meta( $tantum_footer_id, 'trx_addons_options', true );
if ( ! empty( $tantum_footer_meta['margin'] ) ) {
	tantum_add_inline_css( sprintf( '.page_content_wrap{padding-bottom:%s}', esc_attr( tantum_prepare_css_value( $tantum_footer_meta['margin'] ) ) ) );
}
?>
<footer class="footer_wrap footer_custom footer_custom_<?php echo esc_attr( $tantum_footer_id ); ?> footer_custom_<?php echo esc_attr( sanitize_title( get_the_title( $tantum_footer_id ) ) ); ?>
						<?php
						$tantum_footer_scheme = tantum_get_theme_option( 'footer_scheme' );
						if ( ! empty( $tantum_footer_scheme ) && ! tantum_is_inherit( $tantum_footer_scheme  ) ) {
							echo ' scheme_' . esc_attr( $tantum_footer_scheme );
						}
						?>
						">
	<?php
	// Custom footer's layout
	do_action( 'tantum_action_show_layout', $tantum_footer_id );
	?>
</footer><!-- /.footer_wrap -->
