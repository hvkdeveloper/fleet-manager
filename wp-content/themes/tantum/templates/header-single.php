<?php
/**
 * The template to display the featured image in the single post
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

if ( get_query_var( 'tantum_header_image' ) == '' && tantum_trx_addons_featured_image_override( is_singular() && has_post_thumbnail() && in_array( get_post_type(), array( 'post', 'page' ) ) ) ) {
	$tantum_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
	if ( ! empty( $tantum_src[0] ) ) {
		tantum_sc_layouts_showed( 'featured', true );
		?>
		<div class="sc_layouts_featured with_image without_content <?php echo esc_attr( tantum_add_inline_css_class( 'background-image:url(' . esc_url( $tantum_src[0] ) . ');' ) ); ?>"></div>
		<?php
	}
}
