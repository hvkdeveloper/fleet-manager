<?php
/**
 * The template to display Admin notices
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.1
 */

$tantum_theme_obj = wp_get_theme();
?>
<div class="tantum_admin_notice tantum_welcome_notice update-nag">
	<?php
	// Theme image
	$tantum_theme_img = tantum_get_file_url( 'screenshot.jpg' );
	if ( '' != $tantum_theme_img ) {
		?>
		<div class="tantum_notice_image"><img src="<?php echo esc_url( $tantum_theme_img ); ?>" alt="<?php esc_attr_e( 'Theme screenshot', 'tantum' ); ?>"></div>
		<?php
	}

	// Title
	?>
	<h3 class="tantum_notice_title">
		<?php
		echo esc_html(
			sprintf(
				// Translators: Add theme name and version to the 'Welcome' message
				__( 'Welcome to %1$s v.%2$s', 'tantum' ),
				$tantum_theme_obj->name . ( TANTUM_THEME_FREE ? ' ' . __( 'Free', 'tantum' ) : '' ),
				$tantum_theme_obj->version
			)
		);
		?>
	</h3>
	<?php

	// Description
	?>
	<div class="tantum_notice_text">
		<p class="tantum_notice_text_description">
			<?php
			echo str_replace( '. ', '.<br>', wp_kses_data( $tantum_theme_obj->description ) );
			?>
		</p>
		<p class="tantum_notice_text_info">
			<?php
			echo wp_kses_data( __( 'Attention! Plugin "ThemeREX Addons" is required! Please, install and activate it!', 'tantum' ) );
			?>
		</p>
	</div>
	<?php

	// Buttons
	?>
	<div class="tantum_notice_buttons">
		<?php
		// Link to the page 'About Theme'
		?>
		<a href="<?php echo esc_url( admin_url() . 'themes.php?page=tantum_about' ); ?>" class="button button-primary"><i class="dashicons dashicons-nametag"></i> 
			<?php
			echo esc_html__( 'Install plugin "ThemeREX Addons"', 'tantum' );
			?>
		</a>
		<?php		
		// Dismiss this notice
		?>
		<a href="#" class="tantum_hide_notice"><i class="dashicons dashicons-dismiss"></i> <span class="tantum_hide_notice_text"><?php esc_html_e( 'Dismiss', 'tantum' ); ?></span></a>
	</div>
</div>
