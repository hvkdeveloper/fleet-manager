<?php
/**
 * The template to display custom header from the ThemeREX Addons Layouts
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.06
 */

$tantum_header_css   = '';
$tantum_header_image = get_header_image();
$tantum_header_video = tantum_get_header_video();
if ( ! empty( $tantum_header_image ) && tantum_trx_addons_featured_image_override( is_singular() || tantum_storage_isset( 'blog_archive' ) || is_category() ) ) {
	$tantum_header_image = tantum_get_current_mode_image( $tantum_header_image );
}

$tantum_header_id = tantum_get_custom_header_id();
$tantum_header_meta = get_post_meta( $tantum_header_id, 'trx_addons_options', true );
if ( ! empty( $tantum_header_meta['margin'] ) ) {
	tantum_add_inline_css( sprintf( '.page_content_wrap{padding-top:%s}', esc_attr( tantum_prepare_css_value( $tantum_header_meta['margin'] ) ) ) );
}

?><header class="top_panel top_panel_custom top_panel_custom_<?php echo esc_attr( $tantum_header_id ); ?> top_panel_custom_<?php echo esc_attr( sanitize_title( get_the_title( $tantum_header_id ) ) ); ?>
				<?php
				echo ! empty( $tantum_header_image ) || ! empty( $tantum_header_video )
					? ' with_bg_image'
					: ' without_bg_image';
				if ( '' != $tantum_header_video ) {
					echo ' with_bg_video';
				}
				if ( '' != $tantum_header_image ) {
					echo ' ' . esc_attr( tantum_add_inline_css_class( 'background-image: url(' . esc_url( $tantum_header_image ) . ');' ) );
				}
				if ( is_single() && has_post_thumbnail() ) {
					echo ' with_featured_image';
				}
				if ( tantum_is_on( tantum_get_theme_option( 'header_fullheight' ) ) ) {
					echo ' header_fullheight tantum-full-height';
				}
				$tantum_header_scheme = tantum_get_theme_option( 'header_scheme' );
				if ( ! empty( $tantum_header_scheme ) && ! tantum_is_inherit( $tantum_header_scheme  ) ) {
					echo ' scheme_' . esc_attr( $tantum_header_scheme );
				}
				?>
">
	<?php

	// Background video
	if ( ! empty( $tantum_header_video ) ) {
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/header-video' ) );
	}

	// Custom header's layout
	do_action( 'tantum_action_show_layout', $tantum_header_id );

	// Header widgets area
	get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/header-widgets' ) );

	?>
</header>
