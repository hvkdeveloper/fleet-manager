<?php
/**
 * The template to display the copyright info in the footer
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.10
 */

// Copyright area
?> 
<div class="footer_copyright_wrap
<?php
$tantum_copyright_scheme = tantum_get_theme_option( 'copyright_scheme' );
if ( ! empty( $tantum_copyright_scheme ) && ! tantum_is_inherit( $tantum_copyright_scheme  ) ) {
	echo ' scheme_' . esc_attr( $tantum_copyright_scheme );
}
?>
				">
	<div class="footer_copyright_inner">
		<div class="content_wrap">
			<div class="copyright_text">
			<?php
				$tantum_copyright = tantum_get_theme_option( 'copyright' );
			if ( ! empty( $tantum_copyright ) ) {
				// Replace {{Y}} or {Y} with the current year
				$tantum_copyright = str_replace( array( '{{Y}}', '{Y}' ), date( 'Y' ), $tantum_copyright );
				// Replace {{...}} and ((...)) on the <i>...</i> and <b>...</b>
				$tantum_copyright = tantum_prepare_macros( $tantum_copyright );
				// Display copyright
				echo wp_kses_post( nl2br( $tantum_copyright ) );
			}
			?>
			</div>
		</div>
	</div>
</div>
