<?php
/**
 * The template 'Style 5' to displaying related posts
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.54
 */

$tantum_template_args = get_query_var( 'tantum_template_args' );
$tantum_link        = get_permalink();
$tantum_post_format = get_post_format();
$tantum_post_format = empty( $tantum_post_format ) ? 'standard' : str_replace( 'post-format-', '', $tantum_post_format );
?><div id="post-<?php the_ID(); ?>" <?php post_class( 'related_item post_format_' . esc_attr( $tantum_post_format ) ); ?>>
	<?php
	tantum_show_post_featured(
		array(
			'thumb_size'    => apply_filters( 'tantum_filter_related_thumb_size', tantum_get_thumb_size( (int) tantum_get_theme_option( 'related_posts' ) == 1 ? 'big' : 'recent' ) ),
		)
	);
	?>
	<div class="post_header entry-header">
        <?php
        if ( in_array( get_post_type(), array( 'post', 'attachment' ) ) ) {
            ?>
            <div class="post_meta">
                <a href="<?php echo esc_url( $tantum_link ); ?>" class="post_meta_item post_date"><?php echo wp_kses_data( tantum_get_date() ); ?></a>
            </div>
            <?php
        }
        ?>
		<h6 class="post_title entry-title"><a href="<?php echo esc_url( $tantum_link ); ?>"><?php the_title(); ?></a></h6>

	</div>

    <div class="related_more_link">
        <?php
        // More button
        if ( empty( $tantum_template_args['no_links'] )  ) {
            tantum_show_post_more_link( $tantum_template_args, '<p>', '</p>' );
        }
        ?>
    </div>
</div>
