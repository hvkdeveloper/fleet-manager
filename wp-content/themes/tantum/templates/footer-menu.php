<?php
/**
 * The template to display menu in the footer
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.10
 */

// Footer menu
$tantum_menu_footer = tantum_get_nav_menu( 'menu_footer' );
if ( ! empty( $tantum_menu_footer ) ) {
	?>
	<div class="footer_menu_wrap">
		<div class="footer_menu_inner">
			<?php
			tantum_show_layout(
				$tantum_menu_footer,
				'<nav class="menu_footer_nav_area sc_layouts_menu sc_layouts_menu_default"'
					. ' itemscope itemtype="//schema.org/SiteNavigationElement"'
					. '>',
				'</nav>'
			);
			?>
		</div>
	</div>
	<?php
}
