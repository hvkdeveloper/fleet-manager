<?php
/**
 * The template to display the Author bio
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */
?>

<?php
    $post_type = get_post_type();
    if( $post_type != 'car_rental_item') {
?>
<div class="author_info author vcard" itemprop="author" itemscope itemtype="//schema.org/Person">

	<div class="author_avatar" itemprop="image">
		<?php
		$tantum_mult = tantum_get_retina_multiplier();
		echo get_avatar( get_the_author_meta( 'user_email' ), 120 * $tantum_mult );
		?>
	</div><!-- .author_avatar -->

	<div class="author_description">
		<h5 class="author_title" itemprop="name">
		<?php
			// Translators: Add the author's name in the <span>
			echo sprintf( __( 'About Author %s', 'tantum' ), '<span class="fn">' . get_the_author() . '</span>' ) ;
		?>
		</h5>

		<div class="author_bio" itemprop="description">
			<?php echo wp_kses_post( wpautop( get_the_author_meta( 'description' ) ) ); ?>
			<?php do_action( 'tantum_action_user_meta' ); ?>
		</div><!-- .author_bio -->

	</div><!-- .author_description -->

</div><!-- .author_info -->
<?php
}
?>
