<?php
/**
 * The template to display the widgets area in the footer
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.10
 */

// Footer sidebar
$tantum_footer_name    = tantum_get_theme_option( 'footer_widgets' );
$tantum_footer_present = ! tantum_is_off( $tantum_footer_name ) && is_active_sidebar( $tantum_footer_name );
if ( $tantum_footer_present ) {
	tantum_storage_set( 'current_sidebar', 'footer' );
	$tantum_footer_wide = tantum_get_theme_option( 'footer_wide' );
	ob_start();
	if ( is_active_sidebar( $tantum_footer_name ) ) {
		dynamic_sidebar( $tantum_footer_name );
	}
	$tantum_out = trim( ob_get_contents() );
	ob_end_clean();
	if ( ! empty( $tantum_out ) ) {
		$tantum_out          = preg_replace( "/<\\/aside>[\r\n\s]*<aside/", '</aside><aside', $tantum_out );
		$tantum_need_columns = true;   //or check: strpos($tantum_out, 'columns_wrap')===false;
		if ( $tantum_need_columns ) {
			$tantum_columns = max( 0, (int) tantum_get_theme_option( 'footer_columns' ) );			
			if ( 0 == $tantum_columns ) {
				$tantum_columns = min( 4, max( 1, tantum_tags_count( $tantum_out, 'aside' ) ) );
			}
			if ( $tantum_columns > 1 ) {
				$tantum_out = preg_replace( '/<aside([^>]*)class="widget/', '<aside$1class="column-1_' . esc_attr( $tantum_columns ) . ' widget', $tantum_out );
			} else {
				$tantum_need_columns = false;
			}
		}
		?>
		<div class="footer_widgets_wrap widget_area<?php echo ! empty( $tantum_footer_wide ) ? ' footer_fullwidth' : ''; ?> sc_layouts_row sc_layouts_row_type_normal">
			<div class="footer_widgets_inner widget_area_inner">
				<?php
				if ( ! $tantum_footer_wide ) {
					?>
					<div class="content_wrap">
					<?php
				}
				if ( $tantum_need_columns ) {
					?>
					<div class="columns_wrap">
					<?php
				}
				do_action( 'tantum_action_before_sidebar' );
				tantum_show_layout( $tantum_out );
				do_action( 'tantum_action_after_sidebar' );
				if ( $tantum_need_columns ) {
					?>
					</div><!-- /.columns_wrap -->
					<?php
				}
				if ( ! $tantum_footer_wide ) {
					?>
					</div><!-- /.content_wrap -->
					<?php
				}
				?>
			</div><!-- /.footer_widgets_inner -->
		</div><!-- /.footer_widgets_wrap -->
		<?php
	}
}
