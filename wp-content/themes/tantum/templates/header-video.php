<?php
/**
 * The template to display the background video in the header
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.14
 */
$tantum_header_video = tantum_get_header_video();
$tantum_embed_video  = '';
if ( ! empty( $tantum_header_video ) && ! tantum_is_from_uploads( $tantum_header_video ) ) {
	if ( tantum_is_youtube_url( $tantum_header_video ) && preg_match( '/[=\/]([^=\/]*)$/', $tantum_header_video, $matches ) && ! empty( $matches[1] ) ) {
		?><div id="background_video" data-youtube-code="<?php echo esc_attr( $matches[1] ); ?>"></div>
		<?php
	} else {
		?>
		<div id="background_video"><?php tantum_show_layout( tantum_get_embed_video( $tantum_header_video ) ); ?></div>
		<?php
	}
}
