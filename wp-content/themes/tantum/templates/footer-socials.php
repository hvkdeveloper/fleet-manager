<?php
/**
 * The template to display the socials in the footer
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.10
 */


// Socials
if ( tantum_is_on( tantum_get_theme_option( 'socials_in_footer' ) ) ) {
	$tantum_output = tantum_get_socials_links();
	if ( '' != $tantum_output ) {
		?>
		<div class="footer_socials_wrap socials_wrap">
			<div class="footer_socials_inner">
				<?php tantum_show_layout( $tantum_output ); ?>
			</div>
		</div>
		<?php
	}
}
