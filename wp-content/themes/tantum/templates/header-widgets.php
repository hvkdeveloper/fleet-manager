<?php
/**
 * The template to display the widgets area in the header
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

// Header sidebar
$tantum_header_name    = tantum_get_theme_option( 'header_widgets' );
$tantum_header_present = ! tantum_is_off( $tantum_header_name ) && is_active_sidebar( $tantum_header_name );
if ( $tantum_header_present ) {
	tantum_storage_set( 'current_sidebar', 'header' );
	$tantum_header_wide = tantum_get_theme_option( 'header_wide' );
	ob_start();
	if ( is_active_sidebar( $tantum_header_name ) ) {
		dynamic_sidebar( $tantum_header_name );
	}
	$tantum_widgets_output = ob_get_contents();
	ob_end_clean();
	if ( ! empty( $tantum_widgets_output ) ) {
		$tantum_widgets_output = preg_replace( "/<\/aside>[\r\n\s]*<aside/", '</aside><aside', $tantum_widgets_output );
		$tantum_need_columns   = strpos( $tantum_widgets_output, 'columns_wrap' ) === false;
		if ( $tantum_need_columns ) {
			$tantum_columns = max( 0, (int) tantum_get_theme_option( 'header_columns' ) );
			if ( 0 == $tantum_columns ) {
				$tantum_columns = min( 6, max( 1, tantum_tags_count( $tantum_widgets_output, 'aside' ) ) );
			}
			if ( $tantum_columns > 1 ) {
				$tantum_widgets_output = preg_replace( '/<aside([^>]*)class="widget/', '<aside$1class="column-1_' . esc_attr( $tantum_columns ) . ' widget', $tantum_widgets_output );
			} else {
				$tantum_need_columns = false;
			}
		}
		?>
		<div class="header_widgets_wrap widget_area<?php echo ! empty( $tantum_header_wide ) ? ' header_fullwidth' : ' header_boxed'; ?>">
			<div class="header_widgets_inner widget_area_inner">
				<?php
				if ( ! $tantum_header_wide ) {
					?>
					<div class="content_wrap">
					<?php
				}
				if ( $tantum_need_columns ) {
					?>
					<div class="columns_wrap">
					<?php
				}
				do_action( 'tantum_action_before_sidebar' );
				tantum_show_layout( $tantum_widgets_output );
				do_action( 'tantum_action_after_sidebar' );
				if ( $tantum_need_columns ) {
					?>
					</div>	<!-- /.columns_wrap -->
					<?php
				}
				if ( ! $tantum_header_wide ) {
					?>
					</div>	<!-- /.content_wrap -->
					<?php
				}
				?>
			</div>	<!-- /.header_widgets_inner -->
		</div>	<!-- /.header_widgets_wrap -->
		<?php
	}
}
