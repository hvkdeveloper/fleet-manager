<?php
/**
 * The template to display default site header
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

$tantum_header_css   = '';
$tantum_header_image = get_header_image();
$tantum_header_video = tantum_get_header_video();
if ( ! empty( $tantum_header_image ) && tantum_trx_addons_featured_image_override( is_singular() || tantum_storage_isset( 'blog_archive' ) || is_category() ) ) {
	$tantum_header_image = tantum_get_current_mode_image( $tantum_header_image );
}

?><header class="top_panel top_panel_default
	<?php
	echo ! empty( $tantum_header_image ) || ! empty( $tantum_header_video ) ? ' with_bg_image' : ' without_bg_image';
	if ( '' != $tantum_header_video ) {
		echo ' with_bg_video';
	}
	if ( '' != $tantum_header_image ) {
		echo ' ' . esc_attr( tantum_add_inline_css_class( 'background-image: url(' . esc_url( $tantum_header_image ) . ');' ) );
	}
	if ( is_single() && has_post_thumbnail() ) {
		echo ' with_featured_image';
	}
	if ( tantum_is_on( tantum_get_theme_option( 'header_fullheight' ) ) ) {
		echo ' header_fullheight tantum-full-height';
	}
	$tantum_header_scheme = tantum_get_theme_option( 'header_scheme' );
	if ( ! empty( $tantum_header_scheme ) && ! tantum_is_inherit( $tantum_header_scheme  ) ) {
		echo ' scheme_' . esc_attr( $tantum_header_scheme );
	}
	?>
">
	<?php

	// Background video
	if ( ! empty( $tantum_header_video ) ) {
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/header-video' ) );
	}

	// Main menu
	if ( tantum_get_theme_option( 'menu_style' ) == 'top' ) {
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/header-navi' ) );
	}

	// Mobile header
	if ( tantum_is_on( tantum_get_theme_option( 'header_mobile_enabled' ) ) ) {
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/header-mobile' ) );
	}

	if ( !is_single() || ( tantum_get_theme_option( 'post_header_position' ) == 'under' && tantum_get_theme_option( 'post_thumbnail_type' ) == 'default' ) ) {
		// Page title and breadcrumbs area
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/header-title' ) );

		// Display featured image in the header on the single posts
		// Comment next line to prevent show featured image in the header area
		// and display it in the post's content
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/header-single' ) );
	}

	// Header widgets area
	get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/header-widgets' ) );
	?>
</header>
