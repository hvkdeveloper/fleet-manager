<?php
/**
 * The template to display the site logo in the footer
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.10
 */

// Logo
if ( tantum_is_on( tantum_get_theme_option( 'logo_in_footer' ) ) ) {
	$tantum_logo_image = tantum_get_logo_image( 'footer' );
	$tantum_logo_text  = get_bloginfo( 'name' );
	if ( ! empty( $tantum_logo_image['logo'] ) || ! empty( $tantum_logo_text ) ) {
		?>
		<div class="footer_logo_wrap">
			<div class="footer_logo_inner">
				<?php
				if ( ! empty( $tantum_logo_image['logo'] ) ) {
					$tantum_attr = tantum_getimagesize( $tantum_logo_image['logo'] );
					echo '<a href="' . esc_url( home_url( '/' ) ) . '">'
							. '<img src="' . esc_url( $tantum_logo_image['logo'] ) . '"'
								. ( ! empty( $tantum_logo_image['logo_retina'] ) ? ' srcset="' . esc_url( $tantum_logo_image['logo_retina'] ) . ' 2x"' : '' )
								. ' class="logo_footer_image"'
								. ' alt="' . esc_attr__( 'Site logo', 'tantum' ) . '"'
								. ( ! empty( $tantum_attr[3] ) ? ' ' . wp_kses_data( $tantum_attr[3] ) : '' )
							. '>'
						. '</a>';
				} elseif ( ! empty( $tantum_logo_text ) ) {
					echo '<h1 class="logo_footer_text">'
							. '<a href="' . esc_url( home_url( '/' ) ) . '">'
								. esc_html( $tantum_logo_text )
							. '</a>'
						. '</h1>';
				}
				?>
			</div>
		</div>
		<?php
	}
}
