<?php
// Add skin-specific colors and fonts to the custom CSS
if ( ! function_exists( 'tantum_skin_get_css' ) ) {
	add_filter( 'tantum_filter_get_css', 'tantum_skin_get_css', 10, 2 );
	function tantum_skin_get_css( $css, $args ) {

		if ( isset( $css['fonts'] ) && isset( $args['fonts'] ) ) {
			$fonts         = $args['fonts'];
			$css['fonts'] .= <<<CSS


    
    .copyright-text,
    .socials_wrap .social_item .social_name,
    .widget_calendar td#prev a,
    .widget_calendar td#next a,
    .widget_calendar caption,
    .widget_calendar th,
    .widget_area .post_item .post_info, aside .post_item .post_info,
    .widget_area .post_item .post_categories, aside .post_item .post_categories,
    .widget_recent_posts .post_item.with_thumb .post_thumb:before,
    .widget li, 
    .trx_addons_dropcap,
    .sc_table table tr:first-child th, .sc_table table tr:first-child td,
    div .minimal-light .esg-filterbutton,
    div .minimal-light .esg-navigationbutton,
    .minimal-light .esg-sortbutton,
    .minimal-light .esg-cartbutton a, 
    div .car-rental-search-step1 .booking-item .booking-item-body input[type="submit"],
    div .car-rental-slider .responsive-items-slider div.car-rental-item-price,
    .car-rental-slider .responsive-items-slider div.car-rental-item-prefix,
    .car-rental-body .car-rental-item-info .car-rental-item-info-label,
    div .car-rental-slider .responsive-items-slider div.car-rental-item-title a,
    div .car-rental-slider .responsive-items-slider div.car-rental-item-details .car-rental-item-type,
    .car-rental-body .car-rental-item-info .car-rental-item-info-persons span, 
    .car-rental-body .car-rental-item-info .car-rental-item-info-mileage span, 
    .car-rental-body .car-rental-item-info .car-rental-item-info-doors span,
    .single-car_rental_item .car-rental-single-booking .booking-item .booking-item-body input[type="submit"]{
        {$fonts['h5_font-family']}
    }
    .sc_icons.sc_icons_extra .sc_icons_item .sc_icons_item_details .sc_icons_item_description{
         {$fonts['h5_font-family']}
    }
    .font-size .sc_icons.sc_icons_extra .sc_icons_item .sc_icons_item_details .sc_icons_item_description{
       {$fonts['p_font-family']}
    }

CSS;
		}

		if ( isset( $css['vars'] ) && isset( $args['vars'] ) ) {
			$vars         = $args['vars'];
			$css['vars'] .= <<<CSS

CSS;
		}

		if ( isset( $css['colors'] ) && isset( $args['colors'] ) ) {
			$colors         = $args['colors'];
			$css['colors'] .= <<<CSS



    input[type="submit"],
    input[type="reset"],
    input[type="button"],
    .comments_wrap .form-submit input[type="submit"]{
        color: {$colors['inverse_text']};
        background-color: {$colors['text_link']};
        border-color: {$colors['text_link']};
    }    
    input[type="submit"]:hover,
    input[type="reset"]:hover,
    input[type="button"]:hover,
    .comments_wrap .form-submit input[type="submit"]:hover{
        color: {$colors['alter_bd_color']};
        background-color: {$colors['text_hover']};
        border-color: {$colors['text_hover']};
    }   
    .wpcf7 input[type="submit"]:hover,
    .wpcf7 input[type="reset"]:hover,
    .wpcf7 input[type="button"]:hover,
    .comments_wrap .comments_form_wrap .form-submit input[type="submit"]:hover{
        color: {$colors['alter_bd_color']};
        background-color: {$colors['text_hover']};
        border-color: {$colors['text_hover']};
    }
    .wpmtst-submit input[type="submit"]:hover, 
    .wpmtst-submit input[type="submit"]:focus{
        color: {$colors['bg_color']};
        background-color: {$colors['text_hover']};
        border-color: {$colors['text_hover']};
    }

    .car-rental-wrapper  table > tbody > tr.location-headers > td,
    .car-rental-wrapper  table > tbody > tr.item-headers > td,
    .car-rental-wrapper  table > tbody > tr.duration-headers > td,
    .car-rental-wrapper  table > tbody > tr.total-headers > td {
        background-color: {$colors['alter_link2']};
    }
     .car-rental-wrapper  table > tbody > tr  td+td{
         border-color: transparent;
     }
    table > tbody > tr:nth-child(2n) > td {
        background-color: {$colors['alter_link2']};
    }
    table > tbody > tr:nth-child(2n+1) > td {
        background-color: {$colors['alter_hover2']};
    } 
    table td, table th + td, table td + td {
	    color: {$colors['text']};
	}
    
    /* Buttons hover */
    form button:not(.components-button):hover,
    form button:not(.components-button):focus,
    input[type="submit"]:hover,
    input[type="submit"]:focus,
    input[type="reset"]:hover,
    input[type="reset"]:focus,
    input[type="button"]:hover,
    input[type="button"]:focus,
    .post_item .more-link:hover,
    .comments_wrap .form-submit input[type="submit"]:hover,
    .comments_wrap .form-submit input[type="submit"]:focus,
    .wp-block-button:not(.is-style-outline) > .wp-block-button__link:hover,
    .wp-block-button:not(.is-style-outline) > .wp-block-button__link:focus,
    /* BB & Buddy Press */
    #buddypress .comment-reply-link:hover,
    #buddypress .comment-reply-link:focus,
    #buddypress .generic-button a:hover,
    #buddypress .generic-button a:focus,
    #buddypress a.button:hover,
    #buddypress a.button:focus,
    #buddypress button:hover,
    #buddypress button:focus,
    #buddypress input[type="button"]:hover,
    #buddypress input[type="button"]:focus,
    #buddypress input[type="reset"]:hover,
    #buddypress input[type="reset"]:focus,
    #buddypress input[type="submit"]:hover,
    #buddypress input[type="submit"]:focus,
    #buddypress ul.button-nav li a:hover,
    #buddypress ul.button-nav li a:focus,
    a.bp-title-button:hover,
    a.bp-title-button:focus,
    /* Booked */
    .booked-calendar-wrap .booked-appt-list .timeslot .timeslot-people button:hover,
    .booked-calendar-wrap .booked-appt-list .timeslot .timeslot-people button:focus,
    #booked-profile-page .booked-profile-appt-list .appt-block .booked-cal-buttons .google-cal-button > a:hover,
    #booked-profile-page .booked-profile-appt-list .appt-block .booked-cal-buttons .google-cal-button > a:focus,
    #booked-profile-page input[type="submit"]:hover,
    #booked-profile-page input[type="submit"]:focus,
    #booked-profile-page button:hover,
    #booked-profile-page button:focus,
    .booked-list-view input[type="submit"]:hover,
    .booked-list-view input[type="submit"]:focus,
    .booked-list-view button:hover,
    .booked-list-view button:focus,
    table.booked-calendar input[type="submit"]:hover,
    table.booked-calendar input[type="submit"]:focus,
    table.booked-calendar button:hover,
    table.booked-calendar button:focus,
    .booked-modal input[type="submit"]:hover,
    .booked-modal input[type="submit"]:focus,
    .booked-modal button:hover,
    .booked-modal button:focus,
    /* ThemeREX Addons */
    .sc_button_default:hover,
    .sc_button_default:focus,
    .sc_button:not(.sc_button_simple):not(.sc_button_bordered):not(.sc_button_bg_image):hover,
    .sc_button:not(.sc_button_simple):not(.sc_button_bordered):not(.sc_button_bg_image):focus,
    .socials_share:not(.socials_type_drop) .social_icon:hover,
    .socials_share:not(.socials_type_drop) .social_icon:focus,
    /* Tour Master */
    .tourmaster-tour-search-wrap input.tourmaster-tour-search-submit[type="submit"]:hover,
    .tourmaster-tour-search-wrap input.tourmaster-tour-search-submit[type="submit"]:focus,
    /* Tribe Events */
    #tribe-bar-form .tribe-bar-submit input[type="submit"]:hover,
    #tribe-bar-form .tribe-bar-submit input[type="submit"]:focus,
    #tribe-bar-form.tribe-bar-mini .tribe-bar-submit input[type="submit"]:hover,
    #tribe-bar-form.tribe-bar-mini .tribe-bar-submit input[type="submit"]:focus,
    #tribe-bar-form .tribe-bar-views-toggle:hover,
    #tribe-bar-form .tribe-bar-views-toggle:focus,
    #tribe-bar-views li.tribe-bar-views-option:hover,
    #tribe-bar-views li.tribe-bar-views-option:focus,
    #tribe-bar-views .tribe-bar-views-list .tribe-bar-views-option.tribe-bar-active,
    #tribe-bar-views .tribe-bar-views-list .tribe-bar-views-option.tribe-bar-active:hover,
    #tribe-bar-views .tribe-bar-views-list .tribe-bar-views-option.tribe-bar-active:focus,
    #tribe-events .tribe-events-button:hover,
    #tribe-events .tribe-events-button:focus,
    .tribe-events-button:hover,
    .tribe-events-button:focus,
    .tribe-events-cal-links a:hover,
    .tribe-events-cal-links a:focus,
    .tribe-events-sub-nav li a:hover,
    .tribe-events-sub-nav li a:focus,
    /* EDD buttons */
    .edd_download_purchase_form .button:hover, .edd_download_purchase_form .button:active, .edd_download_purchase_form .button:focus,
    #edd-purchase-button:hover, #edd-purchase-button:active, #edd-purchase-button:focus,
    .edd-submit.button:hover, .edd-submit.button:active, .edd-submit.button:focus,
    .widget_edd_cart_widget .edd_checkout a:hover,
    .widget_edd_cart_widget .edd_checkout a:focus,
    .sc_edd_details .downloads_page_tags .downloads_page_data > a:hover,
    .sc_edd_details .downloads_page_tags .downloads_page_data > a:focus,
    /* MailChimp */
    .mc4wp-form input[type="submit"]:hover,
    .mc4wp-form input[type="submit"]:focus {
        color: {$colors['bg_color']};
        background-color: {$colors['text_hover']};
        border-color: {$colors['text_hover']};
    }
    .sidebar_inner .widget{
        background-color: {$colors['extra_bg_hover']};
    }
    .sidebar_inner .widget.widget_search {
        background-color: {$colors['extra_bg_color']};
         
    }
    .sidebar_inner .widget_search input[type="search"] {
        border-color: {$colors['inverse_bd_color']};
        background-color: {$colors['inverse_bd_color']};
        color: {$colors['extra_hover']};
        
    }
    .sidebar .widget_calendar td#prev a:before,
    .sidebar .widget_calendar td#next a:before{
        background-color: {$colors['extra_bg_hover']};
    }
   
    .sidebar_inner .widget_search input[placeholder]::-webkit-input-placeholder	{ color: {$colors['extra_hover']}; }
   
    .sidebar_inner .widget_search input[placeholder]::-moz-placeholder				{ color: {$colors['extra_hover']}; }

    .sidebar_inner .widget_search input[placeholder]:-ms-input-placeholder			{ color: {$colors['extra_hover']}; }

    .sidebar_inner .widget_search input[placeholder]::placeholder					{ color: {$colors['extra_hover']}; }
    
    
    .sidebar_inner .widget_search input[type="search"]:focus,
    .sidebar_inner .widget_search input[type="search"]:active {
        border-color: {$colors['input_bd_hover']};
    }

     .sidebar_inner .widget_search .widget_title {
        color: {$colors['inverse_text']};
     }

    .widget_recent_posts .post_item.with_thumb .post_thumb:before {
        color: {$colors['bg_color']};
        background-color: {$colors['text_hover']};
    }
    
    .widget_recent_comments ul li:before{
        color: {$colors['bg_color']};
        background-color: {$colors['text_link']};
    }

    .sc_item_title_style_accent .sc_item_title_text:before,
    .sc_item_title_style_accent .sc_item_title_text:after {
        background-color: {$colors['bd_color']};
    }
    .sc_title_accent .sc_item_descr {
        color: {$colors['text_light']};
    }
    .sc_title_accent .sc_button_simple:hover,
    .sc_title_accent .sc_button_simple {
        color: {$colors['text_dark']};
    }


    .footer_wrap .sc_socials_icons_names .social_item .social_icon,
    .scheme_self.footer_wrap .sc_socials_icons_names .social_item .social_icon,
    .footer_wrap .sc_socials_icons_names .social_item,
    .scheme_self.footer_wrap .sc_socials_icons_names .social_item {
        color: {$colors['text']};
    }
    .footer_wrap .sc_socials_icons_names .social_item:hover .social_icon,
    .scheme_self.footer_wrap .sc_socials_icons_names .social_item:hover .social_icon,
    .footer_wrap .sc_socials_icons_names .social_item:hover,
    .scheme_self.footer_wrap .sc_socials_icons_names .social_item:hover {
        color: {$colors['text_dark']};
    }    
    .footer_wrap .sc_socials_icons_names .social_item + .social_item,
    .scheme_self.footer_wrap .sc_socials_icons_names .social_item + .social_item {
        border-color: {$colors['bd_color']};
    }
    

    .copyright-text a:hover,
    .copyright-text a {
        color: {$colors['text_dark']};
    }

    .post_meta .post_meta_item:after,
    .post_meta .post_meta_item.post_edit:after,
    .post_meta .vc_inline-link:after {
        border-color: {$colors['text']};
    }    
    .sticky .post_meta .post_meta_item:after,
    .sticky .post_meta .post_meta_item.post_edit:after,
    .sticky .post_meta .vc_inline-link:after {
        border-color: {$colors['inverse_text']};
    }
    .sticky.post_item .more-link{
        color: {$colors['bg_color']};
        background-color: {$colors['text_hover']};
    }   
     .sticky.post_item .more-link:hover{
        color: {$colors['text_hover']};
        background-color: {$colors['bg_color']};
    }

    .post_meta .post_meta_item.post_categories a {
        color: {$colors['inverse_link']};
        background-color: {$colors['text_link']};
    }
    .post_meta .post_meta_item.post_categories a:hover {
        color: {$colors['bg_color']};
        background-color: {$colors['text_hover']};
    }
    .post_item.post_layout_excerpt{
        background-color: {$colors['extra_bg_hover']};
    } 
    
    .trx_addons_tooltip:before{
        background-color: {$colors['alter_hover']}!important;
    }
     .trx_addons_tooltip:after{
        border-top-color:{$colors['alter_hover']};
     }
    
    section>blockquote, div:not(.is-style-solid-color)>blockquote, figure:not(.is-style-solid-color)>blockquote{
         background-color: {$colors['alter_link']};
    }
    blockquote:not(.has-text-color):before{
         color: {$colors['inverse_text']};
    }
    
    .trx_addons_dropcap_style_1{
        background-color: {$colors['extra_bg_color']};
        color: {$colors['extra_text']};
    }
     .trx_addons_dropcap_style_2{
         color: {$colors['text_dark']};
     }
    figure figcaption,
    .wp-caption .wp-caption-text,
    .wp-caption .wp-caption-dd,
    .wp-caption-overlay .wp-caption .wp-caption-text,
    .wp-caption-overlay .wp-caption .wp-caption-dd{
        color: {$colors['inverse_text']}!important;
        background: {$colors['extra_bg_color']}!important;  
   }
    input[type="radio"] + label:before, 
    input[type="checkbox"] + label:before, 
    input[type="radio"] + .wpcf7-list-item-label:before, 
    input[type="checkbox"] + .wpcf7-list-item-label:before, 
    .wpcf7-list-item-label.wpcf7-list-item-right:before, 
    .edd_price_options ul > li > label > input[type="radio"] + span:before, 
    .edd_price_options ul > li > label > input[type="checkbox"] + span:before{
        border-color: {$colors['text_dark']};
    }
    .trx_addons_message_box_success, div.wpcf7-mail-sent-ok{
         border-color: {$colors['text_link']}!important;
    }
    
    .trx_addons_field_error, .wpcf7-not-valid, div.wpcf7-validation-errors, div.wpcf7-acceptance-missing{
        border-color: {$colors['text_hover']}!important;
    }
    span.wpcf7-not-valid-tip, .strong-form span.error, .strong-form label.error{
         color: {$colors['text_hover']};
    }
    .elementor-progress-percentage{
        color: {$colors['text']};
    }
    div .mejs-button button:after{
        background: {$colors['text_link']};   
    }
    div .mejs-button button:before{
       color: {$colors['bg_color']};   
    }   
     div .mejs-button button:hover:before{
       color: {$colors['text_hover']};   
    }
    .elementor-toggle .elementor-tab-title a{
         color: {$colors['text_dark']}; 
    }   
    .elementor-toggle .elementor-tab-title a:hover{
         color: {$colors['text_hover']}; 
    }
    .elementor-toggle .elementor-tab-title .elementor-toggle-icon{
        border-color: {$colors['text_hover']}
    }
    .elementor-toggle .elementor-tab-content{
        border-color: {$colors['bd_color']}
    }
    div .minimal-light .esg-filterbutton, div .minimal-light .esg-navigationbutton, .minimal-light .esg-sortbutton, .minimal-light .esg-cartbutton a{
        color: {$colors['inverse_text']};
        background-color: {$colors['text_link']};
        border-color: {$colors['text_link']}
    }
    .minimal-light .esg-navigationbutton:hover, .minimal-light .esg-filterbutton:hover, .minimal-light .esg-sortbutton:hover, .minimal-light .esg-sortbutton-order:hover, .minimal-light .esg-cartbutton a:hover, .minimal-light .esg-filterbutton.selected{
        background-color: {$colors['text_hover']};
        border-color: {$colors['text_hover']}
    }
    .car-rental-search-step1 .booking-item .booking-item-body input[name="pickup_date"], .car-rental-search-step1 .booking-item .booking-item-body input[name="return_date"],
    div .car-rental-single-booking .booking-item .booking-item-body input[name="pickup_date"], div .car-rental-single-booking .booking-item .booking-item-body input[name="return_date"]{
        background-color: {$colors['input_bd_color']};
        border-color:{$colors['input_bd_color']};
    }
    .car-rental-search-step1 div.styled-select-dropdown{
        border-color:{$colors['input_bd_color']}!important;
    }
    div .car-rental-search-step1 .booking-item .booking-item-body input[type="submit"],
    div .car-rental-search-step1 .booking-item .booking-item-body input[type="submit"], div .car-rental-single-booking .booking-item .booking-item-body input[type="submit"]{
        color: {$colors['inverse_text']};
        background-color: {$colors['text_link']};
        border-color: {$colors['text_link']}
    }
    div .car-rental-search-step1 .booking-item .booking-item-body input[type="submit"]:hover,
    div .car-rental-search-step1 .booking-item .booking-item-body input[type="submit"]:hover, div .car-rental-single-booking .booking-item .booking-item-body input[type="submit"]:hover{
        color: {$colors['alter_bd_color']};
        background-color: {$colors['text_hover']};
        border-color: {$colors['text_hover']}
    }	
    div .car-rental-search-step1 .booking-item .booking-item-body div.styled-select-dropdown select:hover{
        background-color: {$colors['input_bg_hover']}!important;
    }
    .car-rental-body .car-rental-item-price-wrap{
        background-color: {$colors['extra_bg_color']};
    }
    .car-rental-slider .responsive-items-slider div.car-rental-item-prefix{
        color: {$colors['inverse_text']};
    }
    .car-rental-slider .responsive-items-slider div.car-rental-item-details{
         background-color: {$colors['bg_color']};
    }
    .car-rental-wrapper .highlight{
        color: {$colors['text_dark']};
    }
    .car-rental-items-list .description-item .fa, .car-rental-search-result .description-item .fa, .car-rental-single-item .description-item .fa{
         color: {$colors['text_link']};
    }
    ul.car-rental-item-features-list li::before{
        color: {$colors['text_hover']};
    }
    .car-rental-single-item .item-features .item-features-label{
         color: {$colors['text_dark']};
          border-color: {$colors['input_bd_color']}
    }
    ul.car-rental-item-features-list li, .car-rental-list-single-item .description-item{
        border-color: {$colors['input_bd_color']}
    }
    .booking-data-group, .car-rental-search-result .list-headers,
    .car-rental-items-list div.item-type-label,
    .car-rental-options .list-headers{
        background-color: {$colors['inverse_hover']};
    }
    .car-rental-search-result div.item-type-label{
         background-color: {$colors['alter_bg_hover']};
         color: {$colors['text_dark']};
    }
    .booking-data-group-items{
        background-color: {$colors['alter_bg_hover']};
    }
    .car-rental-booking-failure .booking-failure-title{
         color: {$colors['text_dark']}!important;
    }
    .booking-data-text strong, .car-rental-search-result .car-rental-list-item .col3, .car-rental-search-result .car-rental-list-item .col4{
         color: {$colors['text_dark']};
    }
    .booking-data-icon .fa{
        color: {$colors['text_link']};
    }
    .car-rental-search-result .car-rental-list-item .col2 > a{
       color: {$colors['text_dark']};  
    }    
    .car-rental-search-result .car-rental-list-item .col2 > a:hover{
       color: {$colors['text_link']};  
    }
    
    .car-rental-search-result button[name="car_rental_do_search0"], .car-rental-search-result input[name="car_rental_do_search0"], .car-rental-search-result button[name="car_rental_do_search"], .car-rental-search-result input[name="car_rental_do_search"], .car-rental-search-result button[name="car_rental_do_search2"], .car-rental-search-result input[name="car_rental_do_search2"], .car-rental-search-result button[name="car_rental_do_search3"], .car-rental-search-result input[name="car_rental_do_search3"], .car-rental-search-result button[name="car_rental_cancel_booking"], .car-rental-search-result input[name="car_rental_cancel_booking"], .car-rental-options button[name="car_rental_do_search0"], .car-rental-options input[name="car_rental_do_search0"], .car-rental-options button[name="car_rental_do_search"], .car-rental-options input[name="car_rental_do_search"], .car-rental-options button[name="car_rental_do_search2"], .car-rental-options input[name="car_rental_do_search2"], .car-rental-options button[name="car_rental_do_search3"], .car-rental-options input[name="car_rental_do_search3"], .car-rental-options button[name="car_rental_cancel_booking"], .car-rental-options input[name="car_rental_cancel_booking"], .car-rental-booking-details button[name="customer_lookup"], .car-rental-booking-details input[name="customer_lookup"], .car-rental-booking-details button[name="car_rental_do_search4"], .car-rental-booking-details input[name="car_rental_do_search4"]{
        color: {$colors['inverse_text']};
        background-color: {$colors['text_link']};
        border-color: {$colors['text_link']}
    }
    .car-rental-search-result button[name="car_rental_do_search0"]:hover, .car-rental-search-result input[name="car_rental_do_search0"]:hover, .car-rental-search-result button[name="car_rental_do_search"]:hover, .car-rental-search-result input[name="car_rental_do_search"]:hover, .car-rental-search-result button[name="car_rental_do_search2"]:hover, .car-rental-search-result input[name="car_rental_do_search2"]:hover, .car-rental-search-result button[name="car_rental_do_search3"]:hover, .car-rental-search-result input[name="car_rental_do_search3"]:hover, .car-rental-search-result button[name="car_rental_cancel_booking"]:hover, .car-rental-search-result input[name="car_rental_cancel_booking"]:hover, .car-rental-options button[name="car_rental_do_search0"]:hover, .car-rental-options input[name="car_rental_do_search0"]:hover, .car-rental-options button[name="car_rental_do_search"]:hover, .car-rental-options input[name="car_rental_do_search"]:hover, .car-rental-options button[name="car_rental_do_search2"]:hover, .car-rental-options input[name="car_rental_do_search2"]:hover, .car-rental-options button[name="car_rental_do_search3"]:hover, .car-rental-options input[name="car_rental_do_search3"]:hover, .car-rental-options button[name="car_rental_cancel_booking"]:hover, .car-rental-options input[name="car_rental_cancel_booking"]:hover, .car-rental-booking-details button[name="customer_lookup"]:hover, .car-rental-booking-details input[name="customer_lookup"]:hover, .car-rental-booking-details button[name="car_rental_do_search4"]:hover, .car-rental-booking-details input[name="car_rental_do_search4"]:hover {
        color: {$colors['alter_bd_color']};
        background-color: {$colors['text_hover']};
        border-color: {$colors['text_hover']} 
    }
    
    .car-rental-booking-details .customer-data-input input[type="text"], .car-rental-booking-details .email-search input[type="text"], .car-rental-booking-details .customer-data-input select, .car-rental-booking-details .birth-search select, .car-rental-booking-details .customer-data-input textarea{
        background-color: {$colors['input_bd_color']};
        border-color: {$colors['input_bd_color']};
    }   
    .car-rental-booking-failure .booking-failure-content .back-button{
        color: {$colors['inverse_text']};
        background-color: {$colors['text_link']};
        border-color: {$colors['text_link']};
    }
    
    .car-rental-booking-details .customer-data-input input[type="text"]:focus,
    .car-rental-booking-details .email-search input[type="text"]:focus,
    .car-rental-booking-details .customer-data-input select:focus, 
    .car-rental-booking-details .birth-search select:focus, 
    .car-rental-booking-details .customer-data-input textarea:focus{
        background-color: {$colors['input_bg_hover']};
        border-color: {$colors['input_bd_color']};
    }
    .car-rental-booking-failure .booking-failure-content .back-button:hover,
    .car-rental-booking-failure .booking-failure-content .back-button:focus{
        background-color: {$colors['text_hover']};
        border-color: {$colors['text_hover']};
        color: {$colors['bg_color']};
    }
    .car-rental-booking-failure .booking-failure-content{
         border-color: {$colors['bd_color']};
    }
    .sc_action_item_description{
        color: {$colors['text_dark']};
    }
    .sc_recent_news_style_news-excerpt .post_item .post_date a{
        color: {$colors['text_light']};
    }
    .sc_recent_news_style_news-excerpt .post_item .post_date a:hover{
        color: {$colors['text_link']};
    }
    div .hesperiden.tparrows{
         background-color: {$colors['text_link']};
    }  
    div .hesperiden.tparrows:hover{
         background-color: {$colors['text_hover']};
    }
    .sc_action.sc_action_default .sc_action_content .sc_action_item a.sc_action_item_link{
        color: {$colors['inverse_text']};
        background-color: {$colors['extra_link2']};
        border-color: {$colors['extra_link2']}
    }  
    .sc_action.sc_action_default .sc_action_content .sc_action_item a.sc_action_item_link:hover{
        color: {$colors['inverse_text']};
        background-color: {$colors['alter_link']};
        border-color: {$colors['alter_link']}
    }
    .sc_icons_modern .sc_icons_item+.sc_icons_item{
         border-color: {$colors['bd_color']}
    }
    .sc_icons .sc_icons_icon{
          color: {$colors['text_dark']};
    }
    .post_layout_excerpt.post_format_audio .post_featured .post_audio, .post_layout_excerpt.post_format_quote .post_content .post_content_inner blockquote{
         border-color: {$colors['bg_color']}!important;
    }
    .elementor-toggle-item{
        background-color: {$colors['bg_color']};
    }
    .elementor-toggle .elementor-tab-title .elementor-toggle-icon .fa:before{
        color: {$colors['text_hover']}
    }
    .car-rental-body{
       background-color: {$colors['bg_color']}; 
    }
    .car-rental-body .car-rental-item-info{
        border-color: {$colors['bd_color']};
    }
    .car-rental-body .car-rental-item-info .car-rental-item-info-persons,
    .car-rental-body .car-rental-item-info .car-rental-item-info-mileage,
    .car-rental-body .car-rental-item-info .car-rental-item-info-doors{
        color: {$colors['alter_hover']}; 
    }
    .car-rental-body .car-rental-item-info .car-rental-item-info-persons span,
    .car-rental-body .car-rental-item-info .car-rental-item-info-mileage span,
    .car-rental-body .car-rental-item-info .car-rental-item-info-doors span{
        color: {$colors['alter_link3']}; 
    }
    .car-rental-body .car-rental-item-info .car-rental-item-info-persons:before, .car-rental-body .car-rental-item-info .car-rental-item-info-mileage:before, .car-rental-body .car-rental-item-info .car-rental-item-info-doors:before{
        border-color: {$colors['alter_hover']};
    }
    div .car-rental-slider .responsive-items-slider div.car-rental-item-title a{
        color: {$colors['text_dark']}; 
    }  
     div .car-rental-slider .responsive-items-slider div.car-rental-item-title a:hover{
        color: {$colors['text_link']}; 
    }
    .car-rental-booking-failure{
        border-color: {$colors['input_bg_color']};
    }
    .car-rental-search-result, .car-rental-options, .car-rental-booking-details, .car-rental-booking-confirmed, .car-rental-booking-cancelled, .car-rental-booking-failure{
        background-color: {$colors['bg_color']}; 
    }
    .car-rental-list-single-item .description-item i{
        color: {$colors['text_link']}; 
    }
    .car-rental-booking-failure{
        color: {$colors['text_light']}; 
    }
    .elementor-progress-bar{
        background-color: {$colors['input_bd_hover']}; 
    }
    .post_layout_portfolio .post_featured.hover_dots .post_info{
        color: {$colors['inverse_link']}; 
       
    }
    .search_form button.search_submit:hover,
    .search_form button.search_submit:focus{
         background-color: transparent!important; 
    }
    .search_form button.search_submit:hover:before{
         color: {$colors['text_link']};
    }
    .strong-form input.error, .strong-form textarea.error, .strong-form select.error{
         border-color: {$colors['text_hover']}!important;
         background-color: {$colors['input_bg_color']}; 
    }
    .widget_calendar td#today:before{
         background-color: {$colors['extra_bg_color']}; 
    }
    .widget_calendar caption{
         color: {$colors['text_link']};
    }
    div .editor-styles-wrapper  section > blockquote,
    div .editor-styles-wrapper  div:not(.is-style-solid-color) > blockquote,
    div .editor-styles-wrapper  figure:not(.is-style-solid-color) > blockquote{
        background-color: {$colors['text_link']}!important; 
    }
    .strong-view.default .testimonial-inner{
        background-color: {$colors['bg_color']};  
    }
    .sc_icons.sc_icons_extra .sc_icons_item .sc_icons_item_details .sc_icons_item_description,
    .sc_icons.sc_icons_extra .sc_icons_item .sc_icons_item_details .sc_icons_item_description a{
         color: {$colors['text_dark']};
    }
    .sc_icons.sc_icons_extra .sc_icons_item .sc_icons_item_details .sc_icons_item_description a:hover{
         color: {$colors['text_link']};
    }
    .sc_icons.sc_icons_extra .sc_icons_item .sc_icons_icon:before{
        color: {$colors['inverse_text']};
        background-color: {$colors['inverse_bd_color']};
    }
    .sc_icons.sc_icons_extra .sc_icons_item .sc_icons_item_details .sc_icons_item_title{
         color: {$colors['text_link']};
    }
    ul.inline-list li + li{
         border-color: {$colors['bd_color']};
    }
    .strong-view.default .testimonial-image:after{
         color: {$colors['text_link']};
    }
    .strong-view.simple .testimonial-inner .testimonial-image:after{
         color: {$colors['text_link3']};
    }
    .swiper-pagination-custom .swiper-pagination-button.swiper-pagination-button-active,
    .sc_slider_controls.slider_pagination_style_bullets .slider_pagination_bullet.swiper-pagination-bullet-active, 
    .sc_slider_controls.slider_pagination_style_bullets .slider_pagination_bullet:hover, 
    .slider_container .slider_pagination_wrap .swiper-pagination-bullet.swiper-pagination-bullet-active, 
    .slider_outer .slider_pagination_wrap .swiper-pagination-bullet.swiper-pagination-bullet-active, 
     .slider_container .slider_pagination_wrap .swiper-pagination-bullet:hover, 
      .slider_outer .slider_pagination_wrap .swiper-pagination-bullet:hover{
         border-color: {$colors['text_hover']};
         background-color: {$colors['text_hover']};  
    }
    .sc_slider_controls.slider_pagination_style_bullets .slider_pagination_bullet,
    .slider_container .slider_pagination_wrap .swiper-pagination-bullet, 
    .slider_outer .slider_pagination_wrap .swiper-pagination-bullet, 
    .swiper-pagination-custom .swiper-pagination-button{
         background-color: {$colors['text_light_04']};  
    }
    input[placeholder]::-webkit-input-placeholder,
    textarea[placeholder]::-webkit-input-placeholder	{ color: {$colors['input_text']}; }
    input[placeholder]::-moz-placeholder,
    textarea[placeholder]::-moz-placeholder				{ color: {$colors['input_text']}; }
    input[placeholder]:-ms-input-placeholder,
    textarea[placeholder]:-ms-input-placeholder			{ color: {$colors['input_text']}; }
    input[placeholder]::placeholder,
    textarea[placeholder]::placeholder					{ color: {$colors['input_text']}; }   
    input[type="text"]:focus,
    input[type="text"].filled,
    input[type="number"]:focus,
    input[type="number"].filled,
    input[type="email"]:focus, 
    input[type="email"].filled,
    input[type="tel"]:focus, 
    input[type="search"]:focus, 
    input[type="search"].filled, 
    input[type="password"]:focus, 
    input[type="password"].filled, 
    .select_container:hover, 
    select option:hover,
    select option:focus{
        color: {$colors['input_text']};
    }
    .footer_contact_list .footer_contact_list_description{
         color: {$colors['text_link']}!important;
    }
    .trx_addons_audio_player.without_cover,  
    .format-audio .post_featured.without_thumb .post_audio,
    .mejs-container .mejs-controls,
    .wp-playlist .mejs-container .mejs-controls,
    .trx_addons_audio_player.without_cover .mejs-controls,
    .format-audio .post_featured.without_thumb .mejs-controls{
        background-color: {$colors['extra_bg_color']}; 
    }
    .trx_addons_audio_player.without_cover .audio_author,
    .mejs-controls .mejs-time, .format-audio .post_featured.without_thumb .post_audio_author {
         color: {$colors['inverse_text']};
    }
    .sc_layouts_menu_popup .sc_layouts_menu_nav,
    .sc_layouts_menu_nav>li ul{
          background-color: {$colors['inverse_bd_color']}; 
    }
    .color_style_link2 .sc_item_subtitle{
         color: {$colors['text_link']};
    }
    .font-size .sc_icons.sc_icons_extra .sc_icons_item .sc_icons_item_details .sc_icons_item_title{
         color: {$colors['text_dark']};
    }
     .font-size .sc_icons.sc_icons_extra .sc_icons_item .sc_icons_item_details .sc_icons_item_description{
        color: {$colors['text']};
     }
     .sc_icons.sc_icons_default .sc_icons_item:before{
        background-color: {$colors['alter_bd_color']}; 
     }
     .car-rental-single-location .location-description hr{
         background-color: {$colors['text_hover']}; 
     }
     ul.contact_icon_list li div[class*="icon"], .sc_googlemap_content_default ul.contact_icon_list li div[class*="icon"]{
        border-color:{$colors['text_link']}; 
     }
     ul.contact_icon_list li div[class*="icon"]:before, ul.contact_icon_list li div h6{
        color:{$colors['text_link']}; 
     }
     ul.contact_icon_list li + li {
         border-color:{$colors['bd_color']}; 
     }
     .socials_share  .socials_caption{
        color:{$colors['text']}; 
     }
     .sc_team_default .sc_team_item_info{
         background-color: {$colors['alter_bg_color']}; 
         border-color:{$colors['bg_color']}; 
     }
     ul.list_check li{
        color:{$colors['text_dark']}; 
     }
     .sc_blogger_excerpt .post_item.post_layout_excerpt{
         background-color: {$colors['alter_bg_color']}; 
     }
    .post_meta,
    .post_meta_item,
    .post_meta_item:after,
    .post_meta_item:hover:after,
    .post_meta .vc_inline-link,
    .post_meta .vc_inline-link:after,
    .post_meta .vc_inline-link:hover:after,
    .post_meta_item a,
    .post_info .post_info_item,
    .post_info .post_info_item a,
    .post_info_counters .post_meta_item {
        color: {$colors['text']}!important;
    }    
    
    .post_date a:hover, .post_date a:focus,
    a.post_meta_item:hover, a.post_meta_item:focus,
    .post_meta_item a:hover, .post_meta_item a:focus,
    .post_meta .vc_inline-link:hover, .post_meta .vc_inline-link:focus,
    .post_info .post_info_item a:hover, .post_info .post_info_item a:focus,
    .post_info_meta .post_meta_item:hover, .post_info_meta .post_meta_item:focus {
        color: {$colors['text_dark']}!important;
    }
    .sticky .post_meta_item a, .sticky .post_meta_comments{
        color: {$colors['bg_color']}!important;
    }
     .sticky .post_meta_item a:hover, .sticky .post_meta_comments:hover{
        color: {$colors['text_dark']}!important;
    }
    .post_item_single .post_content .post_tags a {
	color: {$colors['inverse_text']}!important;
	background-color: {$colors['text_link']};
}
    .post_item_single .post_content .post_tags a:hover {
        color: {$colors['alter_bd_color']}!important;
        background-color: {$colors['text_hover']};
    }
    .car-rental-wrapper table>tbody>tr.location-headers>td, 
    .car-rental-wrapper table>tbody>tr.item-headers>td, 
    .car-rental-wrapper table>tbody>tr.duration-headers>td, 
    .car-rental-wrapper table>tbody>tr.total-headers>td{
          background-color: {$colors['extra_bg_color']};
          color: {$colors['bg_color']};
    }
    
     .strong-view.default .testimonial-age{
         color: {$colors['text_link']};
     }
     .strong-view.default .testimonial-name{
        color: {$colors['alter_dark']};
     }

CSS;
		}

		return $css;
	}
}

