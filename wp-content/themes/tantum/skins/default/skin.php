<?php
/**
 * Skins support: Main skin file for the skin 'Default'
 *
 * Setup skin-dependent fonts and colors, load scripts and styles,
 * and other operations that affect the appearance and behavior of the theme
 * when the skin is activated
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.46
 */


// Theme init priorities:
// 3 - add/remove Theme Options elements
if ( ! function_exists( 'tantum_skin_theme_setup1' ) ) {
	add_action( 'after_setup_theme', 'tantum_skin_theme_setup1', 1 );
	function tantum_skin_theme_setup1() {
		// ToDo: Add / Modify theme options, color schemes, required plugins, etc.


        // -----------------------------------------------------------------
        // -- Theme fonts (Google and/or custom fonts)
        // -----------------------------------------------------------------


        // Characters subset for the Google fonts. Available values are: latin,latin-ext,cyrillic,cyrillic-ext,greek,greek-ext,vietnamese
        tantum_storage_set( 'load_fonts_subset', 'latin,latin-ext' );


        $schemes = array(

            // Color scheme: 'default'
            'default' => array(
                'title'    => esc_html__( 'Default', 'tantum' ),
                'internal' => true,
                'colors'   => array(

                    // Whole block border and background
                    'bg_color'         => '#ffffff', //ok
                    'bd_color'         => '#e7e5e6', //ok

                    // Text and links colors
                    'text'             => '#8e8997', //ok
                    'text_light'       => '#95919f', //ok
                    'text_dark'        => '#3b396d', //ok
                    'text_link'        => '#48d183', //ok
                    'text_hover'       => '#4d48bc', //ok
                    'text_link2'       => '#4d48bc', //+
                    'text_hover2'      => '#48d183', //+
                    'text_link3'       => '#eeeeee', //+
                    'text_hover3'      => '#e7e5e6', //+

                    // Alternative blocks (sidebar, tabs, alternative blocks, etc.)
                    'alter_bg_color'   => '#eeeeee', //ok
                    'alter_bg_hover'   => '#eeeeee', //ok
                    'alter_bd_color'   => '#ffffff', //ok
                    'alter_bd_hover'   => '#eaeaea', //ok
                    'alter_text'       => '#8d8997', //ok
                    'alter_light'      => '#95919f', //ok
                    'alter_dark'       => '#3c396e', //ok
                    'alter_link'       => '#48d183', //ok
                    'alter_hover'      => '#4d48bc', //ok
                    'alter_link2'      => '#3b396d', //+
                    'alter_hover2'     => '#80d572',
                    'alter_link3'      => '#4d49bd', //+
                    'alter_hover3'     => '#ddb837',

                    // Extra blocks (submenu, tabs, color blocks, etc.)
                    'extra_bg_color'   => '#4d48bc', //ok
                    'extra_bg_hover'   => '#f5f5f5', //+
                    'extra_bd_color'   => '#ffffff', //ok
                    'extra_bd_hover'   => '#ffffff', //+
                    'extra_text'       => '#ffffff', //ok
                    'extra_light'      => '#95919f', //ok
                    'extra_dark'       => '#ffffff', //ok
                    'extra_link'       => '#48d183', //ok
                    'extra_hover'      => '#e9e9e9', //+
                    'extra_link2'      => '#4d49bd', //+
                    'extra_hover2'     => '#8be77c',
                    'extra_link3'      => '#ddb837',
                    'extra_hover3'     => '#eec432',

                    // Input fields (form's fields and textarea)
                    'input_bg_color'   => '#dcdbdc', //ok
                    'input_bg_hover'   => '#ffffff', //ok
                    'input_bd_color'   => '#dcdbdc', //ok
                    'input_bd_hover'   => '#4d49bd', //ok
                    'input_text'       => '#5f596c', //ok
                    'input_light'      => '#95919f', //ok ?
                    'input_dark'       => '#5f596c', //ok

                    // Inverse blocks (text and links on the 'text_link' background)
                    'inverse_bd_color' => '#3d3997', //ok
                    'inverse_bd_hover' => '#302d83', //+
                    'inverse_text'     => '#ffffff', //ok
                    'inverse_light'    => '#95919f', //ok
                    'inverse_dark'     => '#ffffff', //ok
                    'inverse_link'     => '#ffffff', //ok
                    'inverse_hover'    => '#4d48bc', //ok
                ),
            ),

            // Color scheme: 'dark'
            'dark'    => array(
                'title'    => esc_html__( 'Dark', 'tantum' ),
                'internal' => true,
                'colors'   => array(

                    // Whole block border and background
                    'bg_color'         => '#4d48bc', //ok
                    'bd_color'         => '#3d3997', //ok

                    // Text and links colors
                    'text'             => '#dddbe0', //ok
                    'text_light'       => '#dddbe0', //ok
                    'text_dark'        => '#ffffff', //ok
                    'text_link'        => '#48d183', //ok
                    'text_hover'       => '#ffffff', //ok
                    'text_link2'       => '#80d572',
                    'text_hover2'      => '#8be77c',
                    'text_link3'       => '#eeeeee', //+
                    'text_hover3'      => '#4d49bd', //+

                    // Alternative blocks (sidebar, tabs, alternative blocks, etc.)
                    'alter_bg_color'   => '#3d3997', //ok for bg_color
                    'alter_bg_hover'   => '#3b396d', //+
                    'alter_bd_color'   => '#4d49bd', //ok
                    'alter_bd_hover'   => '#3d3997', //+
                    'alter_text'       => '#dddbe0', //ok
                    'alter_light'      => '#dddbe0', //ok
                    'alter_dark'       => '#ffffff', //ok
                    'alter_link'       => '#48d183', //ok
                    'alter_hover'      => '#ffffff', //ok
                    'alter_link2'      => '#3b396d', //+
                    'alter_hover2'     => '#80d572',
                    'alter_link3'      => '#3d3997', //+
                    'alter_hover3'     => '#ddb837',

                    // Extra blocks (submenu, tabs, color blocks, etc.)
                    'extra_bg_color'   => '#232323', //ok
                    'extra_bg_hover'   => '#2B2B2B', //ok ?
                    'extra_bd_color'   => '#2B2B2B', //ok ?
                    'extra_bd_hover'   => '#3d3997', //+
                    'extra_text'       => '#939393', //ok ?
                    'extra_light'      => '#6f6f6f',
                    'extra_dark'       => '#ffffff', //ok
                    'extra_link'       => '#219FFF', //ok ?
                    'extra_hover'      => '#ffffff', //ok ?
                    'extra_link2'      => '#4d49bd', //+
                    'extra_hover2'     => '#8be77c',
                    'extra_link3'      => '#ddb837',
                    'extra_hover3'     => '#eec432',

                    // Input fields (form's fields and textarea)
                    'input_bg_color'   => '#302d83', //ok
                    'input_bg_hover'   => '#302d83', //ok ?
                    'input_bd_color'   => '#302d83', //ok
                    'input_bd_hover'   => '#49d182', //ok
                    'input_text'       => '#a09fc6', //ok
                    'input_light'      => '#a09fc6', //ok ?
                    'input_dark'       => '#a09fc6', //ok

                    // Inverse blocks (text and links on the 'text_link' background)
                    'inverse_bd_color' => '#3b396d', //+
                    'inverse_bd_hover' => '#302d83', //+
                    'inverse_text'     => '#ffffff', //ok
                    'inverse_light'    => '#6f6f6f',
                    'inverse_dark'     => '#ffffff', //ok
                    'inverse_link'     => '#ffffff', //ok
                    'inverse_hover'    => '#4d49bd', //ok
                ),
            ),
        );
        tantum_storage_set( 'schemes', $schemes );
        tantum_storage_set( 'schemes_original', $schemes );

        // Additional colors for each scheme
        // Parameters:	'color' - name of the color from the scheme that should be used as source for the transformation
        //				'alpha' - to make color transparent (0.0 - 1.0)
        //				'hue', 'saturation', 'brightness' - inc/dec value for each color's component
        tantum_storage_set(
            'scheme_colors_add', array(
                'bg_color_0'        => array(
                    'color' => 'bg_color',
                    'alpha' => 0,
                ),
                'bg_color_02'       => array(
                    'color' => 'bg_color',
                    'alpha' => 0.2,
                ),
                'bg_color_07'       => array(
                    'color' => 'bg_color',
                    'alpha' => 0.7,
                ),
                'bg_color_08'       => array(
                    'color' => 'bg_color',
                    'alpha' => 0.8,
                ),
                'bg_color_09'       => array(
                    'color' => 'bg_color',
                    'alpha' => 0.9,
                ),
                'bg_color_04'       => array(
                    'color' => 'bg_color',
                    'alpha' => 0.4,
                ),
                'alter_bg_color_07' => array(
                    'color' => 'alter_bg_color',
                    'alpha' => 0.7,
                ),
                'alter_bg_color_04' => array(
                    'color' => 'alter_bg_color',
                    'alpha' => 0.4,
                ),
                'alter_bg_color_00' => array(
                    'color' => 'alter_bg_color',
                    'alpha' => 0,
                ),
                'alter_bg_color_02' => array(
                    'color' => 'alter_bg_color',
                    'alpha' => 0.2,
                ),
                'alter_bd_color_02' => array(
                    'color' => 'alter_bd_color',
                    'alpha' => 0.2,
                ),
                'alter_link_02'     => array(
                    'color' => 'alter_link',
                    'alpha' => 0.2,
                ),
                'alter_link_07'     => array(
                    'color' => 'alter_link',
                    'alpha' => 0.7,
                ),
                'extra_bg_color_05' => array(
                    'color' => 'extra_bg_color',
                    'alpha' => 0.5,
                ),
                'extra_bg_color_07' => array(
                    'color' => 'extra_bg_color',
                    'alpha' => 0.7,
                ),
                'extra_link_02'     => array(
                    'color' => 'extra_link',
                    'alpha' => 0.2,
                ),
                'extra_link_07'     => array(
                    'color' => 'extra_link',
                    'alpha' => 0.7,
                ),
                'text_dark_07'      => array(
                    'color' => 'text_dark',
                    'alpha' => 0.7,
                ),
                'text_link_02'      => array(
                    'color' => 'text_link',
                    'alpha' => 0.2,
                ),
                'text_link_07'      => array(
                    'color' => 'text_link',
                    'alpha' => 0.7,
                ),
                'text_light_07'      => array(
                    'color' => 'text_light',
                    'alpha' => 0.7,
                ),
                'text_link_blend'   => array(
                    'color'      => 'text_link',
                    'hue'        => 2,
                    'saturation' => -5,
                    'brightness' => 5,
                ),
                'alter_link_blend'  => array(
                    'color'      => 'alter_link',
                    'hue'        => 2,
                    'saturation' => -5,
                    'brightness' => 5,
                ),
                'inverse_link_02' => array(
                    'color' => 'inverse_link',
                    'alpha' => 0.2,
                ),
                'inverse_link_04'      => array(
                    'color' => 'inverse_link',
                    'alpha' => 0.4,
                ),
            )
        );

    }
}

// Filter to add in the required plugins list
if ( ! function_exists( 'tantum_skin_tgmpa_required_plugins' ) ) {
	add_filter( 'tantum_filter_tgmpa_required_plugins', 'tantum_skin_tgmpa_required_plugins' );
	function tantum_skin_tgmpa_required_plugins( $list = array() ) {
		// ToDo: Check if plugin is in the 'required_plugins' and add his parameters to the TGMPA-list
		//       Replace 'skin-specific-plugin-slug' to the real slug of the plugin
		if ( tantum_storage_isset( 'required_plugins', 'skin-specific-plugin-slug' ) ) {
			$list[] = array(
				'name'     => tantum_storage_get_array( 'required_plugins', 'skin-specific-plugin-slug', 'title' ),
				'slug'     => 'skin-specific-plugin-slug',
				'required' => false,
			);
		}
		return $list;
	}
}

// Enqueue skin-specific styles and scripts
// Priority 1150 - after plugins-specific (1100), but before child theme (1500)
if ( ! function_exists( 'tantum_skin_frontend_scripts' ) ) {
	add_action( 'wp_enqueue_scripts', 'tantum_skin_frontend_scripts', 1150 );
	function tantum_skin_frontend_scripts() {
		$tantum_url = tantum_get_file_url( TANTUM_SKIN_DIR . 'skin.css' );
		if ( '' != $tantum_url ) {
			wp_enqueue_style( 'tantum-skin-' . esc_attr( TANTUM_SKIN_NAME ), $tantum_url, array(), null );
		}
		if ( tantum_is_on( tantum_get_theme_option( 'debug_mode' ) ) ) {
			$tantum_url = tantum_get_file_url( TANTUM_SKIN_DIR . 'skin.js' );
			if ( '' != $tantum_url ) {
				wp_enqueue_script( 'tantum-skin-' . esc_attr( TANTUM_SKIN_NAME ), $tantum_url, array( 'jquery' ), null, true );
			}
		}
	}
}

// Enqueue skin-specific responsive styles
// Priority 2150 - after theme responsive 2000
if ( ! function_exists( 'tantum_skin_styles_responsive' ) ) {
	add_action( 'wp_enqueue_scripts', 'tantum_skin_styles_responsive', 2150 );
	function tantum_skin_styles_responsive() {
		$tantum_url = tantum_get_file_url( TANTUM_SKIN_DIR . 'skin-responsive.css' );
		if ( '' != $tantum_url ) {
			wp_enqueue_style( 'tantum-skin-' . esc_attr( TANTUM_SKIN_NAME ) . '-responsive', $tantum_url, array(), null );
		}
	}
}

// Merge custom scripts
if ( ! function_exists( 'tantum_skin_merge_scripts' ) ) {
	add_filter( 'tantum_filter_merge_scripts', 'tantum_skin_merge_scripts' );
	function tantum_skin_merge_scripts( $list ) {
		if ( tantum_get_file_dir( TANTUM_SKIN_DIR . 'skin.js' ) != '' ) {
			$list[] = TANTUM_SKIN_DIR . 'skin.js';
		}
		return $list;
	}
}

//Set theme specific importer options
if ( false && ! function_exists( 'tantum_skin_importer_set_options' ) ) {
	add_filter('trx_addons_filter_importer_options', 'tantum_skin_importer_set_options', 9);
	function tantum_skin_importer_set_options($options = array()) {
		if ( is_array( $options ) ) {
			$options['demo_type'] = 'default';
			$options['files']['skin_slug'] = $options['files']['default'];
			$options['files']['skin_slug']['title'] = esc_html__('Skin Title Demo', 'tantum');
			$options['files']['skin_slug']['domain_dev'] = esc_url( tantum_get_protocol() . '://skin_slug.theme_slug.ancorathemes.dnw' );    // Developers domain
			$options['files']['skin_slug']['domain_demo'] = esc_url( tantum_get_protocol() . '://skin_slug.theme_slug.ancorathemes.net' );   // Demo-site domain
			unset($options['files']['default']);
		}
		return $options;
	}
}

// Add slin-specific colors and fonts to the custom CSS
require_once TANTUM_THEME_DIR . TANTUM_SKIN_DIR . 'skin-styles.php';
