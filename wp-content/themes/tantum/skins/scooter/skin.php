<?php
/**
 * Skins support: Main skin file for the skin 'Scooter'
 *
 * Setup skin-dependent fonts and colors, load scripts and styles,
 * and other operations that affect the appearance and behavior of the theme
 * when the skin is activated
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.46
 */


// Theme init priorities:
// 3 - add/remove Theme Options elements
if ( ! function_exists( 'tantum_skin_theme_setup1' ) ) {
	add_action( 'after_setup_theme', 'tantum_skin_theme_setup1', 1 );
	function tantum_skin_theme_setup1() {
		// ToDo: Add / Modify theme options, color schemes, required plugins, etc.


        // -----------------------------------------------------------------
        // -- Theme fonts (Google and/or custom fonts)
        // -----------------------------------------------------------------

        // Fonts to load when theme start
        // It can be Google fonts or uploaded fonts, placed in the folder /css/font-face/font-name inside the theme folder
        // Attention! Font's folder must have name equal to the font's name, with spaces replaced on the dash '-'
        // For example: font name 'TeX Gyre Termes', folder 'TeX-Gyre-Termes'


        // Characters subset for the Google fonts. Available values are: latin,latin-ext,cyrillic,cyrillic-ext,greek,greek-ext,vietnamese
        tantum_storage_set( 'load_fonts_subset', 'latin,latin-ext' );


        $schemes = array(

            // Color scheme: 'default'
            'default' => array(
                'title'    => esc_html__( 'Default', 'tantum' ),
                'internal' => true,
                'colors'   => array(

                    // Whole block border and background
                    'bg_color'         => '#ffffff', //ok
                    'bd_color'         => '#eeeeee', //ok

                    // Text and links colors
                    'text'             => '#8e8997', //ok
                    'text_light'       => '#95919f', //ok
                    'text_dark'        => '#262858', //ok
                    'text_link'        => '#fb3e5d', //ok
                    'text_hover'       => '#2e3166', //ok
                    'text_link2'       => '#2e3166', //+
                    'text_hover2'      => '#fb3e5d', //+
                    'text_link3'       => '#eeeeee', //+
                    'text_hover3'      => '#e7e5e6',

                    // Alternative blocks (sidebar, tabs, alternative blocks, etc.)
                    'alter_bg_color'   => '#eeeeee', //ok
                    'alter_bg_hover'   => '#e3e2e2', //ok
                    'alter_bd_color'   => '#ffffff', //ok
                    'alter_bd_hover'   => '#eaeaea', //+
                    'alter_text'       => '#aaaaaa', //ok
                    'alter_light'      => '#95919f', //ok
                    'alter_dark'       => '#262858', //ok
                    'alter_link'       => '#fb3e5d', //ok
                    'alter_hover'      => '#8175b1', //+
                    'alter_link2'      => '#3f4277', //+
                    'alter_hover2'     => '#3f4277', //+
                    'alter_link3'      => '#4d49bd', //+
                    'alter_hover3'     => '#eeeeee', //+

                    // Extra blocks (submenu, tabs, color blocks, etc.)
                    'extra_bg_color'   => '#2e3166', //ok
                    'extra_bg_hover'   => '#43308f', //+
                    'extra_bd_color'   => '#ffffff', //ok
                    'extra_bd_hover'   => '#ffffff',
                    'extra_text'       => '#ffffff', //ok
                    'extra_light'      => '#95919f', //ok
                    'extra_dark'       => '#ffffff', //ok
                    'extra_link'       => '#3c2a84', //ok
                    'extra_hover'      => '#ffffff', //+
                    'extra_link2'      => '#4d49bd',
                    'extra_hover2'     => '#8be77c',
                    'extra_link3'      => '#ddb837',
                    'extra_hover3'     => '#eec432',

                    // Input fields (form's fields and textarea)
                    'input_bg_color'   => '#e1e0e0', //ok
                    'input_bg_hover'   => '#ffffff', //ok
                    'input_bd_color'   => '#e1e0e0', //ok
                    'input_bd_hover'   => '#2e3166', //ok
                    'input_text'       => '#787878', //ok
                    'input_light'      => '#95919f',
                    'input_dark'       => '#262858', //ok

                    // Inverse blocks (text and links on the 'text_link' background)
                    'inverse_bd_color' => '#3c2a84', //+
                    'inverse_bd_hover' => '#242656', //+
                    'inverse_text'     => '#ffffff',
                    'inverse_light'    => '#95919f',
                    'inverse_dark'     => '#ffffff',
                    'inverse_link'     => '#ffffff', //ok
                    'inverse_hover'    => '#2e3166', //ok
                ),
            ),

            // Color scheme: 'dark'
            'dark'    => array(
                'title'    => esc_html__( 'Dark', 'tantum' ),
                'internal' => true,
                'colors'   => array(

                    // Whole block border and background
                    'bg_color'         => '#2e3166', //ok
                    'bd_color'         => '#43308f', //ok

                    // Text and links colors
                    'text'             => '#dddbe0', //ok
                    'text_light'       => '#dddbe0', //ok
                    'text_dark'        => '#ffffff', //ok
                    'text_link'        => '#fb3e5d', //ok
                    'text_hover'       => '#ffffff', //ok
                    'text_link2'       => '#43308f', //+
                    'text_hover2'      => '#fb3e5d', //+
                    'text_link3'       => '#3d3997', //+
                    'text_hover3'      => '#3d3997', //+

                    // Alternative blocks (sidebar, tabs, alternative blocks, etc.)
                    'alter_bg_color'   => '#43308f', //ok for bg_color
                    'alter_bg_hover'   => '#43308f', //+
                    'alter_bd_color'   => '#2e3166', //ok
                    'alter_bd_hover'   => '#8175b1', //+
                    'alter_text'       => '#dddbe0', //ok
                    'alter_light'      => '#dddbe0', //ok
                    'alter_dark'       => '#ffffff', //ok
                    'alter_link'       => '#fb3e5d', //ok
                    'alter_hover'      => '#ffffff', //ok
                    'alter_link2'      => '#8175b1', //+
                    'alter_hover2'     => '#3f4277', //+
                    'alter_link3'      => '#fb3e5d', //+
                    'alter_hover3'     => '#eeeeee', //+

                    // Extra blocks (submenu, tabs, color blocks, etc.)
                    'extra_bg_color'   => '#43308f', //ok
                    'extra_bg_hover'   => '#3d3997', //ok ?
                    'extra_bd_color'   => '#2e3166', //ok ?
                    'extra_bd_hover'   => '#3d3997',
                    'extra_text'       => '#dddbe0', //+
                    'extra_light'      => '#dddbe0', //+
                    'extra_dark'       => '#ffffff', //ok
                    'extra_link'       => '#ffffff', //ok
                    'extra_hover'      => '#ffffff', //ok ?
                    'extra_link2'      => '#4d49bd',
                    'extra_hover2'     => '#8be77c',
                    'extra_link3'      => '#ddb837',
                    'extra_hover3'     => '#eeeeee',

                    // Input fields (form's fields and textarea)
                    'input_bg_color'   => '#242656', //ok
                    'input_bg_hover'   => '#43308f', //ok ?
                    'input_bd_color'   => '#242656', //ok
                    'input_bd_hover'   => '#fb3e5d', //ok
                    'input_text'       => '#8f90a6', //ok
                    'input_light'      => '#ffffff', //ok
                    'input_dark'       => '#ffffff', //ok

                    // Inverse blocks (text and links on the 'text_link' background)
                    'inverse_bd_color' => '#3c2a84', //+
                    'inverse_bd_hover' => '#302d83',
                    'inverse_text'     => '#ffffff', //ok
                    'inverse_light'    => '#6f6f6f',
                    'inverse_dark'     => '#ffffff', //ok
                    'inverse_link'     => '#ffffff', //ok
                    'inverse_hover'    => '#43308f', //ok
                ),
            ),
        );
        tantum_storage_set( 'schemes', $schemes );
        tantum_storage_set( 'schemes_original', $schemes );

        // Additional colors for each scheme
        // Parameters:	'color' - name of the color from the scheme that should be used as source for the transformation
        //				'alpha' - to make color transparent (0.0 - 1.0)
        //				'hue', 'saturation', 'brightness' - inc/dec value for each color's component
        tantum_storage_set(
            'scheme_colors_add', array(
                'bg_color_0'        => array(
                    'color' => 'bg_color',
                    'alpha' => 0,
                ),
                'bg_color_02'       => array(
                    'color' => 'bg_color',
                    'alpha' => 0.2,
                ),
                'bg_color_07'       => array(
                    'color' => 'bg_color',
                    'alpha' => 0.7,
                ),
                'bg_color_08'       => array(
                    'color' => 'bg_color',
                    'alpha' => 0.8,
                ),
                'bg_color_09'       => array(
                    'color' => 'bg_color',
                    'alpha' => 0.9,
                ),
                'bg_color_04'       => array(
                    'color' => 'bg_color',
                    'alpha' => 0.4,
                ),
                'alter_bg_color_07' => array(
                    'color' => 'alter_bg_color',
                    'alpha' => 0.7,
                ),
                'alter_bg_color_04' => array(
                    'color' => 'alter_bg_color',
                    'alpha' => 0.4,
                ),
                'alter_bg_color_00' => array(
                    'color' => 'alter_bg_color',
                    'alpha' => 0,
                ),
                'alter_bg_color_02' => array(
                    'color' => 'alter_bg_color',
                    'alpha' => 0.2,
                ),
                'alter_bd_color_02' => array(
                    'color' => 'alter_bd_color',
                    'alpha' => 0.2,
                ),
                'alter_link_02'     => array(
                    'color' => 'alter_link',
                    'alpha' => 0.2,
                ),
                'alter_link_07'     => array(
                    'color' => 'alter_link',
                    'alpha' => 0.7,
                ),
                'extra_bg_color_05' => array(
                    'color' => 'extra_bg_color',
                    'alpha' => 0.5,
                ),
                'extra_bg_color_07' => array(
                    'color' => 'extra_bg_color',
                    'alpha' => 0.7,
                ),
                'extra_link_02'     => array(
                    'color' => 'extra_link',
                    'alpha' => 0.2,
                ),
                'extra_link_07'     => array(
                    'color' => 'extra_link',
                    'alpha' => 0.7,
                ),
                'text_dark_07'      => array(
                    'color' => 'text_dark',
                    'alpha' => 0.7,
                ),
                'text_link_02'      => array(
                    'color' => 'text_link',
                    'alpha' => 0.2,
                ),
                'text_link_07'      => array(
                    'color' => 'text_link',
                    'alpha' => 0.7,
                ),
                'text_light_07'      => array(
                    'color' => 'text_light',
                    'alpha' => 0.7,
                ),
                'text_link_blend'   => array(
                    'color'      => 'text_link',
                    'hue'        => 2,
                    'saturation' => -5,
                    'brightness' => 5,
                ),
                'alter_link_blend'  => array(
                    'color'      => 'alter_link',
                    'hue'        => 2,
                    'saturation' => -5,
                    'brightness' => 5,
                ),
                'inverse_link_02' => array(
                    'color' => 'inverse_link',
                    'alpha' => 0.2,
                ),
                'inverse_link_04'      => array(
                    'color' => 'inverse_link',
                    'alpha' => 0.4,
                ),
            )
        );

    }
}

// Filter to add in the required plugins list
if ( ! function_exists( 'tantum_skin_tgmpa_required_plugins' ) ) {
	add_filter( 'tantum_filter_tgmpa_required_plugins', 'tantum_skin_tgmpa_required_plugins' );
	function tantum_skin_tgmpa_required_plugins( $list = array() ) {
		// ToDo: Check if plugin is in the 'required_plugins' and add his parameters to the TGMPA-list
		//       Replace 'skin-specific-plugin-slug' to the real slug of the plugin
		if ( tantum_storage_isset( 'required_plugins', 'skin-specific-plugin-slug' ) ) {
			$list[] = array(
				'name'     => tantum_storage_get_array( 'required_plugins', 'skin-specific-plugin-slug', 'title' ),
				'slug'     => 'skin-specific-plugin-slug',
				'required' => false,
			);
		}
		return $list;
	}
}

// Enqueue skin-specific styles and scripts
// Priority 1150 - after plugins-specific (1100), but before child theme (1500)
if ( ! function_exists( 'tantum_skin_frontend_scripts' ) ) {
	add_action( 'wp_enqueue_scripts', 'tantum_skin_frontend_scripts', 1150 );
	function tantum_skin_frontend_scripts() {
		$tantum_url = tantum_get_file_url( TANTUM_SKIN_DIR . 'skin.css' );
		if ( '' != $tantum_url ) {
			wp_enqueue_style( 'tantum-skin-' . esc_attr( TANTUM_SKIN_NAME ), $tantum_url, array(), null );
		}
		if ( tantum_is_on( tantum_get_theme_option( 'debug_mode' ) ) ) {
			$tantum_url = tantum_get_file_url( TANTUM_SKIN_DIR . 'skin.js' );
			if ( '' != $tantum_url ) {
				wp_enqueue_script( 'tantum-skin-' . esc_attr( TANTUM_SKIN_NAME ), $tantum_url, array( 'jquery' ), null, true );
			}
		}
	}
}

// Enqueue skin-specific responsive styles
// Priority 2150 - after theme responsive 2000
if ( ! function_exists( 'tantum_skin_styles_responsive' ) ) {
	add_action( 'wp_enqueue_scripts', 'tantum_skin_styles_responsive', 2150 );
	function tantum_skin_styles_responsive() {
		$tantum_url = tantum_get_file_url( TANTUM_SKIN_DIR . 'skin-responsive.css' );
		if ( '' != $tantum_url ) {
			wp_enqueue_style( 'tantum-skin-' . esc_attr( TANTUM_SKIN_NAME ) . '-responsive', $tantum_url, array(), null );
		}
	}
}

// Merge custom scripts
if ( ! function_exists( 'tantum_skin_merge_scripts' ) ) {
	add_filter( 'tantum_filter_merge_scripts', 'tantum_skin_merge_scripts' );
	function tantum_skin_merge_scripts( $list ) {
		if ( tantum_get_file_dir( TANTUM_SKIN_DIR . 'skin.js' ) != '' ) {
			$list[] = TANTUM_SKIN_DIR . 'skin.js';
		}
		return $list;
	}
}

//Set theme specific importer options
if ( ! function_exists( 'tantum_skin_importer_set_options' ) ) {
	add_filter('trx_addons_filter_importer_options', 'tantum_skin_importer_set_options', 9);
	function tantum_skin_importer_set_options($options = array()) {
		if ( is_array( $options ) ) {
			$options['demo_type'] = 'scooter';
			$options['files']['scooter'] = $options['files']['default'];
			$options['files']['scooter']['title'] = esc_html__('Skin Title Demo', 'tantum');
			$options['files']['scooter']['domain_dev'] = '';    // Developers domain
			$options['files']['scooter']['domain_demo'] = esc_url( tantum_get_protocol() . '://scooter.tantum.ancorathemes.com' );   // Demo-site domain
			unset($options['files']['default']);
		}
		return $options;
	}
}

// Add slin-specific colors and fonts to the custom CSS
require_once TANTUM_THEME_DIR . TANTUM_SKIN_DIR . 'skin-styles.php';
