Version 1.0.2
    Fixed:
        - Themerex Addons vulnarability fixed


Version 1.0.1
    Added:
        - Compatibility with WP 5.3.2

    Fixed:
        - Update Plugin TRX Addons


Version 1.0.0
Plugin TRX Addons has not been edited
