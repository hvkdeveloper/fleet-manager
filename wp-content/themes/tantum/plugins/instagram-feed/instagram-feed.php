<?php
/* Instagram Feed support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if ( ! function_exists( 'tantum_instagram_feed_theme_setup9' ) ) {
	add_action( 'after_setup_theme', 'tantum_instagram_feed_theme_setup9', 9 );
	function tantum_instagram_feed_theme_setup9() {
		if ( tantum_exists_instagram_feed() ) {
			add_action( 'wp_enqueue_scripts', 'tantum_instagram_responsive_styles', 2000 );
			add_filter( 'tantum_filter_merge_styles_responsive', 'tantum_instagram_merge_styles_responsive' );
		}
		if ( is_admin() ) {
			add_filter( 'tantum_filter_tgmpa_required_plugins', 'tantum_instagram_feed_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( ! function_exists( 'tantum_instagram_feed_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('tantum_filter_tgmpa_required_plugins',	'tantum_instagram_feed_tgmpa_required_plugins');
	function tantum_instagram_feed_tgmpa_required_plugins( $list = array() ) {
		if ( tantum_storage_isset( 'required_plugins', 'instagram-feed' ) && tantum_storage_get_array( 'required_plugins', 'instagram-feed', 'install' ) !== false ) {
			$list[] = array(
				'name'     => tantum_storage_get_array( 'required_plugins', 'instagram-feed', 'title' ),
				'slug'     => 'instagram-feed',
				'required' => false,
			);
		}
		return $list;
	}
}

// Check if Instagram Feed installed and activated
if ( ! function_exists( 'tantum_exists_instagram_feed' ) ) {
	function tantum_exists_instagram_feed() {
		return defined( 'SBIVER' );
	}
}

// Enqueue responsive styles for frontend
if ( ! function_exists( 'tantum_instagram_responsive_styles' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'tantum_instagram_responsive_styles', 2000 );
	function tantum_instagram_responsive_styles() {
		if ( tantum_is_on( tantum_get_theme_option( 'debug_mode' ) ) ) {
			$tantum_url = tantum_get_file_url( 'plugins/instagram/instagram-responsive.css' );
			if ( '' != $tantum_url ) {
				wp_enqueue_style( 'tantum-instagram-responsive', $tantum_url, array(), null );
			}
		}
	}
}

// Merge responsive styles
if ( ! function_exists( 'tantum_instagram_merge_styles_responsive' ) ) {
	//Handler of the add_filter('tantum_filter_merge_styles_responsive', 'tantum_instagram_merge_styles_responsive');
	function tantum_instagram_merge_styles_responsive( $list ) {
		$list[] = 'plugins/instagram/instagram-responsive.css';
		return $list;
	}
}

