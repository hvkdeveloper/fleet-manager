<?php
/* Mail Chimp support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('tantum_testimonials_theme_setup9')) {
	add_action( 'after_setup_theme', 'tantum_testimonials_theme_setup9', 9 );
	function tantum_testimonials_theme_setup9() {
		if (is_admin()) {
			add_filter( 'tantum_filter_tgmpa_required_plugins',		'tantum_testimonials_tgmpa_required_plugins' );
		}
	}
}


// Filter to add in the required plugins list
if ( ! function_exists( 'tantum_testimonials_tgmpa_required_plugins' ) ) {
    //Handler of the add_filter('tantum_filter_tgmpa_required_plugins',	'tantum_elm_tgmpa_required_plugins');
    function tantum_testimonials_tgmpa_required_plugins( $list = array() ) {
        if ( tantum_storage_isset( 'required_plugins', 'strong-testimonials' ) && tantum_storage_get_array( 'required_plugins', 'strong-testimonials', 'install' ) !== false ) {
            $list[] = array(
                'name'     => tantum_storage_get_array( 'required_plugins', 'strong-testimonials', 'title' ),
                'slug'     => 'strong-testimonials',
                'required' => false,
            );
        }
        return $list;
    }
}

// Check if plugin installed and activated
if ( !function_exists( 'tantum_exists_testimonials' ) ) {
	function tantum_exists_testimonials() {
		return function_exists('cp_shortcode_widget_init');
	}
}

// Check if plugin installed and activated
if ( !function_exists( 'tantum_exists_strong_testimonials' ) ) {
    function tantum_exists_strong_testimonials() {
        return defined('WPMTST_PLUGIN');
    }
}

// Add plugin-specific slugs to the list of the scripts, that don't move to the footer and don't add 'defer' param
if ( !function_exists( 'tantum_strong_testimonials_not_defer_scripts' ) ) {
    add_filter("trx_addons_filter_skip_move_scripts_down", 'tantum_strong_testimonials_not_defer_scripts');
    add_filter("trx_addons_filter_skip_async_scripts_load", 'tantum_strong_testimonials_not_defer_scripts');
    function tantum_strong_testimonials_not_defer_scripts($list) {
        if ( tantum_exists_strong_testimonials() ) {
            $list[] = 'strong-testimonials';
        }
        return $list;
    }
}

?>