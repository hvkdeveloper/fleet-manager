<?php
/* WPBakery PageBuilder support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if ( ! function_exists( 'tantum_vc_theme_setup9' ) ) {
	add_action( 'after_setup_theme', 'tantum_vc_theme_setup9', 9 );
	function tantum_vc_theme_setup9() {

		if ( tantum_exists_vc() ) {
		
			add_action( 'wp_enqueue_scripts', 'tantum_vc_frontend_scripts', 1100 );
			add_action( 'wp_enqueue_scripts', 'tantum_vc_responsive_styles', 2000 );
			add_filter( 'tantum_filter_merge_styles', 'tantum_vc_merge_styles' );
			add_filter( 'tantum_filter_merge_styles_responsive', 'tantum_vc_merge_styles_responsive' );

			// Add/Remove params in the standard VC shortcodes
			//-----------------------------------------------------
			add_filter( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tantum_vc_add_params_classes', 10, 3 );
			add_filter( 'vc_iconpicker-type-fontawesome', 'tantum_vc_iconpicker_type_fontawesome' );

			// Color scheme
			$scheme  = array(
				'param_name'  => 'scheme',
				'heading'     => esc_html__( 'Color scheme', 'tantum' ),
				'description' => wp_kses_data( __( 'Select color scheme to decorate this block', 'tantum' ) ),
				'group'       => esc_html__( 'Colors', 'tantum' ),
				'admin_label' => true,
				'value'       => array_flip( tantum_get_list_schemes( true ) ),
				'type'        => 'dropdown',
			);
			$sc_list = apply_filters( 'tantum_filter_add_scheme_in_vc', array( 'vc_section', 'vc_row', 'vc_row_inner', 'vc_column', 'vc_column_inner', 'vc_column_text' ) );
			foreach ( $sc_list as $sc ) {
				vc_add_param( $sc, $scheme );
			}

			// Load custom VC styles for blog archive page
			add_filter( 'tantum_filter_blog_archive_start', 'tantum_vc_add_inline_css' );
		}

		if ( is_admin() ) {
			add_filter( 'tantum_filter_tgmpa_required_plugins', 'tantum_vc_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( ! function_exists( 'tantum_vc_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('tantum_filter_tgmpa_required_plugins',	'tantum_vc_tgmpa_required_plugins');
	function tantum_vc_tgmpa_required_plugins( $list = array() ) {
		if ( tantum_storage_isset( 'required_plugins', 'js_composer' ) && tantum_storage_get_array( 'required_plugins', 'js_composer', 'install' ) !== false && tantum_is_theme_activated() ) {
			$path = tantum_get_plugin_source_path( 'plugins/js_composer/js_composer.zip' );
			if ( ! empty( $path ) || tantum_get_theme_setting( 'tgmpa_upload' ) ) {
				$list[] = array(
					'name'     => tantum_storage_get_array( 'required_plugins', 'js_composer', 'title' ),
					'slug'     => 'js_composer',
					'source'   => ! empty( $path ) ? $path : 'upload://js_composer.zip',

					'required' => false,
				);
			}
		}
		return $list;
	}
}

// Check if plugin installed and activated
if ( ! function_exists( 'tantum_exists_vc' ) ) {
	function tantum_exists_vc() {
		return class_exists( 'Vc_Manager' );
	}
}

// Check if plugin in frontend editor mode
if ( ! function_exists( 'tantum_vc_is_frontend' ) ) {
	function tantum_vc_is_frontend() {
		return ( isset( $_GET['vc_editable'] ) && 'true' == $_GET['vc_editable'] )
			|| ( isset( $_GET['vc_action'] ) && 'vc_inline' == $_GET['vc_action'] );
		//return function_exists('vc_is_frontend_editor') && vc_is_frontend_editor();
	}
}

// Enqueue styles for frontend
if ( ! function_exists( 'tantum_vc_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'tantum_vc_frontend_scripts', 1100 );
	function tantum_vc_frontend_scripts() {
		if ( tantum_is_on( tantum_get_theme_option( 'debug_mode' ) ) ) {
			$tantum_url = tantum_get_file_url( 'plugins/js_composer/js_composer.css' );
			if ( '' != $tantum_url ) {
				wp_enqueue_style( 'tantum-vc', $tantum_url, array(), null );
			}
		}
	}
}

// Enqueue responsive styles for frontend
if ( ! function_exists( 'tantum_vc_responsive_styles' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'tantum_vc_responsive_styles', 2000 );
	function tantum_vc_responsive_styles() {
		if ( tantum_is_on( tantum_get_theme_option( 'debug_mode' ) ) ) {
			$tantum_url = tantum_get_file_url( 'plugins/js_composer/js_composer-responsive.css' );
			if ( '' != $tantum_url ) {
				wp_enqueue_style( 'tantum-vc-responsive', $tantum_url, array(), null );
			}
		}
	}
}

// Merge custom styles
if ( ! function_exists( 'tantum_vc_merge_styles' ) ) {
	//Handler of the add_filter('tantum_filter_merge_styles', 'tantum_vc_merge_styles');
	function tantum_vc_merge_styles( $list ) {
		$list[] = 'plugins/js_composer/js_composer.css';
		return $list;
	}
}

// Merge responsive styles
if ( ! function_exists( 'tantum_vc_merge_styles_responsive' ) ) {
	//Handler of the add_filter('tantum_filter_merge_styles_responsive', 'tantum_vc_merge_styles_responsive');
	function tantum_vc_merge_styles_responsive( $list ) {
		$list[] = 'plugins/js_composer/js_composer-responsive.css';
		return $list;
	}
}

// Add VC custom styles to the inline CSS
if ( ! function_exists( 'tantum_vc_add_inline_css' ) ) {
	//Handler of the add_filter('tantum_filter_blog_archive_start', 'tantum_vc_add_inline_css');
	function tantum_vc_add_inline_css( $html ) {
		$vc_custom_css = get_post_meta( get_the_ID(), '_wpb_shortcodes_custom_css', true );
		if ( ! empty( $vc_custom_css ) ) {
			tantum_add_inline_css( strip_tags( $vc_custom_css ) );
		}
		return $html;
	}
}



// Shortcodes support
//------------------------------------------------------------------------

// Add params to the standard VC shortcodes
if ( ! function_exists( 'tantum_vc_add_params_classes' ) ) {
	//Handler of the add_filter( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tantum_vc_add_params_classes', 10, 3 );
	function tantum_vc_add_params_classes( $classes, $sc, $atts ) {
		// Add color scheme
		if ( in_array( $sc, apply_filters( 'tantum_filter_add_scheme_in_vc', array( 'vc_section', 'vc_row', 'vc_row_inner', 'vc_column', 'vc_column_inner', 'vc_column_text' ) ) ) ) {
			if ( ! empty( $atts['scheme'] ) && ! tantum_is_inherit( $atts['scheme'] ) ) {
				$classes .= ( $classes ? ' ' : '' ) . 'scheme_' . $atts['scheme'];
			}
		}
		return $classes;
	}
}

// Add theme icons to the VC iconpicker list
if ( ! function_exists( 'tantum_vc_iconpicker_type_fontawesome' ) ) {
	//Handler of the add_filter( 'vc_iconpicker-type-fontawesome',	'tantum_vc_iconpicker_type_fontawesome' );
	function tantum_vc_iconpicker_type_fontawesome( $icons ) {
		$list = tantum_get_list_icons_classes();
		if ( ! is_array( $list ) || count( $list ) == 0 ) {
			return $icons;
		}
		$rez = array();
		foreach ( $list as $icon ) {
			$rez[] = array( $icon => str_replace( 'icon-', '', $icon ) );
		}
		return array_merge( $icons, array( esc_html__( 'Theme Icons', 'tantum' ) => $rez ) );
	}
}


// Add plugin-specific colors and fonts to the custom CSS
if ( tantum_exists_vc() ) {
	require_once TANTUM_THEME_DIR . 'plugins/js_composer/js_composer-styles.php'; }

