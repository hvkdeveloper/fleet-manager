<?php
/* Essential Grid support functions
------------------------------------------------------------------------------- */


// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if ( ! function_exists( 'tantum_essential_grid_theme_setup9' ) ) {
	add_action( 'after_setup_theme', 'tantum_essential_grid_theme_setup9', 9 );
	function tantum_essential_grid_theme_setup9() {
		if ( tantum_exists_essential_grid() ) {
			add_action( 'wp_enqueue_scripts', 'tantum_essential_grid_frontend_scripts', 1100 );
			add_filter( 'tantum_filter_merge_styles', 'tantum_essential_grid_merge_styles' );
		}
		if ( is_admin() ) {
			add_filter( 'tantum_filter_tgmpa_required_plugins', 'tantum_essential_grid_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( ! function_exists( 'tantum_essential_grid_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('tantum_filter_tgmpa_required_plugins',	'tantum_essential_grid_tgmpa_required_plugins');
	function tantum_essential_grid_tgmpa_required_plugins( $list = array() ) {
		if ( tantum_storage_isset( 'required_plugins', 'essential-grid' ) && tantum_storage_get_array( 'required_plugins', 'essential-grid', 'install' ) !== false && tantum_is_theme_activated() ) {
			$path = tantum_get_plugin_source_path( 'plugins/essential-grid/essential-grid.zip' );
			if ( ! empty( $path ) || tantum_get_theme_setting( 'tgmpa_upload' ) ) {
				$list[] = array(
					'name'     => tantum_storage_get_array( 'required_plugins', 'essential-grid', 'title' ),
					'slug'     => 'essential-grid',
					'source'   => ! empty( $path ) ? $path : 'upload://essential-grid.zip',

					'required' => false,
				);
			}
		}
		return $list;
	}
}

// Check if plugin installed and activated
if ( ! function_exists( 'tantum_exists_essential_grid' ) ) {
	function tantum_exists_essential_grid() {
		return defined( 'EG_PLUGIN_PATH' );
	}
}

// Enqueue styles for frontend
if ( ! function_exists( 'tantum_essential_grid_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'tantum_essential_grid_frontend_scripts', 1100 );
	function tantum_essential_grid_frontend_scripts() {
		if ( tantum_is_on( tantum_get_theme_option( 'debug_mode' ) ) ) {
			$tantum_url = tantum_get_file_url( 'plugins/essential-grid/essential-grid.css' );
			if ( '' != $tantum_url ) {
				wp_enqueue_style( 'tantum-essential-grid', $tantum_url, array(), null );
			}
		}
	}
}

// Merge custom styles
if ( ! function_exists( 'tantum_essential_grid_merge_styles' ) ) {
	//Handler of the add_filter('tantum_filter_merge_styles', 'tantum_essential_grid_merge_styles');
	function tantum_essential_grid_merge_styles( $list ) {
		$list[] = 'plugins/essential-grid/essential-grid.css';
		return $list;
	}
}

