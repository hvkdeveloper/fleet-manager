<?php
/* WP GDPR Compliance support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if ( ! function_exists( 'tantum_wp_gdpr_compliance_feed_theme_setup9' ) ) {
	add_action( 'after_setup_theme', 'tantum_wp_gdpr_compliance_theme_setup9', 9 );
	function tantum_wp_gdpr_compliance_theme_setup9() {
		if ( is_admin() ) {
			add_filter( 'tantum_filter_tgmpa_required_plugins', 'tantum_wp_gdpr_compliance_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( ! function_exists( 'tantum_wp_gdpr_compliance_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('tantum_filter_tgmpa_required_plugins',	'tantum_wp_gdpr_compliance_tgmpa_required_plugins');
	function tantum_wp_gdpr_compliance_tgmpa_required_plugins( $list = array() ) {
		if ( tantum_storage_isset( 'required_plugins', 'wp-gdpr-compliance' ) && tantum_storage_get_array( 'required_plugins', 'wp-gdpr-compliance', 'install' ) !== false ) {
			$list[] = array(
				'name'     => tantum_storage_get_array( 'required_plugins', 'wp-gdpr-compliance', 'title' ),
				'slug'     => 'wp-gdpr-compliance',
				'required' => false,
			);
		}
		return $list;
	}
}

// Check if this plugin installed and activated
if ( ! function_exists( 'tantum_exists_wp_gdpr_compliance' ) ) {
	function tantum_exists_wp_gdpr_compliance() {
		return class_exists( 'WPGDPRC\WPGDPRC' );
	}
}
