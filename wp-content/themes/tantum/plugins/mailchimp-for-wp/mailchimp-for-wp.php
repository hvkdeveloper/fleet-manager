<?php
/* Mail Chimp support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if ( ! function_exists( 'tantum_mailchimp_theme_setup9' ) ) {
	add_action( 'after_setup_theme', 'tantum_mailchimp_theme_setup9', 9 );
	function tantum_mailchimp_theme_setup9() {
		if ( tantum_exists_mailchimp() ) {
			add_action( 'wp_enqueue_scripts', 'tantum_mailchimp_frontend_scripts', 1100 );
			add_filter( 'tantum_filter_merge_styles', 'tantum_mailchimp_merge_styles' );
		}
		if ( is_admin() ) {
			add_filter( 'tantum_filter_tgmpa_required_plugins', 'tantum_mailchimp_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( ! function_exists( 'tantum_mailchimp_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('tantum_filter_tgmpa_required_plugins',	'tantum_mailchimp_tgmpa_required_plugins');
	function tantum_mailchimp_tgmpa_required_plugins( $list = array() ) {
		if ( tantum_storage_isset( 'required_plugins', 'mailchimp-for-wp' ) && tantum_storage_get_array( 'required_plugins', 'mailchimp-for-wp', 'install' ) !== false ) {
			$list[] = array(
				'name'     => tantum_storage_get_array( 'required_plugins', 'mailchimp-for-wp', 'title' ),
				'slug'     => 'mailchimp-for-wp',
				'required' => false,
			);
		}
		return $list;
	}
}

// Check if plugin installed and activated
if ( ! function_exists( 'tantum_exists_mailchimp' ) ) {
	function tantum_exists_mailchimp() {
		return function_exists( '__mc4wp_load_plugin' ) || defined( 'MC4WP_VERSION' );
	}
}



// Custom styles and scripts
//------------------------------------------------------------------------

// Enqueue styles for frontend
if ( ! function_exists( 'tantum_mailchimp_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'tantum_mailchimp_frontend_scripts', 1100 );
	function tantum_mailchimp_frontend_scripts() {
		if ( tantum_is_on( tantum_get_theme_option( 'debug_mode' ) ) ) {
			$tantum_url = tantum_get_file_url( 'plugins/mailchimp-for-wp/mailchimp-for-wp.css' );
			if ( '' != $tantum_url ) {
				wp_enqueue_style( 'tantum-mailchimp', $tantum_url, array(), null );
			}
		}
	}
}

// Merge custom styles
if ( ! function_exists( 'tantum_mailchimp_merge_styles' ) ) {
	//Handler of the add_filter( 'tantum_filter_merge_styles', 'tantum_mailchimp_merge_styles');
	function tantum_mailchimp_merge_styles( $list ) {
		$list[] = 'plugins/mailchimp-for-wp/mailchimp-for-wp.css';
		return $list;
	}
}


// Add plugin-specific colors and fonts to the custom CSS
if ( tantum_exists_mailchimp() ) {
	require_once TANTUM_THEME_DIR . 'plugins/mailchimp-for-wp/mailchimp-for-wp-styles.php'; }

