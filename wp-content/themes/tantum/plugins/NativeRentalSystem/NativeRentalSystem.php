<?php
/* Revolution Slider support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 1 - register filters, that add/remove lists items for the Theme Options
if ( ! function_exists( 'tantum_NativeRentalSystem_theme_setup1' ) ) {
    add_action( 'after_setup_theme', 'tantum_NativeRentalSystem_theme_setup1', 1 );
    function tantum_NativeRentalSystem_theme_setup1() {
        add_filter( 'tantum_filter_detect_blog_mode', 'tantum_NativeRentalSystem_detect_blog_mode' );
    }
}

// Theme init priorities:
// 3 - add/remove Theme Options elements
if ( ! function_exists( 'tantum_NativeRentalSystem_theme_setup3' ) ) {
    add_action( 'after_setup_theme', 'tantum_NativeRentalSystem_theme_setup3', 3 );
    function tantum_NativeRentalSystem_theme_setup3() {
        if (  tantum_exists_NativeRentalSystem() ) {
            // Section 'Native Rental System'
            $new_option = tantum_options_get_list_cpt_options( 'vehicle' );

            unset($new_option['sidebar_position_vehicle']);
            unset($new_option['expand_content_vehicle']);
            unset($new_option['sidebar_widgets_vehicle']);
            unset($new_option['sidebar_position_ss_vehicle']);

            tantum_storage_merge_array(
                'options', '', array_merge(
                    array(
                        'vehicle'     => array(
                            'title' => esc_html__( 'Native Rental System', 'tantum' ),
                            'desc'  => wp_kses_data( __( 'Select parameters to display the single vehicle', 'tantum' ) ),
                            'type'  => 'section',
                        ),
                    ),
                    $new_option
                )
            );
        }
    }
}



// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if ( ! function_exists( 'tantum_NativeRentalSystem_theme_setup9' ) ) {
	add_action( 'after_setup_theme', 'tantum_NativeRentalSystem_theme_setup9', 9 );
	function tantum_NativeRentalSystem_theme_setup9() {
		if ( is_admin() ) {
			add_filter( 'tantum_filter_tgmpa_required_plugins', 'tantum_NativeRentalSystem_tgmpa_required_plugins' );
            add_filter( 'trx_addons_cpt_list_options', 'tantum_trx_addons_cpt_list_options', 10, 4 );
		}
	}
}

// Filter to add in the required plugins list
if ( ! function_exists( 'tantum_NativeRentalSystem_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('tantum_filter_tgmpa_required_plugins',	'tantum_NativeRentalSystem_tgmpa_required_plugins');
	function tantum_NativeRentalSystem_tgmpa_required_plugins( $list = array() ) {
		if ( tantum_storage_isset( 'required_plugins', 'NativeRentalSystem' ) && tantum_storage_get_array( 'required_plugins', 'NativeRentalSystem', 'install' ) !== false && tantum_is_theme_activated() ) {
			$path = tantum_get_plugin_source_path( 'plugins/NativeRentalSystem/NativeRentalSystem.zip' );
			if ( ! empty( $path ) || tantum_get_theme_setting( 'tgmpa_upload' ) ) {
				$list[] = array(
					'name'     => tantum_storage_get_array( 'required_plugins', 'NativeRentalSystem', 'title' ),
					'slug'     => 'NativeRentalSystem',
                    'source'   => ! empty( $path ) ? $path : 'upload://NativeRentalSystem.zip',
					'version'  => '5.0',
					'required' => false,
				);
			}
		}
		return $list;
	}
}

// Check if plugin installed and activated
if ( ! function_exists( 'tantum_exists_NativeRentalSystem' ) ) {
    function tantum_exists_NativeRentalSystem() {
        return function_exists( 'uninstall__CarRentalSystem' );
    }
}

// Return true, if current page is any Native Rental System page
if ( ! function_exists( 'tantum_is_NativeRentalSystem_page' ) ) {
    function tantum_is_NativeRentalSystem_page() {
        $rez = false;
        if ( tantum_exists_NativeRentalSystem() ) {
            $rez =  is_singular( 'car_rental_item' );
        }
        return $rez;
    }
}

// Detect current blog mode
if ( ! function_exists( 'tantum_NativeRentalSystem_detect_blog_mode' ) ) {
    //Handler of the add_filter( 'tantum_filter_detect_blog_mode', 'tantum_NativeRentalSystem_detect_blog_mode' );
    function tantum_NativeRentalSystem_detect_blog_mode( $mode = '' ) {
        if ( tantum_is_NativeRentalSystem_page() ) {
            $mode = 'vehicle';
        }
        return $mode;
    }
}

