<?php
/**
 * The custom template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.50
 */

$tantum_template_args = get_query_var( 'tantum_template_args' );
if ( is_array( $tantum_template_args ) ) {
	$tantum_columns    = empty( $tantum_template_args['columns'] ) ? 2 : max( 1, $tantum_template_args['columns'] );
	$tantum_blog_style = array( $tantum_template_args['type'], $tantum_columns );
} else {
	$tantum_blog_style = explode( '_', tantum_get_theme_option( 'blog_style' ) );
	$tantum_columns    = empty( $tantum_blog_style[1] ) ? 2 : max( 1, $tantum_blog_style[1] );
}
$tantum_blog_id       = tantum_get_custom_blog_id( join( '_', $tantum_blog_style ) );
$tantum_blog_style[0] = str_replace( 'blog-custom-', '', $tantum_blog_style[0] );
$tantum_expanded      = ! tantum_sidebar_present() && tantum_is_on( tantum_get_theme_option( 'expand_content' ) );
$tantum_components    = tantum_array_get_keys_by_value( tantum_get_theme_option( 'meta_parts' ) );

$tantum_post_format   = get_post_format();
$tantum_post_format   = empty( $tantum_post_format ) ? 'standard' : str_replace( 'post-format-', '', $tantum_post_format );

$tantum_blog_meta     = tantum_get_custom_layout_meta( $tantum_blog_id );
$tantum_custom_style  = ! empty( $tantum_blog_meta['scripts_required'] ) ? $tantum_blog_meta['scripts_required'] : 'none';

if ( ! empty( $tantum_template_args['slider'] ) || $tantum_columns > 1 || ! tantum_is_off( $tantum_custom_style ) ) {
	?><div class="
		<?php
		if ( ! empty( $tantum_template_args['slider'] ) ) {
			echo 'slider-slide swiper-slide';
		} else {
			echo esc_attr( ( tantum_is_off( $tantum_custom_style ) ? 'column' : sprintf( '%1$s_item %1$s_item', $tantum_custom_style ) ) . "-1_{$tantum_columns}" );
		}
		?>
	">
	<?php
}
?>
<article id="post-<?php the_ID(); ?>" data-post-id="<?php the_ID(); ?>"
	<?php
	post_class(
			'post_item post_format_' . esc_attr( $tantum_post_format )
					. ' post_layout_custom post_layout_custom_' . esc_attr( $tantum_columns )
					. ' post_layout_' . esc_attr( $tantum_blog_style[0] )
					. ' post_layout_' . esc_attr( $tantum_blog_style[0] ) . '_' . esc_attr( $tantum_columns )
					. ( ! tantum_is_off( $tantum_custom_style )
						? ' post_layout_' . esc_attr( $tantum_custom_style )
							. ' post_layout_' . esc_attr( $tantum_custom_style ) . '_' . esc_attr( $tantum_columns )
						: ''
						)
		);
	tantum_add_blog_animation( $tantum_template_args );
	?>
>
	<?php
	// Sticky label
	if ( is_sticky() && ! is_paged() ) {
		?>
		<span class="post_label label_sticky"></span>
		<?php
	}
	// Custom layout
	do_action( 'tantum_action_show_layout', $tantum_blog_id, get_the_ID() );
	?>
</article><?php
if ( ! empty( $tantum_template_args['slider'] ) || $tantum_columns > 1 || ! tantum_is_off( $tantum_custom_style ) ) {
	?></div><?php
	// Need opening PHP-tag above just after </div>, because <div> is a inline-block element (used as column)!
}
