<?php
/**
 * The Portfolio template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

$tantum_template_args = get_query_var( 'tantum_template_args' );
if ( is_array( $tantum_template_args ) ) {
	$tantum_columns    = empty( $tantum_template_args['columns'] ) ? 2 : max( 1, $tantum_template_args['columns'] );
	$tantum_blog_style = array( $tantum_template_args['type'], $tantum_columns );
} else {
	$tantum_blog_style = explode( '_', tantum_get_theme_option( 'blog_style' ) );
	$tantum_columns    = empty( $tantum_blog_style[1] ) ? 2 : max( 1, $tantum_blog_style[1] );
}
$tantum_post_format = get_post_format();
$tantum_post_format = empty( $tantum_post_format ) ? 'standard' : str_replace( 'post-format-', '', $tantum_post_format );

?><div class="
<?php
if ( ! empty( $tantum_template_args['slider'] ) ) {
	echo ' slider-slide swiper-slide';
} else {
	echo 'masonry_item masonry_item-1_' . esc_attr( $tantum_columns );
}
?>
"><article id="post-<?php the_ID(); ?>" 
	<?php
	post_class(
		'post_item post_format_' . esc_attr( $tantum_post_format )
		. ' post_layout_portfolio'
		. ' post_layout_portfolio_' . esc_attr( $tantum_columns )
		. ( is_sticky() && ! is_paged() ? ' sticky' : '' )
	);
	tantum_add_blog_animation( $tantum_template_args );
	?>
>
<?php

// Sticky label
if ( is_sticky() && ! is_paged() ) {
	?>
		<span class="post_label label_sticky"></span>
		<?php
}

	$tantum_image_hover = ! empty( $tantum_template_args['hover'] ) && ! tantum_is_inherit( $tantum_template_args['hover'] )
								? $tantum_template_args['hover']
								: tantum_get_theme_option( 'image_hover' );
	// Featured image
	tantum_show_post_featured(
		array(
			'hover'         => $tantum_image_hover,
			'no_links'      => ! empty( $tantum_template_args['no_links'] ),
			'thumb_size'    => tantum_get_thumb_size(
				strpos( tantum_get_theme_option( 'body_style' ), 'full' ) !== false || $tantum_columns < 3
								? 'masonry-big'
				: 'masonry'
			),
			'show_no_image' => true,
			'class'         => 'dots' == $tantum_image_hover ? 'hover_with_info' : '',
			'post_info'     => 'dots' == $tantum_image_hover ? '<div class="post_info">' . esc_html( get_the_title() ) . '</div>' : '',
		)
	);
	?>
</article></div><?php
// Need opening PHP-tag above, because <article> is a inline-block element (used as column)!