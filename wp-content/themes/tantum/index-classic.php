<?php
/**
 * The template for homepage posts with "Classic" style
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

tantum_storage_set( 'blog_archive', true );

get_header();

if ( have_posts() ) {

	tantum_blog_archive_start();

	$tantum_classes    = 'posts_container '
						. ( substr( tantum_get_theme_option( 'blog_style' ), 0, 7 ) == 'classic'
							? 'columns_wrap columns_padding_bottom'
							: 'masonry_wrap'
							);
	$tantum_stickies   = is_home() ? get_option( 'sticky_posts' ) : false;
	$tantum_sticky_out = tantum_get_theme_option( 'sticky_style' ) == 'columns'
							&& is_array( $tantum_stickies ) && count( $tantum_stickies ) > 0 && get_query_var( 'paged' ) < 1;
	if ( $tantum_sticky_out ) {
		?>
		<div class="sticky_wrap columns_wrap">
		<?php
	}
	if ( ! $tantum_sticky_out ) {
		if ( tantum_get_theme_option( 'first_post_large' ) && ! is_paged() && ! in_array( tantum_get_theme_option( 'body_style' ), array( 'fullwide', 'fullscreen' ) ) ) {
			the_post();
			get_template_part( apply_filters( 'tantum_filter_get_template_part', 'content', 'excerpt' ), 'excerpt' );
		}

		?>
		<div class="<?php echo esc_attr( $tantum_classes ); ?>">
		<?php
	}
	while ( have_posts() ) {
		the_post();
		if ( $tantum_sticky_out && ! is_sticky() ) {
			$tantum_sticky_out = false;
			?>
			</div><div class="<?php echo esc_attr( $tantum_classes ); ?>">
			<?php
		}
		$tantum_part = $tantum_sticky_out && is_sticky() ? 'sticky' : 'classic';
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'content', $tantum_part ), $tantum_part );
	}

	?>
	</div>
	<?php

	tantum_show_pagination();

	tantum_blog_archive_end();

} else {

	if ( is_search() ) {
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'content', 'none-search' ), 'none-search' );
	} else {
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'content', 'none-archive' ), 'none-archive' );
	}
}

get_footer();
