<?php
/**
 * The default template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

$tantum_template_args = get_query_var( 'tantum_template_args' );
if ( is_array( $tantum_template_args ) ) {
	$tantum_columns    = empty( $tantum_template_args['columns'] ) ? 1 : max( 1, $tantum_template_args['columns'] );
	$tantum_blog_style = array( $tantum_template_args['type'], $tantum_columns );
	if ( ! empty( $tantum_template_args['slider'] ) ) {
		?><div class="slider-slide swiper-slide">
		<?php
	} elseif ( $tantum_columns > 1 ) {
		?>
		<div class="column-1_<?php echo esc_attr( $tantum_columns ); ?>">
		<?php
	}
}
$tantum_expanded    = ! tantum_sidebar_present() && tantum_is_on( tantum_get_theme_option( 'expand_content' ) );
$tantum_post_format = get_post_format();
$tantum_post_format = empty( $tantum_post_format ) ? 'standard' : str_replace( 'post-format-', '', $tantum_post_format );
?>
<article id="post-<?php the_ID(); ?>" data-post-id="<?php the_ID(); ?>"
	<?php
	post_class( 'post_item post_layout_excerpt post_format_' . esc_attr( $tantum_post_format ) );
	tantum_add_blog_animation( $tantum_template_args );
	?>
>
	<?php

	// Sticky label
	if ( is_sticky() && ! is_paged() ) {
		?>
		<span class="post_label label_sticky"><?php esc_html_e('sticky post', 'tantum' ) ?></span>
		<?php
	}

	// Featured image
	$tantum_hover = ! empty( $tantum_template_args['hover'] ) && ! tantum_is_inherit( $tantum_template_args['hover'] )
						? $tantum_template_args['hover']
						: tantum_get_theme_option( 'image_hover' );
	tantum_show_post_featured(
		array(
			'no_links'   => ! empty( $tantum_template_args['no_links'] ),
			'hover'      => $tantum_hover,
			'thumb_size' => tantum_get_thumb_size( strpos( tantum_get_theme_option( 'body_style' ), 'full' ) !== false ? 'full' : ( $tantum_expanded ? 'huge' : 'huge' ) ),
		)
	);

	// Title and post meta
	$tantum_show_title = get_the_title() != '';
	$tantum_components = tantum_array_get_keys_by_value( tantum_get_theme_option( 'meta_parts' ) );
	$tantum_show_meta  = ! empty( $tantum_components ) && ! in_array( $tantum_hover, array( 'border', 'pull', 'slide', 'fade' ) );
	if ( $tantum_show_title || $tantum_show_meta ) {
        if($tantum_post_format != 'quote'){
	    ?>
		<div class="post_header entry-header">
			<?php

                // Post meta
                if ( $tantum_show_meta ) {
                    do_action( 'tantum_action_before_post_meta' );
                    tantum_show_post_meta(
                        apply_filters(
                            'tantum_filter_post_meta_args', array(
                                'components' => $tantum_components,
                                'seo'        => false,
                            ), 'excerpt', 1
                        )
                    );
                }
                // Post title
                if ( $tantum_show_title ) {
                    do_action( 'tantum_action_before_post_title' );
                    if ( empty( $tantum_template_args['no_links'] ) ) {
                        the_title( sprintf( '<h2 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                    } else {
                        the_title( '<h2 class="post_title entry-title">', '</h2>' );
                    }
                }
            ?>
        </div><!-- .post_header -->
        <?php
        }

	}

	// Post content
	if ( empty( $tantum_template_args['hide_excerpt'] ) && tantum_get_theme_option( 'excerpt_length' ) > 0 ) {
		?>
		<div class="post_content entry-content">
			<?php
			if ( tantum_get_theme_option( 'blog_content' ) == 'fullpost' ) {
				// Post content area
				?>
				<div class="post_content_inner">
					<?php
					do_action( 'tantum_action_before_full_post_content' );
					the_content( '' );
					do_action( 'tantum_action_after_full_post_content' );
					?>
				</div>
				<?php
				// Inner pages
				wp_link_pages(
					array(
						'before'      => '<div class="page_links"><span class="page_links_title">' . esc_html__( 'Pages:', 'tantum' ) . '</span>',
						'after'       => '</div>',
						'link_before' => '<span>',
						'link_after'  => '</span>',
						'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'tantum' ) . ' </span>%',
						'separator'   => '<span class="screen-reader-text">, </span>',
					)
				);
			} else {
				// Post content area
				tantum_show_post_content( $tantum_template_args, '<div class="post_content_inner">', '</div>' );

                    if( $tantum_post_format == 'quote' ){
                    ?>
                    <div class="post_header entry-header">
                        <?php

                            // Post meta
                            if ( $tantum_show_meta ) {
                                do_action( 'tantum_action_before_post_meta' );
                                tantum_show_post_meta(
                                    apply_filters(
                                        'tantum_filter_post_meta_args', array(
                                            'components' => $tantum_components,
                                            'seo'        => false,
                                        ), 'excerpt', 1
                                    )
                                );
                    }
                    // Post title
                    if ( $tantum_show_title ) {
                        do_action( 'tantum_action_before_post_title' );
                        if ( empty( $tantum_template_args['no_links'] ) ) {
                            the_title( sprintf( '<h2 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                        } else {
                            the_title( '<h2 class="post_title entry-title">', '</h2>' );
                        }
                    }
                        ?>
                    </div><!-- .post_header -->

                <?php
                }

				// More button
				if ( empty( $tantum_template_args['no_links'] ) && ! in_array( $tantum_post_format, array( 'link', 'aside', 'status' ) ) ) {
					tantum_show_post_more_link( $tantum_template_args, '<p>', '</p>' );
				}
			}
			?>
		</div><!-- .entry-content -->
		<?php
	}
	?>
</article>
<?php

if ( is_array( $tantum_template_args ) ) {
	if ( ! empty( $tantum_template_args['slider'] ) || $tantum_columns > 1 ) {
		?>
		</div>
		<?php
	}
}
