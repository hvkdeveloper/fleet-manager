<div class="front_page_section front_page_section_blog<?php
	$tantum_scheme = tantum_get_theme_option( 'front_page_blog_scheme' );
	if ( ! empty( $tantum_scheme ) && ! tantum_is_inherit( $tantum_scheme ) ) {
		echo ' scheme_' . esc_attr( $tantum_scheme );
	}
	echo ' front_page_section_paddings_' . esc_attr( tantum_get_theme_option( 'front_page_blog_paddings' ) );
	if ( tantum_get_theme_option( 'front_page_blog_stack' ) ) {
		echo ' sc_stack_section_on';
	}
?>"
		<?php
		$tantum_css      = '';
		$tantum_bg_image = tantum_get_theme_option( 'front_page_blog_bg_image' );
		if ( ! empty( $tantum_bg_image ) ) {
			$tantum_css .= 'background-image: url(' . esc_url( tantum_get_attachment_url( $tantum_bg_image ) ) . ');';
		}
		if ( ! empty( $tantum_css ) ) {
			echo ' style="' . esc_attr( $tantum_css ) . '"';
		}
		?>
>
<?php
	// Add anchor
	$tantum_anchor_icon = tantum_get_theme_option( 'front_page_blog_anchor_icon' );
	$tantum_anchor_text = tantum_get_theme_option( 'front_page_blog_anchor_text' );
if ( ( ! empty( $tantum_anchor_icon ) || ! empty( $tantum_anchor_text ) ) && shortcode_exists( 'trx_sc_anchor' ) ) {
	echo do_shortcode(
		'[trx_sc_anchor id="front_page_section_blog"'
									. ( ! empty( $tantum_anchor_icon ) ? ' icon="' . esc_attr( $tantum_anchor_icon ) . '"' : '' )
									. ( ! empty( $tantum_anchor_text ) ? ' title="' . esc_attr( $tantum_anchor_text ) . '"' : '' )
									. ']'
	);
}
?>
	<div class="front_page_section_inner front_page_section_blog_inner
	<?php
	if ( tantum_get_theme_option( 'front_page_blog_fullheight' ) ) {
		echo ' tantum-full-height sc_layouts_flex sc_layouts_columns_middle';
	}
	?>
			"
			<?php
			$tantum_css      = '';
			$tantum_bg_mask  = tantum_get_theme_option( 'front_page_blog_bg_mask' );
			$tantum_bg_color_type = tantum_get_theme_option( 'front_page_blog_bg_color_type' );
			if ( 'custom' == $tantum_bg_color_type ) {
				$tantum_bg_color = tantum_get_theme_option( 'front_page_blog_bg_color' );
			} elseif ( 'scheme_bg_color' == $tantum_bg_color_type ) {
				$tantum_bg_color = tantum_get_scheme_color( 'bg_color', $tantum_scheme );
			} else {
				$tantum_bg_color = '';
			}
			if ( ! empty( $tantum_bg_color ) && $tantum_bg_mask > 0 ) {
				$tantum_css .= 'background-color: ' . esc_attr(
					1 == $tantum_bg_mask ? $tantum_bg_color : tantum_hex2rgba( $tantum_bg_color, $tantum_bg_mask )
				) . ';';
			}
			if ( ! empty( $tantum_css ) ) {
				echo ' style="' . esc_attr( $tantum_css ) . '"';
			}
			?>
	>
		<div class="front_page_section_content_wrap front_page_section_blog_content_wrap content_wrap">
			<?php
			// Caption
			$tantum_caption = tantum_get_theme_option( 'front_page_blog_caption' );
			if ( ! empty( $tantum_caption ) || ( current_user_can( 'edit_theme_options' ) && is_customize_preview() ) ) {
				?>
				<h2 class="front_page_section_caption front_page_section_blog_caption front_page_block_<?php echo ! empty( $tantum_caption ) ? 'filled' : 'empty'; ?>"><?php echo wp_kses_post( $tantum_caption ); ?></h2>
				<?php
			}

			// Description (text)
			$tantum_description = tantum_get_theme_option( 'front_page_blog_description' );
			if ( ! empty( $tantum_description ) || ( current_user_can( 'edit_theme_options' ) && is_customize_preview() ) ) {
				?>
				<div class="front_page_section_description front_page_section_blog_description front_page_block_<?php echo ! empty( $tantum_description ) ? 'filled' : 'empty'; ?>"><?php echo wp_kses_post( wpautop( $tantum_description ) ); ?></div>
				<?php
			}

			// Content (widgets)
			?>
			<div class="front_page_section_output front_page_section_blog_output">
			<?php
			if ( is_active_sidebar( 'front_page_blog_widgets' ) ) {
				dynamic_sidebar( 'front_page_blog_widgets' );
			} elseif ( current_user_can( 'edit_theme_options' ) ) {
				if ( ! tantum_exists_trx_addons() ) {
					tantum_customizer_need_trx_addons_message();
				} else {
					tantum_customizer_need_widgets_message( 'front_page_blog_caption', 'ThemeREX Addons - Blogger' );
				}
			}
			?>
			</div>
		</div>
	</div>
</div>
