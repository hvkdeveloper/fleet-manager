<?php
/**
 * The template file to display taxonomies archive
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.57
 */

// Redirect to the template page (if exists) for output current taxonomy
if ( is_category() || is_tag() || is_tax() ) {
	$tantum_term = get_queried_object();
	global $wp_query;
	if ( ! empty( $tantum_term->taxonomy ) && ! empty( $wp_query->posts[0]->post_type ) ) {
		$tantum_taxonomy  = tantum_get_post_type_taxonomy( $wp_query->posts[0]->post_type );
		if ( $tantum_taxonomy == $tantum_term->taxonomy ) {
			$tantum_template_page_id = tantum_get_template_page_id( array(
				'post_type'  => $wp_query->posts[0]->post_type,
				'parent_cat' => $tantum_term->term_id
			) );
			if ( 0 < $tantum_template_page_id ) {
				wp_safe_redirect( get_permalink( $tantum_template_page_id ) );
				exit;
			}
		}
	}
}
// If template page is not exists - display default blog archive template
get_template_part( apply_filters( 'tantum_filter_get_template_part', tantum_blog_archive_get_template() ) );
