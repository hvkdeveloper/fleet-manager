<?php
/**
 * The template for homepage posts with "Portfolio" style
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

tantum_storage_set( 'blog_archive', true );

get_header();

if ( have_posts() ) {

	tantum_blog_archive_start();

	$tantum_stickies   = is_home() ? get_option( 'sticky_posts' ) : false;
	$tantum_sticky_out = tantum_get_theme_option( 'sticky_style' ) == 'columns'
							&& is_array( $tantum_stickies ) && count( $tantum_stickies ) > 0 && get_query_var( 'paged' ) < 1;

	// Show filters
	$tantum_cat          = tantum_get_theme_option( 'parent_cat' );
	$tantum_post_type    = tantum_get_theme_option( 'post_type' );
	$tantum_taxonomy     = tantum_get_post_type_taxonomy( $tantum_post_type );
	$tantum_show_filters = tantum_get_theme_option( 'show_filters' );
	$tantum_tabs         = array();
	if ( ! tantum_is_off( $tantum_show_filters ) ) {
		$tantum_args           = array(
			'type'         => $tantum_post_type,
			'child_of'     => $tantum_cat,
			'orderby'      => 'name',
			'order'        => 'ASC',
			'hide_empty'   => 1,
			'hierarchical' => 0,
			'taxonomy'     => $tantum_taxonomy,
			'pad_counts'   => false,
		);
		$tantum_portfolio_list = get_terms( $tantum_args );
		if ( is_array( $tantum_portfolio_list ) && count( $tantum_portfolio_list ) > 0 ) {
			$tantum_tabs[ $tantum_cat ] = esc_html__( 'All', 'tantum' );
			foreach ( $tantum_portfolio_list as $tantum_term ) {
				if ( isset( $tantum_term->term_id ) ) {
					$tantum_tabs[ $tantum_term->term_id ] = $tantum_term->name;
				}
			}
		}
	}
	if ( count( $tantum_tabs ) > 0 ) {
		$tantum_portfolio_filters_ajax   = true;
		$tantum_portfolio_filters_active = $tantum_cat;
		$tantum_portfolio_filters_id     = 'portfolio_filters';
		?>
		<div class="portfolio_filters tantum_tabs tantum_tabs_ajax">
			<ul class="portfolio_titles tantum_tabs_titles">
				<?php
				foreach ( $tantum_tabs as $tantum_id => $tantum_title ) {
					?>
					<li><a href="<?php echo esc_url( tantum_get_hash_link( sprintf( '#%s_%s_content', $tantum_portfolio_filters_id, $tantum_id ) ) ); ?>" data-tab="<?php echo esc_attr( $tantum_id ); ?>"><?php echo esc_html( $tantum_title ); ?></a></li>
					<?php
				}
				?>
			</ul>
			<?php
			$tantum_ppp = tantum_get_theme_option( 'posts_per_page' );
			if ( tantum_is_inherit( $tantum_ppp ) ) {
				$tantum_ppp = '';
			}
			foreach ( $tantum_tabs as $tantum_id => $tantum_title ) {
				$tantum_portfolio_need_content = $tantum_id == $tantum_portfolio_filters_active || ! $tantum_portfolio_filters_ajax;
				?>
				<div id="<?php echo esc_attr( sprintf( '%s_%s_content', $tantum_portfolio_filters_id, $tantum_id ) ); ?>"
					class="portfolio_content tantum_tabs_content"
					data-blog-template="<?php echo esc_attr( tantum_storage_get( 'blog_template' ) ); ?>"
					data-blog-style="<?php echo esc_attr( tantum_get_theme_option( 'blog_style' ) ); ?>"
					data-posts-per-page="<?php echo esc_attr( $tantum_ppp ); ?>"
					data-post-type="<?php echo esc_attr( $tantum_post_type ); ?>"
					data-taxonomy="<?php echo esc_attr( $tantum_taxonomy ); ?>"
					data-cat="<?php echo esc_attr( $tantum_id ); ?>"
					data-parent-cat="<?php echo esc_attr( $tantum_cat ); ?>"
					data-need-content="<?php echo ( false === $tantum_portfolio_need_content ? 'true' : 'false' ); ?>"
				>
					<?php
					if ( $tantum_portfolio_need_content ) {
						tantum_show_portfolio_posts(
							array(
								'cat'        => $tantum_id,
								'parent_cat' => $tantum_cat,
								'taxonomy'   => $tantum_taxonomy,
								'post_type'  => $tantum_post_type,
								'page'       => 1,
								'sticky'     => $tantum_sticky_out,
							)
						);
					}
					?>
				</div>
				<?php
			}
			?>
		</div>
		<?php
	} else {
		tantum_show_portfolio_posts(
			array(
				'cat'        => $tantum_cat,
				'parent_cat' => $tantum_cat,
				'taxonomy'   => $tantum_taxonomy,
				'post_type'  => $tantum_post_type,
				'page'       => 1,
				'sticky'     => $tantum_sticky_out,
			)
		);
	}

	tantum_blog_archive_end();

} else {

	if ( is_search() ) {
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'content', 'none-search' ), 'none-search' );
	} else {
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'content', 'none-archive' ), 'none-archive' );
	}
}

get_footer();
