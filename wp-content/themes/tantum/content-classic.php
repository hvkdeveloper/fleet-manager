<?php
/**
 * The Classic template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

$tantum_template_args = get_query_var( 'tantum_template_args' );
if ( is_array( $tantum_template_args ) ) {
	$tantum_columns    = empty( $tantum_template_args['columns'] ) ? 2 : max( 1, $tantum_template_args['columns'] );
	$tantum_blog_style = array( $tantum_template_args['type'], $tantum_columns );
} else {
	$tantum_blog_style = explode( '_', tantum_get_theme_option( 'blog_style' ) );
	$tantum_columns    = empty( $tantum_blog_style[1] ) ? 2 : max( 1, $tantum_blog_style[1] );
}
$tantum_expanded   = ! tantum_sidebar_present() && tantum_is_on( tantum_get_theme_option( 'expand_content' ) );
$tantum_components = tantum_array_get_keys_by_value( tantum_get_theme_option( 'meta_parts' ) );

$tantum_post_format = get_post_format();
$tantum_post_format = empty( $tantum_post_format ) ? 'standard' : str_replace( 'post-format-', '', $tantum_post_format );



    ?>
    <div class="
<?php
    if (!empty($tantum_template_args['slider'])) {
        echo ' slider-slide swiper-slide';
    } else {
        echo ('classic' == $tantum_blog_style[0] ? 'column' : 'masonry_item masonry_item') . '-1_' . esc_attr($tantum_columns);
    }
    ?>
">
    <article id="post-<?php the_ID(); ?>" data-post-id="<?php the_ID(); ?>"
        <?php
        post_class(
            'post_item post_format_' . esc_attr($tantum_post_format)
            . ' post_layout_classic post_layout_classic_' . esc_attr($tantum_columns)
            . ' post_layout_' . esc_attr($tantum_blog_style[0])
            . ' post_layout_' . esc_attr($tantum_blog_style[0]) . '_' . esc_attr($tantum_columns)
        );
        tantum_add_blog_animation($tantum_template_args);
        ?>
    >
        <?php
        // Sticky label
        if (is_sticky() && !is_paged()) {
            ?>
            <span class="post_label label_sticky"></span>
            <?php
        }

        // Featured image
        $tantum_hover = !empty($tantum_template_args['hover']) && !tantum_is_inherit($tantum_template_args['hover'])
            ? $tantum_template_args['hover']
            : tantum_get_theme_option('image_hover');
        tantum_show_post_featured(
            array(
                'thumb_size' => tantum_get_thumb_size(
                    'classic' == $tantum_blog_style[0]
                        ? (strpos(tantum_get_theme_option('body_style'), 'full') !== false
                        ? ($tantum_columns > 2 ? 'big' : 'huge')
                        : ($tantum_columns > 2
                            ? ($tantum_expanded ? 'med' : 'small')
                            : ($tantum_expanded ? 'big' : 'med')
                        )
                    )
                        : (strpos(tantum_get_theme_option('body_style'), 'full') !== false
                        ? ($tantum_columns > 2 ? 'masonry-big' : 'full')
                        : ($tantum_columns <= 2 && $tantum_expanded ? 'masonry-big' : 'masonry')
                    )
                ),
                'hover' => $tantum_hover,
                'no_links' => !empty($tantum_template_args['no_links']),
            )
        );

        if (!in_array($tantum_post_format, array('link', 'aside', 'status', 'quote'))) {
            ?>
            <div class="post_header entry-header">
                <?php
                do_action('tantum_action_before_post_title');

                // Post title
                if (empty($tantum_template_args['no_links'])) {
                    the_title(sprintf('<h4 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h4>');
                } else {
                    the_title('<h4 class="post_title entry-title">', '</h4>');
                }

                do_action('tantum_action_before_post_meta');

                // Post meta
                if (!empty($tantum_components) && !in_array($tantum_hover, array('border', 'pull', 'slide', 'fade'))) {
                    tantum_show_post_meta(
                        apply_filters(
                            'tantum_filter_post_meta_args', array(
                            'components' => $tantum_components,
                            'seo' => false,
                        ), $tantum_blog_style[0], $tantum_columns
                        )
                    );
                }

                do_action('tantum_action_after_post_meta');
                ?>
            </div><!-- .entry-header -->
            <?php
        }
        ?>

        <div class="post_content entry-content">
            <?php
            if (empty($tantum_template_args['hide_excerpt']) && tantum_get_theme_option('excerpt_length') > 0) {
                // Post content area
                tantum_show_post_content($tantum_template_args, '<div class="post_content_inner">', '</div>');
            }

            // Post meta
            if (in_array($tantum_post_format, array('link', 'aside', 'status', 'quote'))) {
                if (!empty($tantum_components)) {
                    tantum_show_post_meta(
                        apply_filters(
                            'tantum_filter_post_meta_args', array(
                            'components' => $tantum_components,
                        ), $tantum_blog_style[0], $tantum_columns
                        )
                    );
                }
            }

            // More button
            if (empty($tantum_template_args['no_links']) && !empty($tantum_template_args['more_text']) && !in_array($tantum_post_format, array('link', 'aside', 'status', 'quote'))) {
                tantum_show_post_more_link($tantum_template_args, '<p>', '</p>');
            }
            ?>
        </div><!-- .entry-content -->

    </article></div><?php

// Need opening PHP-tag above, because <div> is a inline-block element (used as column)!
