<?php
/**
 * The template to display blog archive
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

/*
Template Name: Blog archive
*/

/**
 * Make page with this template and put it into menu
 * to display posts as blog archive
 * You can setup output parameters (blog style, posts per page, parent category, etc.)
 * in the Theme Options section (under the page content)
 * You can build this page in the WordPress editor or any Page Builder to make custom page layout:
 * just insert %%CONTENT%% in the desired place of content
 */

if ( function_exists( 'tantum_elementor_is_preview' ) && tantum_elementor_is_preview() ) {

	// Redirect to the page
	get_template_part( apply_filters( 'tantum_filter_get_template_part', 'page' ) );

} else {

	// Store post with blog archive template
	if ( have_posts() ) {
		the_post();
		if ( isset( $GLOBALS['post'] ) && is_object( $GLOBALS['post'] ) ) {
			tantum_storage_set( 'blog_archive_template_post', $GLOBALS['post'] );
		}
	}

	// Prepare args for a new query
	$tantum_args        = array(
		'post_status' => current_user_can( 'read_private_pages' ) && current_user_can( 'read_private_posts' ) ? array( 'publish', 'private' ) : 'publish',
	);
	$tantum_args        = tantum_query_add_posts_and_cats( $tantum_args, '', tantum_get_theme_option( 'post_type' ), tantum_get_theme_option( 'parent_cat' ) );
	$tantum_page_number = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 );
	if ( $tantum_page_number > 1 ) {
		$tantum_args['paged']               = $tantum_page_number;
		$tantum_args['ignore_sticky_posts'] = true;
	}
	$tantum_ppp = tantum_get_theme_option( 'posts_per_page' );
	if ( 0 != (int) $tantum_ppp ) {
		$tantum_args['posts_per_page'] = (int) $tantum_ppp;
	}
	// Make a new main query
	$GLOBALS['wp_the_query']->query( $tantum_args );

	get_template_part( apply_filters( 'tantum_filter_get_template_part', tantum_blog_archive_get_template() ) );
}
