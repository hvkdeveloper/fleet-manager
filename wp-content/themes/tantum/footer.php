<?php
/**
 * The Footer: widgets area, logo, footer menu and socials
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

							// Widgets area inside page content
							tantum_create_widgets_area( 'widgets_below_content' );
							?>
						</div><!-- </.content> -->
					<?php

					// Show main sidebar
					get_sidebar();

					$tantum_body_style = tantum_get_theme_option( 'body_style' );
					?>
					</div><!-- </.content_wrap> -->
					<?php

					// Widgets area below page content and related posts below page content
					$tantum_widgets_name = tantum_get_theme_option( 'widgets_below_page' );
					$tantum_show_widgets = ! tantum_is_off( $tantum_widgets_name ) && is_active_sidebar( $tantum_widgets_name );
					$tantum_show_related = is_single() && tantum_get_theme_option( 'related_position' ) == 'below_page';
					if ( $tantum_show_widgets || $tantum_show_related ) {
						if ( 'fullscreen' != $tantum_body_style ) {
							?>
							<div class="content_wrap">
							<?php
						}
						// Show related posts before footer
						if ( $tantum_show_related ) {
							do_action( 'tantum_action_related_posts' );
						}

						// Widgets area below page content
						if ( $tantum_show_widgets ) {
							tantum_create_widgets_area( 'widgets_below_page' );
						}
						if ( 'fullscreen' != $tantum_body_style ) {
							?>
							</div><!-- </.content_wrap> -->
							<?php
						}
					}
					?>
			</div><!-- </.page_content_wrap> -->

			<?php
			// Single posts banner before footer
			if ( is_singular( 'post' ) ) {
				tantum_show_post_banner('footer');
			}
			
			// Skip link anchor to fast access to the footer from keyboard
			?>
			<a id="footer_skip_link_anchor" class="tantum_skip_link_anchor" href="#"></a>
			<?php
			
			// Footer
			$tantum_footer_type = tantum_get_theme_option( 'footer_type' );
			if ( 'custom' == $tantum_footer_type && ! tantum_is_layouts_available() ) {
				$tantum_footer_type = 'default';
			}
			get_template_part( apply_filters( 'tantum_filter_get_template_part', "templates/footer-{$tantum_footer_type}" ) );
			?>

		</div><!-- /.page_wrap -->

	</div><!-- /.body_wrap -->

	<?php wp_footer(); ?>

</body>
</html>