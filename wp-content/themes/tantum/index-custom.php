<?php
/**
 * The template for homepage posts with custom style
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0.50
 */

tantum_storage_set( 'blog_archive', true );

get_header();

if ( have_posts() ) {

	$tantum_blog_style = tantum_get_theme_option( 'blog_style' );
	$tantum_parts      = explode( '_', $tantum_blog_style );
	$tantum_columns    = ! empty( $tantum_parts[1] ) ? max( 1, min( 6, (int) $tantum_parts[1] ) ) : 1;
	$tantum_blog_id    = tantum_get_custom_blog_id( $tantum_blog_style );
	$tantum_blog_meta  = tantum_get_custom_layout_meta( $tantum_blog_id );
	if ( ! empty( $tantum_blog_meta['margin'] ) ) {
		tantum_add_inline_css( sprintf( '.page_content_wrap{padding-top:%s}', esc_attr( tantum_prepare_css_value( $tantum_blog_meta['margin'] ) ) ) );
	}
	$tantum_custom_style = ! empty( $tantum_blog_meta['scripts_required'] ) ? $tantum_blog_meta['scripts_required'] : 'none';

	tantum_blog_archive_start();

	$tantum_classes    = 'posts_container blog_custom_wrap' 
							. ( ! tantum_is_off( $tantum_custom_style )
								? sprintf( ' %s_wrap', $tantum_custom_style )
								: ( $tantum_columns > 1 
									? ' columns_wrap columns_padding_bottom' 
									: ''
									)
								);
	$tantum_stickies   = is_home() ? get_option( 'sticky_posts' ) : false;
	$tantum_sticky_out = tantum_get_theme_option( 'sticky_style' ) == 'columns'
							&& is_array( $tantum_stickies ) && count( $tantum_stickies ) > 0 && get_query_var( 'paged' ) < 1;
	if ( $tantum_sticky_out ) {
		?>
		<div class="sticky_wrap columns_wrap">
		<?php
	}
	if ( ! $tantum_sticky_out ) {
		if ( tantum_get_theme_option( 'first_post_large' ) && ! is_paged() && ! in_array( tantum_get_theme_option( 'body_style' ), array( 'fullwide', 'fullscreen' ) ) ) {
			the_post();
			get_template_part( apply_filters( 'tantum_filter_get_template_part', 'content', 'excerpt' ), 'excerpt' );
		}
		?>
		<div class="<?php echo esc_attr( $tantum_classes ); ?>">
		<?php
	}
	while ( have_posts() ) {
		the_post();
		if ( $tantum_sticky_out && ! is_sticky() ) {
			$tantum_sticky_out = false;
			?>
			</div><div class="<?php echo esc_attr( $tantum_classes ); ?>">
			<?php
		}
		$tantum_part = $tantum_sticky_out && is_sticky() ? 'sticky' : 'custom';
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'content', $tantum_part ), $tantum_part );
	}
	?>
	</div>
	<?php

	tantum_show_pagination();

	tantum_blog_archive_end();

} else {

	if ( is_search() ) {
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'content', 'none-search' ), 'none-search' );
	} else {
		get_template_part( apply_filters( 'tantum_filter_get_template_part', 'content', 'none-archive' ), 'none-archive' );
	}
}

get_footer();
