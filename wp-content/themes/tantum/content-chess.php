<?php
/**
 * The Classic template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */

$tantum_template_args = get_query_var( 'tantum_template_args' );
if ( is_array( $tantum_template_args ) ) {
	$tantum_columns    = empty( $tantum_template_args['columns'] ) ? 1 : max( 1, min( 3, $tantum_template_args['columns'] ) );
	$tantum_blog_style = array( $tantum_template_args['type'], $tantum_columns );
} else {
	$tantum_blog_style = explode( '_', tantum_get_theme_option( 'blog_style' ) );
	$tantum_columns    = empty( $tantum_blog_style[1] ) ? 1 : max( 1, min( 3, $tantum_blog_style[1] ) );
}
$tantum_expanded    = ! tantum_sidebar_present() && tantum_is_on( tantum_get_theme_option( 'expand_content' ) );
$tantum_post_format = get_post_format();
$tantum_post_format = empty( $tantum_post_format ) ? 'standard' : str_replace( 'post-format-', '', $tantum_post_format );

?><article id="post-<?php the_ID(); ?>"	data-post-id="<?php the_ID(); ?>"
	<?php
	post_class(
		'post_item'
		. ' post_layout_chess'
		. ' post_layout_chess_' . esc_attr( $tantum_columns )
		. ' post_format_' . esc_attr( $tantum_post_format )
		. ( ! empty( $tantum_template_args['slider'] ) ? ' slider-slide swiper-slide' : '' )
	);
	tantum_add_blog_animation( $tantum_template_args );
	?>
>

	<?php
	// Add anchor
	if ( 1 == $tantum_columns && ! is_array( $tantum_template_args ) && shortcode_exists( 'trx_sc_anchor' ) ) {
		echo do_shortcode( '[trx_sc_anchor id="post_' . esc_attr( get_the_ID() ) . '" title="' . esc_attr( get_the_title() ) . '" icon="' . esc_attr( tantum_get_post_icon() ) . '"]' );
	}

	// Sticky label
	if ( is_sticky() && ! is_paged() ) {
		?>
		<span class="post_label label_sticky"></span>
		<?php
	}

	// Featured image
	$tantum_hover = ! empty( $tantum_template_args['hover'] ) && ! tantum_is_inherit( $tantum_template_args['hover'] )
						? $tantum_template_args['hover']
						: tantum_get_theme_option( 'image_hover' );
	tantum_show_post_featured(
		array(
			'class'         => 1 == $tantum_columns && ! is_array( $tantum_template_args ) ? 'tantum-full-height' : '',
			'hover'         => $tantum_hover,
			'no_links'      => ! empty( $tantum_template_args['no_links'] ),
			'show_no_image' => true,
			'thumb_ratio'   => '1:1',
			'thumb_bg'      => true,
			'thumb_size'    => tantum_get_thumb_size(
				strpos( tantum_get_theme_option( 'body_style' ), 'full' ) !== false
										? ( 1 < $tantum_columns ? 'huge' : 'original' )
										: ( 2 < $tantum_columns ? 'big' : 'huge' )
			),
		)
	);

	?>
	<div class="post_inner"><div class="post_inner_content"><div class="post_header entry-header">
		<?php
			do_action( 'tantum_action_before_post_title' );

			// Post title
			if ( empty( $tantum_template_args['no_links'] ) ) {
				the_title( sprintf( '<h3 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
			} else {
				the_title( '<h3 class="post_title entry-title">', '</h3>' );
			}

			do_action( 'tantum_action_before_post_meta' );

			// Post meta
			$tantum_components = tantum_array_get_keys_by_value( tantum_get_theme_option( 'meta_parts' ) );
			$tantum_post_meta  = empty( $tantum_components ) || in_array( $tantum_hover, array( 'border', 'pull', 'slide', 'fade' ) )
										? ''
										: tantum_show_post_meta(
											apply_filters(
												'tantum_filter_post_meta_args', array(
													'components' => $tantum_components,
													'seo'  => false,
													'echo' => false,
												), $tantum_blog_style[0], $tantum_columns
											)
										);
			tantum_show_layout( $tantum_post_meta );
			?>
		</div><!-- .entry-header -->

		<div class="post_content entry-content">
			<?php
			// Post content area
			if ( empty( $tantum_template_args['hide_excerpt'] ) && tantum_get_theme_option( 'excerpt_length' ) > 0 ) {
				tantum_show_post_content( $tantum_template_args, '<div class="post_content_inner">', '</div>' );
			}
			// Post meta
			if ( in_array( $tantum_post_format, array( 'link', 'aside', 'status', 'quote' ) ) ) {
				tantum_show_layout( $tantum_post_meta );
			}
			// More button
			if ( empty( $tantum_template_args['no_links'] ) && ! in_array( $tantum_post_format, array( 'link', 'aside', 'status', 'quote' ) ) ) {
				tantum_show_post_more_link( $tantum_template_args, '<p>', '</p>' );
			}
			?>
		</div><!-- .entry-content -->

	</div></div><!-- .post_inner -->

</article><?php
// Need opening PHP-tag above, because <article> is a inline-block element (used as column)!
