<?php
/**
 * The Header: Logo and main menu
 *
 * @package WordPress
 * @subpackage TANTUM
 * @since TANTUM 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js
									<?php
										// Class scheme_xxx need in the <html> as context for the <body>!
										echo ' scheme_' . esc_attr( tantum_get_theme_option( 'color_scheme' ) );
									?>
										">
<head>
	<?php wp_head(); ?>
</head>

<body <?php	body_class(); ?>>

	<?php do_action( 'tantum_action_before_body' ); ?>

	<div class="body_wrap">

		<div class="page_wrap">
			
			<?php
			// Short links to fast access to the content, sidebar and footer from the keyboard
			?>
			<a class="tantum_skip_link skip_to_content_link" href="#content_skip_link_anchor" tabindex="1"><?php esc_html_e( "Skip to content", 'tantum' ); ?></a>
			<?php if ( tantum_sidebar_present() ) { ?>
			<a class="tantum_skip_link skip_to_sidebar_link" href="#sidebar_skip_link_anchor" tabindex="1"><?php esc_html_e( "Skip to sidebar", 'tantum' ); ?></a>
			<?php } ?>
			<a class="tantum_skip_link skip_to_footer_link" href="#footer_skip_link_anchor" tabindex="1"><?php esc_html_e( "Skip to footer", 'tantum' ); ?></a>
			
			<?php
			// Desktop header
			$tantum_header_type = tantum_get_theme_option( 'header_type' );
			if ( 'custom' == $tantum_header_type && ! tantum_is_layouts_available() ) {
				$tantum_header_type = 'default';
			}
			get_template_part( apply_filters( 'tantum_filter_get_template_part', "templates/header-{$tantum_header_type}" ) );

			// Side menu
			if ( in_array( tantum_get_theme_option( 'menu_style' ), array( 'left', 'right' ) ) ) {
				get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/header-navi-side' ) );
			}

			// Mobile menu
			get_template_part( apply_filters( 'tantum_filter_get_template_part', 'templates/header-navi-mobile' ) );
			
			// Single posts banner after header
			tantum_show_post_banner( 'header' );
			?>

			<div class="page_content_wrap">
				<?php
				// Single posts banner on the background
				if ( is_singular( 'post' ) || is_singular( 'attachment' ) ) {

					tantum_show_post_banner( 'background' );

					$tantum_post_thumbnail_type  = tantum_get_theme_option( 'post_thumbnail_type' );
					$tantum_post_header_position = tantum_get_theme_option( 'post_header_position' );
					$tantum_post_header_align    = tantum_get_theme_option( 'post_header_align' );

					// Boxed post thumbnail
					if ( in_array( $tantum_post_thumbnail_type, array( 'boxed', 'fullwidth') ) ) {
						ob_start();
						?>
						<div class="header_content_wrap header_align_<?php echo esc_attr( $tantum_post_header_align ); ?>">
							<?php
							if ( 'boxed' === $tantum_post_thumbnail_type ) {
								?>
								<div class="content_wrap">
								<?php
							}

							// Post title and meta
							if ( 'above' === $tantum_post_header_position ) {
								tantum_show_post_title_and_meta();
							}

							// Featured image
							tantum_show_post_featured_image();

							// Post title and meta
							if ( in_array( $tantum_post_header_position, array( 'under', 'on_thumb' ) ) ) {
								tantum_show_post_title_and_meta();
							}

							if ( 'boxed' === $tantum_post_thumbnail_type ) {
								?>
								</div>
								<?php
							}
							?>
						</div>
						<?php
						$tantum_post_header = ob_get_contents();
						ob_end_clean();
						if ( strpos( $tantum_post_header, 'post_featured' ) !== false
							|| strpos( $tantum_post_header, 'post_title' ) !== false
							|| strpos( $tantum_post_header, 'post_meta' ) !== false
						) {
							tantum_show_layout( $tantum_post_header );
						}
					}
				}

				// Widgets area above page content
				$tantum_body_style   = tantum_get_theme_option( 'body_style' );
				$tantum_widgets_name = tantum_get_theme_option( 'widgets_above_page' );
				$tantum_show_widgets = ! tantum_is_off( $tantum_widgets_name ) && is_active_sidebar( $tantum_widgets_name );
				if ( $tantum_show_widgets ) {
					if ( 'fullscreen' != $tantum_body_style ) {
						?>
						<div class="content_wrap">
							<?php
					}
					tantum_create_widgets_area( 'widgets_above_page' );
					if ( 'fullscreen' != $tantum_body_style ) {
						?>
						</div><!-- </.content_wrap> -->
						<?php
					}
				}

				// Content area
				?>
				<div class="content_wrap<?php echo 'fullscreen' == $tantum_body_style ? '_fullscreen' : ''; ?>">

					<div class="content">
						<?php
						// Skip link anchor to fast access to the content from keyboard
						?>
						<a id="content_skip_link_anchor" class="tantum_skip_link_anchor" href="#"></a>
						<?php
						// Widgets area inside page content
						tantum_create_widgets_area( 'widgets_above_content' );
