<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://hyvikk.com/
 * @since      1.0.0
 *
 * @package    Fleet_Manager
 * @subpackage Fleet_Manager/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Fleet_Manager
 * @subpackage Fleet_Manager/includes
 * @author     Hyvikk Solutions <enquiry@hyvikk.com>
 */
class Fleet_Manager_i18n
{

    /**
     * Load the plugin text domain for translation.
     *
     * @since    1.0.0
     */
    public function load_plugin_textdomain()
    {

        load_plugin_textdomain(
            'fleet-manager',
            false,
            dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
        );

    }

}
