<?php

/**
 * Fired during plugin activation
 *
 * @link       https://hyvikk.com/
 * @since      1.0.0
 *
 * @package    Fleet_Manager
 * @subpackage Fleet_Manager/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Fleet_Manager
 * @subpackage Fleet_Manager/includes
 * @author     Hyvikk Solutions <enquiry@hyvikk.com>
 */
class Fleet_Manager_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
