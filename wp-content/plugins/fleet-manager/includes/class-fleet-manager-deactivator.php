<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://hyvikk.com/
 * @since      1.0.0
 *
 * @package    Fleet_Manager
 * @subpackage Fleet_Manager/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Fleet_Manager
 * @subpackage Fleet_Manager/includes
 * @author     Hyvikk Solutions <enquiry@hyvikk.com>
 */
class Fleet_Manager_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
