<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://hyvikk.com/
 * @since             1.0.0
 * @package           Fleet_Manager
 *
 * @wordpress-plugin
 * Plugin Name:       Fleet Manager
 * Plugin URI:        https://fleetdocs.hyvikk.space/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Hyvikk Solutions
 * Author URI:        https://hyvikk.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       fleet-manager
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('FLEET_MANAGER_VERSION', '1.0.0');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-fleet-manager-activator.php
 */
function activate_fleet_manager()
{
    add_action('init', 'fleet_start_session', 1);
    require_once plugin_dir_path(__FILE__) . 'includes/class-fleet-manager-activator.php';
    Fleet_Manager_Activator::activate();
}
function fleet_start_session()
{
    if (!session_id()) {
        session_start();
    }
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-fleet-manager-deactivator.php
 */
function deactivate_fleet_manager()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-fleet-manager-deactivator.php';
    Fleet_Manager_Deactivator::deactivate();
    session_destroy();
}
add_action('init', 'fleet_start_session', 1);
register_activation_hook(__FILE__, 'activate_fleet_manager');
register_deactivation_hook(__FILE__, 'deactivate_fleet_manager');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-fleet-manager.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_fleet_manager()
{

    $plugin = new Fleet_Manager();
    $plugin->run();

}

function curl_call($url, $post_data = array())
{
    $curl = curl_init();
    if (count($post_data)) {
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
    } else {
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_URL, $url);

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;

}
function dateDiffInDays($date1, $date2)
{
    // Calulating the difference in timestamps
    $diff = strtotime($date2) - strtotime($date1);

    // 1 day = 24 hours
    // 24 * 60 * 60 = 86400 seconds
    return abs(round($diff / 86400));
}
function get_next_key_array($month_array, $key)
{

    $keys = array_keys($month_array);
    $position = array_search($key, $keys);
    if (isset($keys[$position + 1])) {
        $nextKey = $keys[$position + 1];
    } else {
        $nextKey = $keys[0];
    }
    return $nextKey;
}
function recurs($days, $formatted_start_date, $formatted_end_date, $mkey, $mvalue, $month_array, $price)
{

    $mkey_array = explode("-", $mkey);
    $mkey_start = $mkey_array[0];
    $mkey_end = $mkey_array[1];

    $formatted_mkey_start = date('Y-m-d', strtotime($mkey_start));
    $formatted_mkey_end = date('Y-m-d', strtotime($mkey_end));

    if (($formatted_start_date >= $formatted_mkey_start) && ($formatted_start_date <= $formatted_mkey_end)) {

        if ($formatted_end_date <= $formatted_mkey_end) {

            foreach ($mvalue as $dkey => $dvalue) {

                if (strpos($dkey, '-') !== false) {
                    $dkey_array = explode("-", $dkey);
                    if ($days >= $dkey_array[0] && $days <= $dkey_array[1]) {
                        $price = $price + $dvalue;
                        break;
                    }
                } else if ($dkey == '28plus' && $days >= 28) {
                    $price = $price + $dvalue;
                    break;
                } else if ($dkey == 'hourly' && $days < 1) {
                    $price = $price + $dvalue;
                    break;
                }
            }
            //echo $price. "<br/>";
            return $price;
        } else {
            /***/
            $completed_days = dateDiffInDays($formatted_start_date, $formatted_mkey_end);

            foreach ($mvalue as $dkey => $dvalue) {
                //echo $dkey. "<br/>";
                if (strpos($dkey, '-') !== false) {
                    $dkey_array = explode("-", $dkey);
                    if ($completed_days >= $dkey_array[0] && $completed_days <= $dkey_array[1]) {
                        $price = $price + $dvalue;
                        break;
                    }
                } else if ($dkey == '28plus' && $completed_days > 28) {
                    $price = $price + $dvalue;
                    break;
                } else if ($dkey == 'hourly' && $completed_days < 1) {
                    $price = $price + $dvalue;
                    break;
                }
            }
            //echo $price."<br/>";
            /***/

            $remaining_days = $days - $completed_days;

            if ($remaining_days > 0) {
                $mnextkey = get_next_key_array($month_array, $mkey);

                $next_array = $month_array[$mnextkey];
                $next_mkey_array = explode("-", $mnextkey);
                // call recursive function
                $next_mkey_start = $next_mkey_array[0];
                $next_mkey_end = $next_mkey_array[1];

                $next_formatted_mkey_start = date('Y-m-d', strtotime($next_mkey_start));
                $next_formatted_mkey_end = date('Y-m-d', strtotime($next_mkey_end));

                $return_price = recurs($remaining_days, $next_formatted_mkey_start, $formatted_end_date, $mnextkey, $next_array, $month_array, $price);
                return $return_price;
            }

        }
    }

}
function get_max($array)
{
    $max = -PHP_INT_MAX;
    foreach ($array as $k => $v) {
        $max = max(array($max, $v));
    }
    return $max;
}
function get_min($array)
{
    $min = PHP_INT_MAX;
    foreach ($array as $k => $v) {
        $min = min(array($min, $v));
    }
    return $min;
}
function currency_converter()
{
    $url = "https://api.exchangeratesapi.io/latest";

    $request = curl_init();
    $timeOut = 0;
    curl_setopt($request, CURLOPT_URL, $url);
    curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($request, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
    $response = curl_exec($request);
    curl_close($request);

    return $response;
}
function cur_price($eur_amount, $to)
{
    if ($to == "EUR") {
        return number_format((float) $eur_amount, 2, '.', '');

    } else {
        $rawData = currency_converter();
        $exchange_rates = (array) json_decode($rawData);
        $rate = $exchange_rates['rates']->$to;
        $final_amount = $eur_amount * $rate;
        return number_format((float) $final_amount, 2, '.', '');

    }

}
function charge($base_price, $days)
{
    $result = $base_price * $days;
    return number_format((float) $result, 2, '.', '');

}
run_fleet_manager();
