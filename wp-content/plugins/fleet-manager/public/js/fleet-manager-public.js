(function ($) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */


})(jQuery);
jQuery(document).ready(function () {
	var $loading = jQuery('#app-loader').hide();
	jQuery(document).ajaxStart(function () {
		$loading.show();
	}).ajaxStop(function () {
		$loading.hide();
	});
	jQuery(document).on('change', '#currency-switcher', function () {
		this.form.submit();
	});

	jQuery('[data-toggle="tooltip"]').tooltip();
	jQuery("#email_quote_form").validate({
		rules: {
			'name': {
				required: true,
			},
			'from': {
				required: true,
			},
			'to': {
				required: true,
			},
			'subject': {
				required: true,
			}
		}
	});
	/*jQuery('#email_quote_form').on('submit', function (e) {

		e.preventDefault();
		var data = jQuery('#email_quote_form').serialize() + '&action=quote_submit_action';
		jQuery.ajax({
			type: 'post',
			url: FleetAjax.ajaxurl,
			contentType: "application/json",
			//data: jQuery('#email_quote_form').serialize(),
			data: data,
			success: function () {
				alert('form was submitted');
			}
		});

	});*/

	jQuery("#pickup_drop_form").validate({
		rules: {
			'form[pickupPlace]': {
				required: true,
			},
			'form[fromDate]': {
				required: true,
			},
			'form[fromTime]': {
				required: true,
			},
			'form[dropoffPlace]': {
				required: true,
			},
			'form[toDate]': {
				required: true,
				greaterThan: "#form_fromDate",
			},
			'form[toTime]': {
				required: true,
			}
		}

	});

	let startpicker = jQuery(".from_datepicker").flatpickr(
		{
			altInput: true,
			altFormat: "d/m/Y",
			dateFormat: "Y-m-d",
			minDate: "today",
			maxDate: jQuery('.to_datepicker').attr('value'),
			onClose: function (selectedDates, dateStr, instance) {
				endpicker.set('minDate', new Date(dateStr).fp_incr(+3));
			},
		}
	);
	let endpicker = jQuery(".to_datepicker").flatpickr(
		{
			altInput: true,
			altFormat: "d/m/Y",
			dateFormat: "Y-m-d",
			minDate: jQuery('.from_datepicker').attr('value'),
			onClose: function (selectedDates, dateStr, instance) {
				startpicker.set('maxDate', new Date(dateStr).fp_incr(-3));
			},
		}
	);
	jQuery(".timepicker").flatpickr({
		enableTime: true,
		noCalendar: true,
		dateFormat: "H:i",
		time_24hr: true
	}
	);

	var clicked_location = null;
	jQuery("#locationModal").on('show.bs.modal', function (e) {
		clicked_location = jQuery(e.relatedTarget);

	});
	jQuery(".filter-places").on("keyup", function (e) {

		var value = jQuery(this).val().toLowerCase();
		jQuery(this).parents('.tab-pane').find('.place').filter(function () {
			jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
		});
	});

	jQuery('.place a').click(function () {
		var selected_text = jQuery(this).text();

		jQuery(clicked_location).val(selected_text);
		jQuery("#locationModal").modal('hide');
	});

	/*jQuery('#location-picker-wrapper').locationpicker({
		enableAutocomplete: true,
		enableReverseGeocode: true,
		radius: 0,
	
		onchanged: function (currentLocation, radius, isMarkerDropped) {
			console.log(jQuery(this).locationpicker('map'));
			var addressComponents = jQuery(this).locationpicker('map').location.addressComponents;
			console.log(currentLocation);  //latlon  
			updateControls(addressComponents); //Data
		}
	});*/

	function updateControls(addressComponents) {
		console.log(addressComponents);
		jQuery(clicked_location).val(addressComponents.addressLine1);
		jQuery("#locationModal").modal('hide');
		alert('Map is mot integrated');
	}
	jQuery('#price-slider').slider();
	jQuery('#price-slider').on("slide", function (slideEvt) {
		var values = slideEvt.value;
		jQuery(".price-selection").text(values[0] + " € - " + values[1] + " €");
	}).on("slideStop", function () {
		updateWithFilters();
	});
	jQuery('#modify-search').find('input').on('change dp.change', function () {
		jQuery('#change-submit-btn').html('Update Now');
	});
	jQuery('.filter').change(function () {
		updateWithFilters();
	});


	function updateWithFilters() {
		var minMax = jQuery('#price-slider').slider("getValue");
		var minPrice = minMax[0] - 0.05;
		var maxPrice = minMax[1] + 0.05;
		var transmissions = jQuery("[data-filter-name=Transmission]:checked").map(function () {
			return jQuery(this).data("filter-value");
		});

		var categories = jQuery("[data-filter-name=Category]:checked").map(function () {
			return jQuery(this).data("filter-value");
		});
		var nbVehicles = 0;
		var noVehicleAlert = jQuery('#no-vehicle-for-filters');
		noVehicleAlert.hide();
		jQuery('.vehicle-wrapper').hide().each(function () {
			var price = jQuery(this).data("price");

			var transmission = jQuery(this).data("transmission");
			var category = jQuery(this).data("category");
			if (price >= minPrice && price <= maxPrice && (transmissions.length == 0 || jQuery.inArray(transmission, transmissions) != -1) && (categories.length == 0 || jQuery.inArray(category, categories) != -1)) {
				jQuery(this).show();
				nbVehicles++;
			}
		});
		if (nbVehicles == 0) {
			noVehicleAlert.show();
		}
		jQuery('#nb-vehicles').text(nbVehicles);
	}
	jQuery('.email-quote').click(function (e) {
		e.preventDefault();
		jQuery('#emailQuoteModal').modal('show');
		jQuery('#vehicleId').val(jQuery(this).data('vehicle'));
	});
	/*jQuery('#share-btn').click(function (e) {
		jQuery('#share-btn').button('loading');
	});
	jQuery('#currency-switcher').change(function (e) {
		window.location.href = '/currency?_currency=' + jQuery(this).val();
	});
	var pickupPlace = jQuery('#form_pickupPlace');
	var dropoffPlace = jQuery('#form_dropoffPlace');
	updateTimepickersLimits(pickupPlace, pickupPlace.data('timepickers'));
	updateTimepickersLimits(dropoffPlace, dropoffPlace.data('timepickers'));*/
})
