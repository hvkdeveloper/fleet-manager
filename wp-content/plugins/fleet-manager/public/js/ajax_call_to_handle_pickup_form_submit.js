jQuery(document).ready(function () {
    // Handler for .ready() called.


    //
    jQuery(function () {

        jQuery("#pickup_drop_form").submit(function (event) {
            if (jQuery("#pickup_drop_form").valid()) {
                event.preventDefault();
                // var formOk = true;
                // do js validation 
                jQuery.ajax({
                    url: ajax_object.ajax_url,
                    type: 'POST',
                    data: jQuery(this).serialize() + "&action=pickup_do_something",
                    success: function (response) {
                        var objJSON = JSON.parse(response);

                        if (objJSON.success == 1) {
                            window.location.href = objJSON.redirect;
                        }
                        else {
                            if (objJSON.error != '') {
                                var errorArr = objJSON.error.split(',');
                                var error_html = '<div class="row post-errors"><div class="col-sm-12"><div class="panel-body"><ul>';
                                jQuery.each(errorArr, function (key, value) {
                                    error_html += '<li class="error">' + value + '</li>';
                                });
                                error_html += '</ul></div></div></div></div>';
                            }
                            jQuery("#error_data").html(error_html);
                        }
                    }
                });
            }
        });
    });

});