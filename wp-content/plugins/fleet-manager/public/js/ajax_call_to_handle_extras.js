
jQuery(document).on('change', '.extra_select', function () {
    var e = 0;
    var splashArray = new Array();
    jQuery(".extra_select").each(function () {
        var thisone = jQuery(this);
        var tempArray = new Array();
        var equipmentid = jQuery(thisone).data('equipment-id');
        var charge = jQuery(thisone).data('charge');
        var count = jQuery(thisone).val();

        var equipment_name = jQuery(thisone).attr('id');
        tempArray = [equipmentid, count, charge, equipment_name];
        splashArray.push(tempArray);

        if (count > 0) {
            jQuery(thisone).closest('tr').find('td#charged').html('<i class="option-added fa fa-check"></i>');
        } else {
            jQuery(thisone).closest('tr').find('td#charged').html('');
        }
        e++;
    });

    var data = {
        'action': 'extra_select_ajax',
        'equipment': splashArray,
    }

    call_pricing_ajax(data);

});
jQuery(document).ready(function () {
    var data = {
        'action': 'extra_select_ajax',
    }
    call_pricing_ajax(data);
});
function call_pricing_ajax(data) {
    jQuery.ajax({
        url: ajax_object.ajax_url,
        type: 'POST',
        data: data,
        success: function (response) {
            jQuery('#pricing-table').html(response);
        }
    });
}
