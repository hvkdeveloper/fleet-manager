<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://hyvikk.com/
 * @since      1.0.0
 *
 * @package    Fleet_Manager
 * @subpackage Fleet_Manager/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Fleet_Manager
 * @subpackage Fleet_Manager/public
 * @author     Hyvikk Solutions <enquiry@hyvikk.com>
 */
class Fleet_Manager_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public $search_form_page_id;
    public $vehicle_selection_page_id;
    public $extras_page_id;
    public $payment_page_id;
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        add_action('wp_footer', array($this, 'add_modal_footer'));
        add_action('wp_footer', array($this, 'email_quote'));
        add_action('wp_footer', array($this, 'ajax_loader_html'));

    }
    public function quote_form_submit_callback()
    {
        if (isset($_POST['quote_btn'])) {
            echo json_encode(print_r($_POST));
        }
        die();
    }
    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Fleet_Manager_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Fleet_Manager_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        $style = 'bootstrap';
        if ((!wp_style_is($style, 'queue')) && (!wp_style_is($style, 'done'))) {
            //queue up your bootstrap
            wp_enqueue_style('bootstrap4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css');

        }
        wp_enqueue_style('bootstrap-slider', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.2/css/bootstrap-slider.css', array(), $this->version, 'all');

        wp_enqueue_style('flatpickr', 'https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css', array(), $this->version, 'all');

        //https: //stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css
        $style = 'font-awesome';
        if ((!wp_style_is($style, 'queue')) && (!wp_style_is($style, 'done'))) {
            //queue up your bootstrap
            wp_enqueue_style('font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

        }
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/fleet-manager-public.css', array(), $this->version, 'all');
        wp_enqueue_style('booking-process', plugin_dir_url(__FILE__) . 'css/booking-process.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Fleet_Manager_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Fleet_Manager_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        //http: //ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js
        //wp_enqueue_script('jquery-1-9-0','http: //ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js', array(), $this->version, false);
        wp_enqueue_script('jquery-validation', 'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js', array('jquery'), $this->version, false);
        wp_enqueue_script('additional-methods', 'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/additional-methods.js', array('jquery-validation'), $this->version, false);
        wp_enqueue_script('bootstrap-popover', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery'), $this->version, false);

        $handle = 'bootstrap.min.js';
        $list = 'enqueued';
        if (wp_script_is($handle, $list)) {
            return;
        } else {
            wp_enqueue_script('boot3', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array('jquery'), '', true);

        }
        wp_enqueue_script('bootstrap-slider', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.2/bootstrap-slider.min.js', array('boot3'), $this->version, false);

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/fleet-manager-public.js', array('jquery'), $this->version, false);

        wp_localize_script($this->plugin_name, 'FleetAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
        wp_enqueue_script('flatpickr', 'https://cdn.jsdelivr.net/npm/flatpickr', array('boot3'), $this->version, false);
        add_action('wp_ajax_quote_submit_action', array($this, 'quote_form_submit_callback'));
        //wp_enqueue_script('google-map', 'https://maps.google.com/maps/api/js?sensor=false&amp;libraries=places', array('jquery'), $this->version, false);
        //wp_enqueue_script('location-picker', 'https://cdn.rawgit.com/Logicify/jquery-locationpicker-plugin/master/dist/locationpicker.jquery.min.js', array('google-map'), $this->version, false);

    }

    public function add_modal_footer()
    {
        $html = '';
        $html .= '<div class="modal location-modal fade" id="locationModal">';
        $html .= '<div class="modal-dialog modal-dialog-centered">';
        $html .= '<div class="modal-content">';

        $html .= '<div class="modal-header">';
        $html .= '<span class="modal-title"><i class="fa fa-location-arrow"></i> Choose a location ...</span>';
        $html .= '<button type="button" class="close" data-dismiss="modal">&times;</button>';
        $html .= '</div>';

        $html .= '<div class="modal-body">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" ><a href="#loc-hotels" data-toggle="tab" class="active"><i class="fa fa-building-o"></i>Hotels</a></li>
                    <li role="presentation"><a href="#loc-poi" data-toggle="tab"><i class="fa fa-map-pin"></i> Cities</a></li>
                    <li role="presentation"><a href="#loc-map" data-toggle="tab" id="loc-map-tab"><i class="fa fa-map"></i> Map</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="loc-hotels">
                        <div class="search">
                            <div class="input-icon">
                                <input type="text" class="form-control filter-places" placeholder="Search ...">
                                <span class="icon fa fa-search"></span>
                            </div>
                        </div>
                        <ul>

                                <li class="place" data-name="Yaash Villa">
                                    <a href="javascript:void(0)"><i class="fa fa-building-o"></i> Yaash Villa</a>
                                </li>
                                                            <li class="place" data-name="Zilwa Attitude Hotel">
                                    <a href="javascript:void(0)"><i class="fa fa-building-o"></i> Zilwa Attitude Hotel</a>
                                </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="loc-poi">
                        <div class="search">
                            <div class="input-icon">
                                <input type="text" class="form-control filter-places" placeholder="Search ...">
                                <span class="icon fa fa-search"></span>
                            </div>
                        </div>
                        <ul>
                                <li class="place " data-name="Trou d Eau Douce">
                                    <a href="javascript:void(0)"><i class="fa fa-map-pin"></i> Trou d\'Eau Douce</a>
                                </li>
                                                            <li class="place " data-name="Vacoas">
                                    <a href="javascript:void(0)"><i class="fa fa-map-pin"></i> Vacoas</a>
                                </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="loc-map">
                        <div id="location-picker-wrapper" style="width: 100%; height: 100%;"></div>
                    </div>
                </div>
            </div>';

        $html .= '<div class="modal-footer">';
        $html .= '<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>';
        $html .= ' </div>';

        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        echo $html;
    }
    public function acriss_popover()
    {
        $html = '<div class="popover acriss-popover fade bottom in" role="tooltip" id="popover-content" style="top: 288px; left: 366.039px; display: block;"><div class="arrow" style="left: 50%;"></div><h3 class="popover-title" style="display: none;"></h3><div class="popover-content">
        <table class="table table-condensed arciss-table">
        <thead>
            <tr>
                <th>Category</th>
                <th>Type</th>
                <th>Transmission / Drive</th>
                <th>Fuel / Air-conditioning</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><span class="abbr">M</span>Mini</td>
                <td><span class="abbr">B</span>2-3 doors</td>
                <td><span class="abbr">A</span>Auto unspecified drive</td>
                <td><span class="abbr">R</span>Unspecified fuel/power with air</td>
            </tr>
            <tr>
                <td><span class="abbr">E</span>Economy</td>
                <td><span class="abbr">D</span>4-5 doors</td>
                <td><span class="abbr">M</span>Manual unspecified drive</td>
                <td><span class="abbr">N</span>Unspecified fuel/power without air</td>
            </tr>
            <tr>
                <td><span class="abbr">C</span>Compact</td>
                <td><span class="abbr">C</span>2/4 doors</td>
                <td><span class="abbr">N</span>Manual 4WD</td>
                <td><span class="abbr">D</span>Diesel air</td>
            </tr>
            <tr>
                <td><span class="abbr">I </span>Intermediate</td>
                <td><span class="abbr">W</span>Wagon/Estate</td>
                <td><span class="abbr">C </span>Manual AWD</td>
                <td><span class="abbr">Q</span>Diesel no air</td>
            </tr>
            <tr>
                <td><span class="abbr">S</span>Standard</td>
                <td><span class="abbr">V</span>Passenger van</td>
                <td><span class="abbr">B </span>Auto 4WD</td>
                <td><span class="abbr">H</span>Hybrid air</td>
            </tr>
            <tr>
                <td><span class="abbr">F</span>Full-Size</td>
                <td><span class="abbr">L </span>Limousine</td>
                <td><span class="abbr">D</span>Auto AWD</td>
                <td><span class="abbr">I</span>Hybrid no air</td>
            </tr>
            <tr>
                <td><span class="abbr">P</span>Premium</td>
                <td><span class="abbr">S</span>Sport</td>
                <td></td>
                <td><span class="abbr">E </span>Electric air</td>
            </tr>
            <tr>
                <td><span class="abbr">L </span>Luxury</td>
                <td><span class="abbr">T</span>Convertible</td>
                <td></td>
                <td><span class="abbr">C</span>Electric no air</td>
            </tr>
            <tr>
                <td><span class="abbr">X</span>Special</td>
                <td><span class="abbr">F</span>SUV</td>
                <td></td>
                <td><span class="abbr">L </span>LPG/compressed gas air</td>
            </tr>
            <tr>
                <td><span class="abbr">N</span>Mini Elite</td>
                <td><span class="abbr">P</span>Pick-up regular cab</td>
                <td></td>
                <td><span class="abbr">S </span>LPG/compressed gas no air</td>
            </tr>
            <tr>
                <td><span class="abbr">H</span>Economy Elite</td>
                <td><span class="abbr">J</span>Open Air All-Terrain</td>
                <td></td>
                <td><span class="abbr">A </span>Hydrogen air</td>
            </tr>
            <tr>
                <td><span class="abbr">D</span>Compact Elite</td>
                <td><span class="abbr">X</span>Special</td>
                <td></td>
                <td><span class="abbr">B</span>Hydrogen no air</td>
            </tr>
            <tr>
                <td><span class="abbr">J </span>Intermediate Elite</td>
                <td><span class="abbr">E</span>Coupe</td>
                <td></td>
                <td><span class="abbr">M</span>Multi fuel/power air</td>
            </tr>
            <tr>
                <td><span class="abbr">R</span>Standard Elite</td>
                <td><span class="abbr">G </span>Crossover</td>
                <td></td>
                <td><span class="abbr">F</span>Multi fuel/power no air</td>
            </tr>
            <tr>
                <td><span class="abbr">G</span>Full-Size Elite</td>
                <td><span class="abbr">H</span>Motor home</td>
                <td></td>
                <td><span class="abbr">V</span>Petrol air</td>
            </tr>
            <tr>
                <td><span class="abbr">U</span>Premium Elite</td>
                <td><span class="abbr">M </span>Monospace</td>
                <td></td>
                <td><span class="abbr">Z</span>Petrol no air</td>
            </tr>
            <tr>
                <td><span class="abbr">W</span>Luxury Elite</td>
                <td><span class="abbr">N</span>Roadster</td>
                <td></td>
                <td><span class="abbr">U</span>Ethanol air</td>
            </tr>
            <tr>
                <td><span class="abbr">O</span>Oversize</td>
                <td><span class="abbr">Q </span>Pick-up extended cab</td>
                <td></td>
                <td><span class="abbr">X</span>Ethanol no air</td>
            </tr>
            <tr>
                <td></td>
                <td><span class="abbr">Y</span>2 wheel vehicle</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><span class="abbr">Z</span>Special offer car</td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
        </table>    </div></div>';
        echo $html;

    }
    public function email_quote()
    {
        $html = '<div class="modal flat fade email-share-modal " id="emailQuoteModal" tabindex="-1" role="dialog" aria-labelledby="emailQuoteModalLabel" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="pull-right">
                        <a href="#" data-dismiss="modal" class="close-modal"><i class="fa fa-close"></i></a>
                    </div>
                    <div class="modal-inner">
                        <h2 class="text-center">E-mail Quote</h2>

                        <p class="text-center">Share this quote by e-mail</p>

                        <form method="post" id="email_quote_form" name="email_quote_form">
                            <input type="hidden" name="vehicleId" id="vehicleId" value="" required>

                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" placeholder="Your name" required="required" class="custom-input">
                            </div>
                            <div class="form-group">
                                <label>To</label>
                                <input type="email" name="to" placeholder="Recipient e-mail address" required="required" class="custom-input">
                            </div>
                            <div class="form-group">
                                <label>From</label>
                                <input type="email" name="from" placeholder="Your e-mail address" required="required" class="custom-input">
                            </div>
                            <div class="form-group">
                                <label>Subject</label>
                                <input type="text" name="subject" placeholder="Subject" value="I found this quote !" required="required" class="custom-input">
                            </div>
                            <div class="text-center">
                                <button class="btn" type="submit" id="share-btn" name="quote_btn">
                                    <i class="fa fa-share"></i>
                                    Share                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>';
        echo $html;
    }
    public function ajax_loader_html()
    {
        $html = '<div id="app-loader" style="display: none;"></div>';
        echo $html;
    }
    public function get_category()
    {
        $fleet_options = get_option('fleet_option_name');
        $api_url = $fleet_options['api_base_url'] . "frontend/vehicle-types";
        $api_response = curl_call($api_url);
        $json_response = json_decode($api_response);
        return $json_response;
    }
    public function get_rates($start_date_time = '', $end_date_time = '')
    {

        $category_data = array();

        $fleet_options = get_option('fleet_option_name');
        $rate_api_url = $fleet_options['api_base_url'] . "frontend/rate-settings";
        $rate_api_response = curl_call($rate_api_url);
        $rate_json_response = json_decode($rate_api_response);
        $type_rates_array = (array) $rate_json_response;

        $rate_by_type = array();
        foreach ($type_rates_array as $type_rates) {
            $type_rates = (array) $type_rates;

            $rate_by_type[$type_rates['type_id']] = array();

            if (count($type_rates) > 3) {
                $month_array = array();
                $day_array = array();
                foreach ($type_rates as $key => $value_rates) {

                    if (!in_array($key, array('type_id', 'vehicletype', 'displayname'))) {
                        $key_split_array = explode("_", $key);
                        if ($key_split_array[1] == 'hourly') {
                            $month_array[$key_split_array[2] . "-" . $key_split_array[3]][$key_split_array[1]] = $value_rates;
                        } else if ($key_split_array[1] == '28plus') {
                            $month_array[$key_split_array[3] . "-" . $key_split_array[4]][$key_split_array[1]] = $value_rates;
                        } else {
                            $month_array[$key_split_array[4] . "-" . $key_split_array[5]][$key_split_array[1] . "-" . $key_split_array[2]] = $value_rates;
                        }
                    } else {

                    }

                }

                $rate_by_type[$type_rates['type_id']]['month_array'] = $month_array;
                $formatted_start_date = date('Y-m-d', strtotime($start_date_time));
                $formatted_end_date = date('Y-m-d', strtotime($end_date_time));
                $days = dateDiffInDays($start_date_time, $end_date_time);
                $price = 0;
                foreach ($month_array as $mkey => $mvalue) {
                    if ($price == 0) {
                        $price = recurs($days, $formatted_start_date, $formatted_end_date, $mkey, $mvalue, $month_array, $price);
                    }

                }
                $rate_by_type[$type_rates['type_id']] = $price;

            }
        }

        return $rate_by_type;
    }
}
