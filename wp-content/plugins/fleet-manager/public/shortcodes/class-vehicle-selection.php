<?php

class Vehicle_Selection extends Fleet_Manager_Public
{
    public $car_count;
    public $car_data;
    public $car_category;
    public function __construct()
    {
        $fleet_options = get_option('fleet_option_name');
        $this->search_form_page_id = $fleet_options['search_form_page'];
        $this->vehicle_selection_page_id = $fleet_options['vehicle_selection_page'];
        $this->extras_page_id = $fleet_options['extras_page'];
        // $this->payment_page_id = $fleet_options['payment_page'];

        add_shortcode('vehicle_selection', array($this, 'vehicle_selection_callback'));

    }

    public function vehicle_selection_callback()
    {

        if (!is_admin()) {
            if (isset($_POST['currency_code'])) {
                $_SESSION['currency_code'] = $_POST['currency_code'];
            } else {
                $_SESSION['currency_code'] = 'EUR';
            }

            if (isset($_SESSION['form']) && isset($_SESSION['search_data'])) {

                $this->car_count = count($_SESSION['search_data']);
                $this->car_data = $_SESSION['search_data'];
                $this->car_category = $this->get_category();
                $this->rates = $this->get_rates($_SESSION['form']['fromDate'], $_SESSION['form']['toDate']);

                $this->max = cur_price(get_max($this->rates), $_SESSION['currency_code']);
                $this->min = cur_price(get_min($this->rates), $_SESSION['currency_code']);

                ob_start();

                $this->submit_vehicle_selection();
                $this->html_vehicle_selection();
                return ob_get_clean();

            } else {
                //get pickup page id from option and redirect to that page
                $fleet_options = get_option('fleet_option_name');
                $search_form_page_id = $fleet_options['search_form_page'];
                echo '<script>window.location.href = "' . get_permalink($this->search_form_page_id) . '";</script>';
                exit();

            }
        } else {

        }

    }

    public function submit_vehicle_selection()
    {

        if (isset($_POST['search-submit-btn'])) {
            $pickup_drop = new Pickup_Drop_Form();
            $pickup_drop->submit_pickup_drop_form();

        } else if (isset($_POST['book-now'])) {
            $_SESSION['booked_vehicle'] = $_POST['vehicleId'];
            $_SESSION['original_rent'] = $_POST['price'];
            $fleet_options = get_option('fleet_option_name');
            $extras_page_id = $fleet_options['extras_page'];
            echo '<script>window.location.href = "' . get_permalink($extras_page_id) . '";</script>';
            exit();

        }
    }

    public function html_vehicle_selection()
    {
        $html = 'Vehicle Selection';
        // print_r($_SESSION['search_data']);

        $html = '<div class="booking-process">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-md-4 col-lg-3 booking-filters hidden-xs">
                    <div class="panel">
                        <div class="panel-heading">
                            <i class="icon fa fa-search"></i>
                            Search Result                            <a class="collapse-chevron" data-toggle="collapse" href="#modify-search" aria-expanded="true" aria-controls="modify-search">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                        <div id="modify-search" class="collapse in show">
                            <div class="panel-body">
                                <form name="form" method="post" class="form-horizontal" id="pickup_drop_form">
                                <div class="input-icon icon-trigger">
                                    <input type="text" id="form_pickupPlace" name="form[pickupPlace]" required="required" class="custom-input" placeholder="Pick-up place" readonly="readonly" data-toggle="modal" data-target="#locationModal" value="' . $_SESSION['form']['pickupPlace'] . '">
                                    <span class="icon fa fa-map-marker"></span>
                                </div>
                                <div class="input-icon icon-trigger">
                                    <input type="text" id="form_dropoffPlace" name="form[dropoffPlace]" required="required" class="custom-input" placeholder="Drop-off place" readonly="readonly" data-toggle="modal" data-target="#locationModal" value="' . $_SESSION['form']['dropoffPlace'] . '">
                                    <span class="icon fa fa-map-marker"></span>
                                </div>
                                <div class="input-icon icon-trigger">
                                    <input type="text" id="form_fromDate" name="form[fromDate]" required="required" class="custom-input from_datepicker" readonly="readonly" placeholder="Date" value="' . $_SESSION['form']['fromDate'] . '">
                                    <span class="icon fa fa-calendar"></span>
                                </div>
                                <div class="input-icon icon-trigger">
                                    <input type="text" id="form_fromTime" name="form[fromTime]" required="required" class="timepicker custom-input" readonly="readonly" placeholder="Time" value="' . $_SESSION['form']['fromTime'] . '">
                                    <span class="icon fa fa-clock-o"></span>
                                </div>
                                <div class="input-icon icon-trigger">
                                    <input type="text" id="form_toDate" name="form[toDate]" required="required" class="custom-input to_datepicker" readonly="readonly" placeholder="Date" value="' . $_SESSION['form']['toDate'] . '">
                                    <span class="icon fa fa-calendar"></span>
                                </div>
                                <div class="input-icon icon-trigger">
                                    <input type="text" id="form_toTime" name="form[toTime]" required="required" class="timepicker custom-input" readonly="readonly" placeholder="Time" value="' . $_SESSION['form']['toTime'] . '">
                                    <span class="icon fa fa-clock-o"></span>
                                </div>

                                <button type="submit" class="btn submit-btn" name="search-submit-btn" id="change-submit-btn">
                                    Edit Search</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <i class="icon fa fa-filter"></i>
                            Price                            <a class="collapse-chevron" data-toggle="collapse" href="#price-filter" aria-expanded="true" aria-controls="price-filter">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                        <div id="price-filter" class="collapse in show">
                            <div class="panel-body">
                                <div class="price-selection">' . $this->min . ' ' . $_SESSION['currency_code'] . ' - ' . $this->max . ' ' . $_SESSION['currency_code'] . '</div>
                                <div class="slider slider-horizontal">
                                    <input type="text" class="slider-widget" name="prices" data-slider-min="' . $this->min . '" data-slider-max="' . $this->max . '" data-slider-step="1" data-slider-value="[' . $this->min . ',' . $this->max . ']" data-slider-tooltip="hide" data-value="' . $this->min . ',' . $this->max . '" value="' . $this->min . ',' . $this->max . '" style="display: none;" id="price-slider" />

                            </div>
                        </div>
                    </div>
                                            <div class="panel">
                            <div class="panel-heading">
                                                                    <i class="flaticon-transmission"></i>
                                                                Transmission
                                <a class="collapse-chevron" data-toggle="collapse" href="#filter-Transmission" aria-expanded="true" aria-controls="#filter-Transmission">
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                            </div>

                            <div id="filter-Transmission" class="collapse in show">
                                <table class="table selection">
                                                                            <tbody><tr>
                                            <td>
                                                <div class="checkbox checkbox-success">
                                                    <input type="checkbox" id="Automatic" class="filter" data-filter-name="Transmission" data-filter-value="A">
                                                    <label for="Automatic">
                                                        Automatic
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                                                            <tr>
                                            <td>
                                                <div class="checkbox checkbox-success">
                                                    <input type="checkbox" id="Manual" class="filter" data-filter-name="Transmission" data-filter-value="M">
                                                    <label for="Manual">
                                                        Manual
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                                                    </tbody></table>
                            </div>
                        </div>';
        if (count($this->car_category)) {
            $html .= '<div class="panel">
                            <div class="panel-heading">
                                                                    <i class="icon fa fa-car"></i>
                                                                Category
                                <a class="collapse-chevron" data-toggle="collapse" href="#filter-Category" aria-expanded="true" aria-controls="#filter-Category">
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                            </div>
                            <div id="filter-Category" class="collapse in show">
                                <table class="table selection">
                                                                            <tbody>
                                                                            ';
            foreach ($this->car_category as $type) {
                $html .= '<tr>
                                            <td>
                                                <div class="checkbox checkbox-success">
                                                    <input type="checkbox" id="' . $type->id . '" class="filter" data-filter-name="Category" data-filter-value="' . $type->vehicle_type . '">
                                                    <label for="' . $type->id . '">' .
                $type->vehicle_type . '
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>';
            }

            $html .= '</tbody></table>
                            </div>
                        </div>';
        }

        $html .= '</div>
                   </div>
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 booking-results">

                    <nav class="multi-step cols-4">
                    <ul>
                        <li class="active">
                            <a href="' . get_permalink($this->vehicle_selection_page_id) . '">1. Vehicle</a>
                        </li>
                        <li>
                            <a href="' . get_permalink($this->extras_page_id) . '">2. Extras</a>
                        </li>
                        <li>
                            <a href="' . get_permalink($this->payment_page_id) . '">3. Payment</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check-circle-o"></i> Done</a>
                        </li>
                    </ul>
                    </nav>
                ';
        if ($this->car_count > 0) {
            $html .= '
                    <h2 class="pull-left">
                        We found <span class="highlight">
                            <span id="nb-vehicles">' . $this->car_count . '</span>
                            available cars</span> for you
                    </h2>
                    <div class="pull-right change-currency">
                    <form action="" method="POST">
                            Currency :
                            <select id="currency-switcher" name="currency_code"> ';
            if (isset($_SESSION['currency_code']) && $_SESSION['currency_code'] == 'EUR') {
                $html .= '<option  value="EUR" selected>Euro (€)
                                </option>';

            } else {
                $html .= '<option  value="EUR" selected>Euro (€)
                                </option>';
            }
            if (isset($_SESSION['currency_code']) && $_SESSION['currency_code'] == 'USD') {
                $html .= '<option value="USD" selected>U.S. dollar ($)
                                </option>';

            } else {
                $html .= '<option value="USD">U.S. dollar ($)
                                </option>';
            }
            if (isset($_SESSION['currency_code']) && $_SESSION['currency_code'] == 'GBP') {
                $html .= '<option value="GBP" selected>Pound sterling (£)
                                </option>';

            } else {
                $html .= '<option value="GBP">Pound sterling (£)
                                </option>';
            }
            if (isset($_SESSION['currency_code']) && $_SESSION['currency_code'] == 'CHF') {
                $html .= '<option value="CHF" selected>Swiss franc (CHF)
                                </option>';

            } else {
                $html .= '<option value="CHF">Swiss franc (CHF)
                                </option>';
            }

            if (isset($_SESSION['currency_code']) && $_SESSION['currency_code'] == 'CAD') {
                $html .= ' <option value="CAD" selected>Canadian dollar (CAD)
                                </option>';

            } else {
                $html .= ' <option value="CAD">Canadian dollar (CAD)
                                </option>';
            }

            $html .= '
                            </select>
                    </form>
                        </div>';
        } else {
            $html .= '
                    <div id="no-vehicle-for-filters" class="alert alert-danger booking-alert" style="display: none;">
                        We found no car available as per your requirement. Please call our reservation department on: +230 637 8080 to find out an alternate solution for you or select another dates and retry. Thank you.
                    </div>';

        }
        if ($this->car_count > 0) {
            foreach ($this->car_data as $car_info) {

                $html .= '<div class="row vehicle-wrapper" data-id="' . $car_info->id . '" data-name="' . $car_info->name . '" data-price="' . $this->rates[$car_info->type_id] . '" data-transmission="' . $car_info->transmission . '" data-category="' . $car_info->type . '">
                <div class="col-xs-12 col-sm-4 col-md-3 text-center" >
                    <img class="vehicle-image" src="' . $car_info->image . '" alt="image" data-pagespeed-url-hash="1271663790" >
                </div>
                <div class="col-xs-12 col-sm-8 col-md-5 col-lg-6">
                    <h3>' . $car_info->name . '<span class="similar">or similar</span></h3>
                    <h4>' . $car_info->type . ' | <span data-placement="bottom" data-toggle="popover" data-container="body" data-placement="left" type="button" data-html="true" href="#">' . $car_info->acriss . '</span></h4>
                    <table class="details"><tbody><tr>
                    <td><i class="fa fa-users" aria-hidden="true"></i></td>
                    <td> ' . $car_info->passenger . '</td>
                    <td><i class="fa fa-briefcase" aria-hidden="true"></i></td>
                    <td>' . $car_info->luggage . '</td>
                    <td><i class="fa fa-car" aria-hidden="true"></i></td>
                    <td>' . $car_info->doors . '</td>
                    <td><i class="fa fa-cogs" aria-hidden="true"></i></td>
                    <td>' . $car_info->transmission . '</td>';
                $html .= ($car_info->airconditionar == 1) ? '<td><i class="fa fa-snowflake-o" aria-hidden="true"></i></td>' : '';

                $html .= '</tr></tbody></table>';
                $html .= '<table class="included"><tbody><tr>';
                $html .= ($car_info->unlimited_mileage == 1) ? '<td>Unlimited mileage</td>' : '';

                $html .= ($car_info->road_side_assistance == 1) ? '<td>Road-side assistance</td>' : '';
                $html .= '</tr>
                            <tr>';
                $html .= ($car_info->free_airport_delivery == 1) ? '<td>Free Airport Delivery</td>' : '';
                $html .= ($car_info->insurance == 1) ? '<td>Insurance</td>' : '';
                $html .= '</tr></tbody></table>';
                $html .= '</div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                <div class="total">' . $_SESSION['currency_code'] . ' ' . cur_price($this->rates[$car_info->type_id], $_SESSION['currency_code']) . '</div>
                <div class="duration">Total price for ' . $_SESSION['rental_days'] . ' d.</div>

                <form action="#" method="post" class="text-center">
                    <input type="hidden" name="vehicleId" value="' . $car_info->id . '">
                    <input type="hidden" name="price" value="' . $this->rates[$car_info->type_id] . '">
                    <button class="book-now btn" type="submit" name="book-now">Book Now !</button>
                    <a href="#" type="button" class="email-quote" data-vehicle="' . $car_info->id . '">
                        <i class="fa fa-envelope-o"></i> E-mail Quote</a>
                </form>
            </div>
        </div>';
            }
        }
        $html .= '
                </div>
            </div>
        </div>
    </div>';
        echo $html;
    }
}
$vehicle_selection = new Vehicle_Selection();
