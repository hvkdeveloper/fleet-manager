<?php

class Pickup_Drop_Form extends Fleet_Manager_Public
{
    public $error_message = '';
    public function __construct()
    {
        $fleet_options = get_option('fleet_option_name');
        $this->search_form_page_id = $fleet_options['search_form_page'];
        $this->vehicle_selection_page_id = $fleet_options['vehicle_selection_page'];
        $this->extras_page_id = $fleet_options['extras_page'];
        // $this->payment_page_id = $fleet_options['payment_page'];
        $this->attach_actions();
        $this->attach_shortcodes();

    }
    public function attach_actions()
    {

        add_action('wp_ajax_nopriv_pickup_do_something', array($this, 'pickup_do_something_serverside'));
        add_action('wp_ajax_pickup_do_something', array($this, 'pickup_do_something_serverside'));

        add_action('wp_ajax_nopriv_extra_select_ajax', array($this, 'extra_select_ajax_serverside'));
        add_action('wp_ajax_extra_select_ajax', array($this, 'extra_select_ajax_serverside'));

        add_action('wp_enqueue_scripts', array($this, 'pickup_enqueue_script'), 99999);
    }
    public function pickup_enqueue_script()
    {
        /*
         * include the js
         */
        wp_enqueue_script('pickup-script', plugin_dir_url(__FILE__) . '../js/ajax_call_to_handle_pickup_form_submit.js', array('jquery'));

        // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
        wp_localize_script('pickup-script', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php'), 'we_value' => 1234));

    }
    public function attach_shortcodes()
    {

        add_shortcode('pickup_drop_form', array($this, 'pickup_drop_form_callback'));

    }

    public function pickup_do_something_serverside()
    {
        if (isset($_POST['form'])) {

            $_SESSION['form'] = $_POST['form'];
            $fleet_options = get_option('fleet_option_name');
            $api_url = $fleet_options['api_base_url'] . "frontend/search";
            $post_data = $_POST['form'];
            if ($post_data['fromDate'] != '') {
                $post_data['pickup_datetime'] = $post_data['fromDate'] . " " . $post_data['fromTime'] . ":";
            }

            if ($post_data['toDate'] != '') {
                $post_data['dropoff_datetime'] = $post_data['toDate'] . " " . $post_data['toTime'] . ":";
            }

            $api_response = curl_call($api_url, $post_data);
            $json_response = json_decode($api_response);

            $return_data = array();
            if ($json_response->success == 0) {
                $return_data['error'] = $json_response->message;
                $return_data['success'] = 0;
            } else {
                $_SESSION['search_data'] = (array) $json_response->data;
                $_SESSION['rental_days'] = $this->dateDiffInDays($post_data['fromDate'], $post_data['toDate']);
                //get vehicle selection page id from option and redirect to that page
                // echo '<script>window.location.href = "' . get_permalink($this->vehicle_selection_page_id) . '";</script>';
                $return_data['success'] = 1;
                $return_data['redirect'] = get_permalink($this->vehicle_selection_page_id);

            }

            echo json_encode($return_data);
        }

        die();

    }

    public function pickup_drop_form_callback()
    {
        if (!is_admin()) {
            ob_start();
            // $this->submit_pickup_drop_form();
            $this->html_pickup_drop_form();
            return ob_get_clean();
        } else {

        }
    }

    public function submit_pickup_drop_form()
    {

    }
    public function dateDiffInDays($date1, $date2)
    {
        // Calulating the difference in timestamps
        $diff = strtotime($date2) - strtotime($date1);

        // 1 day = 24 hours
        // 24 * 60 * 60 = 86400 seconds
        return abs(round($diff / 86400));
    }
    public function html_pickup_drop_form()
    {
        $html = '';
        $html .= '<div class="booking">';

        $html .= '<form name="form" method="post" class="highlight-submit-validation form-horizontal" action="' . esc_url($_SERVER['REQUEST_URI']) . '" id="pickup_drop_form">';

        $html .= '<div class="row">';
        $html .= '<div class="col-xs-12 col-sm-6 sep-column">';
        $html .= '<div class="panel panel-default">';
        $html .= '<div class="panel-heading">';
        /*  $html .= '<i class="icon pg-pickup"></i>';

         */
        $html .= '<div class="title">' . __('Pick-up', 'fleet-manager') . '</div>';
        $html .= '</div>';
        $html .= '<div class="panel-body" style="padding-bottom: 8px;">';
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12 col-md-12">';
        $html .= '<div class="input-icon icon-trigger form-group">';
        $html .= '<input type="text" id="form_pickupPlace" name="form[pickupPlace]" required="required" class="custom-input" placeholder="Pick-up place" readonly="readonly" data-toggle="modal" data-target="#locationModal" value="' . $_SESSION['form']['pickupPlace'] . '">';
        $html .= '<span class="icon fa fa-map-marker"></span>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12 col-md-6">';
        $html .= '<div class="input-icon icon-trigger">';
        $html .= '<input type="text" id="form_fromDate" name="form[fromDate]" required="required" class="from_datepicker custom-input" placeholder="Date" readonly="readonly" value="' . $_SESSION['form']['fromDate'] . '">';
        $html .= '<span class="icon fa fa-calendar"></span>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-12 col-md-6">';
        $html .= '<div class="input-icon icon-trigger">';
        $html .= '<input type="text" id="form_fromTime" name="form[fromTime]" required="required" class="timepicker custom-input" placeholder="Time" readonly="readonly" value="' . $_SESSION['form']['fromTime'] . '">';
        $html .= '<span class="icon fa fa-clock-o"></span>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        /* $html .= '<div class="row booking-notes">';
        $html .= '<div class="col-sm-12">';
        $html .= 'If you wish to rent a car up to date, please contact us on +230 637 80 80';
        $html .= '</div>';
        $html .= '</div>'; */
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-xs-12 col-sm-6 sep-column">';
        $html .= '<div class="panel panel-default">';
        $html .= '<div class="panel-heading">';
        /* $html .= '<i class="icon pg-dropoff"></i>';

         */$html .= '<div class="title">Drop-off</div>';
        $html .= '</div>';
        $html .= '<div class="panel-body">';
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12 col-md-12">';
        $html .= '<div class="input-icon icon-trigger form-group">';
        $html .= '<input type="text" id="form_dropoffPlace" name="form[dropoffPlace]" required="required" class="custom-input" placeholder="Drop-off place" readonly="readonly" data-toggle="modal" data-target="#locationModal" value="' . $_SESSION['form']['dropoffPlace'] . '">';
        $html .= '<span class="icon fa fa-map-marker"></span>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12 col-md-6 date">';
        $html .= '<div class="input-icon icon-trigger">';
        $html .= '<input type="text" id="form_toDate" name="form[toDate]" required="required" class="to_datepicker custom-input" placeholder="Date" readonly="readonly" value="' . $_SESSION['form']['toDate'] . '">';
        $html .= '<span class="icon fa fa-calendar"></span>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-12 col-md-6 time">';
        $html .= '<div class="input-icon icon-trigger">';
        $html .= '<input type="text" id="form_toTime" name="form[toTime]" required="required" class="timepicker custom-input" placeholder="Time" readonly="readonly" value="' . $_SESSION['form']['toTime'] . '">';
        $html .= '<span class="icon fa fa-clock-o"></span>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div id="error_data"></div>';
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12">';
        $html .= '<button class="btn submit" type="submit" name="search-submit-btn" >Search <i class="fa fa-search"></i></button>';
        $html .= '</div>';
        $html .= '</div>';
        //$html .= '<input type="hidden" id="form__token" name="form[_token]" class="form-control" value="EGB8AkhZMYpmv7dMDVZmTIyKRNCjiG50kURc5LI6z9E">';
        $html .= '</form>';

        $html .= '</div>';

        echo $html;
    }
}
$pickup_drop = new Pickup_Drop_Form();
