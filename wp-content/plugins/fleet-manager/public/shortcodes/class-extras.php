<?php

class Extras extends Fleet_Manager_Public
{

    public function __construct()
    {
        $fleet_options = get_option('fleet_option_name');
        $this->search_form_page_id = $fleet_options['search_form_page'];
        $this->vehicle_selection_page_id = $fleet_options['vehicle_selection_page'];
        $this->extras_page_id = $fleet_options['extras_page'];
        // $this->payment_page_id = $fleet_options['payment_page'];
        $this->attach_actions();
        $this->attach_shortcodes();

    }
    public function attach_actions()
    {

        add_action('wp_ajax_nopriv_extra_select_ajax', array($this, 'extra_select_ajax_serverside'));
        add_action('wp_ajax_extra_select_ajax', array($this, 'extra_select_ajax_serverside'));

        add_action('wp_enqueue_scripts', array($this, 'extra_select_ajax'), 99999);
    }
    public function extra_select_ajax()
    {
        /*
         * include the js
         */
        wp_enqueue_script('extra-script', plugin_dir_url(__FILE__) . '../js/ajax_call_to_handle_extras.js', array('jquery'));

        // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
        wp_localize_script('extra-script', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php'), 'we_value' => 1234));

    }
    public function attach_shortcodes()
    {

        add_shortcode('extras', array($this, 'extras_callback'));

    }
    public function extra_select_ajax_serverside()
    {

        $rental_days = $_SESSION['rental_days'];

        $currentcy_code = $_SESSION['currency_code'];

        $original_rent = $_SESSION['original_rent'];

        $_SESSION['delivery_fee'] = $delivery_fee = 15;

        $_SESSION['drop_off_fee'] = $drop_off_fee = 15;
        $total = 0;
        $total = $total + $original_rent;
        if ($rental_days <= 5) {
            $total = $total + $delivery_fee + $drop_off_fee;
        }
        $extra_html = '';
        $epq_total_charge = 0;

        if (isset($_POST['equipment'])) {

            foreach ($_POST['equipment'] as $eqp) {

                array_push($_SESSION['eqp'][$epq[3]], $eqp);

                if ($eqp[1] > 0) {

                    $epq_total_charge = $epq_total_charge + $eqp[2];

                    $total = $total + $eqp[2];
                    $extra_html .= '<tr>
                        <td>' . $eqp[0] . '
                            (x' . $eqp[1] . ')
                        </td>
                        <td class="text-right">
                            ' . $currentcy_code . ' ' . $eqp[2] .
                        '</td>
                    </tr>';
                }
            }

        }
        $_SESSION['extra_charges'] = $epq_total_charge;

        $_SESSION['total'] = $original_rent + $epq_total_charge + $delivery_fee + $drop_off_fee;

        $price_html = '';
        $price_html .= '<tbody>';
        $price_html .= '<tr class="title">';
        $price_html .= '<td colspan="2">';
        $price_html .= '<i class="fa fa-credit-card"></i>';
        $price_html .= 'Price                  ';
        $price_html .= '</td>';
        $price_html .= '</tr>';
        $price_html .= '<tr>';
        $price_html .= '<td>';
        $price_html .= $rental_days . ' days rental   ';
        $price_html .= '</td>';
        $price_html .= '<td class="text-right">';
        $price_html .= $currentcy_code . ' ' . $original_rent;
        $price_html .= '</td>';
        $price_html .= '</tr>';

        if ($rental_days <= 5) {
            $price_html .= '<tr>';
            $price_html .= '<td>';
            $price_html .= '<button class="btn btn-link tooltip-info small" data-toggle="tooltip" data-placement="bottom" data-html="true" data-trigger="click" title="" data-original-title="Delivery fee outside Airport is applied.<br/>(Only for rentals less than 6 days)">';
            $price_html .= '<i class="fa fa-info-circle" aria-hidden="true"></i>';
            $price_html .= '</button>';
            $price_html .= 'Delivery Fee</td>';
            $price_html .= '<td class="text-right">';
            $price_html .= $currentcy_code . ' ' . number_format((float) $delivery_fee, 2, '.', '');
            $price_html .= '</td>';
            $price_html .= '</tr>';

            $price_html .= '<tr>';
            $price_html .= '<td>';
            $price_html .= '<button class="btn btn-link tooltip-info small" data-toggle="tooltip" data-placement="bottom" data-html="true" data-trigger="click" title="" data-original-title="Drop-off fee outside Airport is applied.<br/>(Only for rentals less than 6 days)">';
            $price_html .= '<i class="fa fa-info-circle" aria-hidden="true"></i>';
            $price_html .= '</button>';
            $price_html .= 'Drop-off Fee</td>';
            $price_html .= '<td class="text-right">';
            $price_html .= $currentcy_code . ' ' . number_format((float) $drop_off_fee, 2, '.', '');
            $price_html .= ' </td>';
            $price_html .= '</tr>';
        }
        $price_html .= $extra_html;
        $price_html .= '<tr>';
        $price_html .= '<td>';
        $price_html .= '<div>Total</div>';
        $price_html .= '</td>';
        $price_html .= '<td class="text-right" id="total">';
        $price_html .= $currentcy_code . ' ' . number_format((float) $total, 2, '.', '');
        $price_html .= '</td>';
        $price_html .= '</tr>';
        $price_html .= '</tbody>';
        echo $price_html;
        die();
    }
    public function extras_callback()
    {
        if (!is_admin()) {

            if (!isset($_SESSION['extra_settings'])) {
                $fleet_options = get_option('fleet_option_name');
                $api_url = $fleet_options['api_base_url'] . "frontend/extra-settings";
                $api_response = curl_call($api_url);
                $json_response = json_decode($api_response);
                $_SESSION['extra_settings'] = (array) $json_response;

            }
            if (isset($_SESSION['booked_vehicle']) && isset($_SESSION['search_data'])) {
                ob_start();
                $this->submit_extras();
                $this->html_extras();
                return ob_get_clean();
            } else {
                //get vehicle selection page id from option and redirect to that page

                echo '<script>window.location.href = "' . get_permalink($this->vehicle_selection_page_id) . '";</script>';
                exit();

            }
        }

    }

    public function submit_extras()
    {

    }
    public function searchForId($id, $array)
    {
        foreach ($array as $key => $val) {

            if ($val->id == $id) {
                return $key;
            }
        }
        return null;
    }
    public function html_extras()
    {
        $booked_vehicle_id = $this->searchForId($_SESSION['booked_vehicle'], $_SESSION['search_data']);
        $booked_vehicle_data = $_SESSION['search_data'][$booked_vehicle_id];

        $html = 'Extras and Options';
        $html = '<div class="booking-process">
        <div class="container extras">
            <div class="row">
                <div class="col-sm-7 col-md-8 col-lg-9 booking-options">
                    <nav class="multi-step cols-4">
                     <ul>
                        <li>
                            <a href="' . get_permalink($this->vehicle_selection_page_id) . '">1. Vehicle</a>
                        </li>
                        <li class="active">
                            <a href="' . get_permalink($this->extras_page_id) . '">2. Extras</a>
                        </li>
                        <li>
                            <a href="' . get_permalink($this->payment_page_id) . '">3. Payment</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check-circle-o"></i> Done</a>
                        </li>
                    </ul>
            </nav>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <span class="panel-heading-number">1</span>
                                    Options / Equipments</div>

                                <table class="table extras-table">
                                            <tbody>
                                            ';
        $html .= $this->additional_driver();
        $html .= $this->baby_seat();
        $html .= $this->booster_seat();
        $html .= $this->child_seat();
        $html .= $this->map_of_mauritius();
        $html .= $this->mobile_gps();
        $html .= $this->premium_assist();
        $html .= $this->sim_card();
        $html .= $this->theft_protection();
        $html .= $this->wifi();
        $html .= $this->senior_driver();

        $html .= '</tbody></table>
                            </div>
                        </div>


     </div>
                    <div class="hidden-xs additional-infos-wrapper">
                        <div class="row">
            <div class="col-sm-12 booking-options">
            <form action="/booking/additional-info" method="post" class="additional-infos-form">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel additional-info">
                        <div class="panel-heading">
                            <span class="panel-heading-number">2</span>
                            Useful Information                        </div>
                        <div class="panel-body">
                                                            <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="arrivalFlightNumber">
                                            Arrival Flight Number                                            <button class="btn btn-link tooltip-info small" data-toggle="tooltip" data-placement="bottom" data-trigger="click" type="button" title="" data-original-title="Providing us your flight number helps us to ensure that your car is available if your flight is delayed">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                            </button>
                                        </label>
                                        <input id="arrivalFlightNumber" type="text" class="form-control" name="arrivalFlightNumber" value="">
                                    </div>
                                </div>
                                                                                                                                                                                                                                                                                                                                                                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label for="hotel">
                                        Hotel                                        <button class="btn btn-link tooltip-info small" data-toggle="tooltip" data-placement="bottom" data-trigger="click" type="button" title="" data-original-title="Please indicate the name of your accommodation in Mauritius">
                                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        </button>
                                    </label>
                                    <input id="hotel" type="text" class="form-control" name="hotel" value="">
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="comments">
                                        Additional wishes</label>
                                    <textarea id="comments" class="form-control" rows="5" name="comments"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-right">
                    <button type="submit" class="btn continue">
                        Continue</button>
                </div>
            </div>
        </form>
            </div>
        </div>                    </div>
                </div>

                <div class="col-sm-4 col-md-3">
                    <div class="booking-summary">
    <div class="panel">
        <div class="panel-heading global-title">
            <h2 class="title">
                Booking Summary            </h2>
        </div>
        <div class="panel-body">
            <img class="vehicle-image" src="' . $booked_vehicle_data->image . '" alt="image" >

            <h3>' . $booked_vehicle_data->name . '</h3>
        </div>

        <table class="table">
            <tbody>
                <tr class="title">
                    <td colspan="2"><i class="pg-pickup"></i> Pick-up</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div>' . $_SESSION['form']['pickupPlace'] . '</div>
                        <div>' . $_SESSION['form']['fromDate'] . ' - ' . $_SESSION['form']['fromTime'] . '</div>
                    </td>
                </tr>
                <tr class="title">
                    <td colspan="2"><i class="pg-dropoff"></i> Drop-off</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div>' . $_SESSION['form']['dropoffPlace'] . '</div>
                        <div>' . $_SESSION['form']['toDate'] . ' - ' . $_SESSION['form']['toTime'] . '</div>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table" id="pricing-table">

        </table>
    </div></div></div></div></div>';

        echo $html;
    }
    public function additional_driver()
    {
        if (isset($_SESSION['extra_settings']['additional_driver'])) {
            if ($_SESSION['extra_settings']['additional_driver'] == 0) {
                $charge = 0;
                $price_cell = '<span class="free">Free</span>';
            } else {
                $charge = charge($_SESSION['extra_settings']['baby_seat'], $_SESSION['rental_days']);
                $price_cell = '<span class="paid">' . $_SESSION['currency_code'] . " " . $charge . ' for ' . $_SESSION['rental_days'] . ' days</span>';
            }
            $html = '<tr>
                        <td>
                            <img src="https://pingouincar.atracio.com:443/uploads/images/rentalOptions/77c4a965-8630-496b-bda5-3b48fe9f5751.png" alt="Equipment" class="extra-image" >

                            <div class="extra-name">
                                ADDITIONAL DRIVER
                            </div>
                            <div class="btn-tooltip">
                                <button class="pull-right btn btn-link tooltip-info" data-toggle="tooltip" data-placement="bottom" data-trigger="click" title="" data-original-title="Additional Driver">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="text-center" width="10%">
                            <select id="additional_driver" class="extra_select" data-equipment-id="Additional driver" data-charge="' . $charge . '">
                              <option value="0">0</option>
                                <option value="1">1</option>
                            </select>
                        </td>
                        <td width="25%" class="price-cell">' . $price_cell . '</td>
                        <td width="5%" id="charged"></td>
                    </tr>';

        } else {
            $html = '';
        }
        return $html;

    }
    public function baby_seat()
    {
        if (isset($_SESSION['extra_settings']['baby_seat'])) {
            if ($_SESSION['extra_settings']['baby_seat'] == 0) {
                $charge = 0;
                $price_cell = '<span class="free">Free</span>';
            } else {
                $charge = charge($_SESSION['extra_settings']['baby_seat'], $_SESSION['rental_days']);
                $price_cell = '<span class="paid">' . $_SESSION['currency_code'] . " " . $charge . ' for ' . $_SESSION['rental_days'] . ' days </span>';
            }
            $html = '<tr>
                        <td>
                            <img src="https://pingouincar.atracio.com:443/uploads/images/rentalOptions/e6896996-9194-4505-b5a7-1e09ab8c4de9.png" alt="Equipment" class="extra-image" >

                            <div class="extra-name">
                                BABY SEAT
                            </div>
                            <div class="btn-tooltip">
                                <button class="pull-right btn btn-link tooltip-info" data-toggle="tooltip" data-placement="bottom" data-trigger="click" title="" data-original-title="Baby seat suitable for Baby: 0 to 12 months (0 - 13 kg)">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="text-center" width="10%">
                            <select id="baby_seat" class="extra_select" data-equipment-id="Baby Seat" data-charge="' . $charge . '">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            </select>
                        </td>
                        <td width="25%" class="price-cell">' . $price_cell . '</td>
                        <td width="5%" id="charged"></td>
                    </tr>';
        } else {
            $html = '';
        }
        return $html;

    }
    public function booster_seat()
    {
        if (isset($_SESSION['extra_settings']['booster_seat'])) {
            if ($_SESSION['extra_settings']['booster_seat'] == 0) {
                $charge = 0;
                $price_cell = '<span class="free">Free</span>';
            } else {
                $charge = charge($_SESSION['extra_settings']['booster_seat'], $_SESSION['rental_days']);
                $price_cell = '<span class="paid">' . $_SESSION['currency_code'] . " " . $charge . ' for ' . $_SESSION['rental_days'] . ' days </span>';
            }
            $html = '<tr>
                        <td>
                            <img src="https://pingouincar.atracio.com:443/uploads/images/rentalOptions/78eafc68-ab0b-4070-bf04-72cf81d0c35c.png" alt="Equipment" class="extra-image" >
                            <div class="extra-name">
                                BOOSTER SEAT
                            </div>
                            <div class="btn-tooltip">
                                <button class="pull-right btn btn-link tooltip-info" data-toggle="tooltip" data-placement="bottom" data-trigger="click" title="" data-original-title="Booster seat suitable for Children: 4 years to 6 years (18 kg - 38 kg)">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="text-center" width="10%">
                            <select id="booster_seat" class="extra_select" data-equipment-id="Booster Seat" data-charge="' . $charge . '">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            </select>
                        </td>
                        <td width="25%" class="price-cell">' . $price_cell . '</td>
                        <td width="5%" id="charged"></td>
                    </tr>';
        } else {
            $html = '';
        }
        return $html;

    }
    public function child_seat()
    {
        if (isset($_SESSION['extra_settings']['child_seat'])) {
            if ($_SESSION['extra_settings']['child_seat'] == 0) {
                $charge = 0;
                $price_cell = '<span class="free">Free</span>';
            } else {
                $charge = charge($_SESSION['extra_settings']['child_seat'], $_SESSION['rental_days']);
                $price_cell = '<span class="paid">' . $_SESSION['currency_code'] . " " . $charge . ' for ' . $_SESSION['rental_days'] . ' days </span>';
            }
            $html = '<tr>
                        <td>
                            <img src="https://pingouincar.atracio.com:443/uploads/images/rentalOptions/d27cc15f-60d1-402a-92c4-f446615e8ed9.png" alt="Equipment" class="extra-image" >
                            <div class="extra-name">
                                CHILD SEAT
                            </div>
                            <div class="btn-tooltip">
                                <button class="pull-right btn btn-link tooltip-info" data-toggle="tooltip" data-placement="bottom" data-trigger="click" title="" data-original-title="Booster seat suitable for Children: 4 years to 6 years (18 kg - 38 kg)">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="text-center" width="10%">
                            <select id="child_seat" class="extra_select" data-equipment-id="Child Seat" data-charge="' . $charge . '">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            </select>
                        </td>
                        <td width="25%" class="price-cell">' . $price_cell . '</td>
                        <td width="5%" id="charged"></td>
                    </tr>';
        } else {
            $html = '';
        }
        return $html;

    }
    public function map_of_mauritius()
    {
        if (isset($_SESSION['extra_settings']['map_of_mauritius'])) {
            if ($_SESSION['extra_settings']['map_of_mauritius'] == 0) {
                $charge = 0;
                $price_cell = '<span class="free">Free</span>';
            } else {
                $charge = charge($_SESSION['extra_settings']['map_of_mauritius'], $_SESSION['rental_days']);
                $price_cell = '<span class="paid">' . $_SESSION['currency_code'] . " " . $charge . ' for ' . $_SESSION['rental_days'] . ' days </span>';
            }
            $html = '<tr>
                        <td>
                            <img src="https://pingouincar.atracio.com:443/uploads/images/rentalOptions/d27cc15f-60d1-402a-92c4-f446615e8ed9.png" alt="Equipment" class="extra-image" >
                            <div class="extra-name">
                                MAP OF MAURITIUS
                            </div>
                            <div class="btn-tooltip">
                                <button class="pull-right btn btn-link tooltip-info" data-toggle="tooltip" data-placement="bottom" data-trigger="click" title="" data-original-title="Road Map">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="text-center" width="10%">
                            <select id="map_of_mauritius" class="extra_select" data-equipment-id="Map Of Mauritius" data-charge="' . $charge . '">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            </select>
                        </td>
                        <td width="25%" class="price-cell">' . $price_cell . '</td>
                        <td width="5%" id="charged"></td>
                    </tr>';
        } else {
            $html = '';
        }
        return $html;

    }
    public function mobile_gps()
    {
        if (isset($_SESSION['extra_settings']['mobile_gps'])) {
            if ($_SESSION['extra_settings']['mobile_gps'] == 0) {
                $charge = 0;
                $price_cell = '<span class="free">Free</span>';
            } else {
                $charge = charge($_SESSION['extra_settings']['mobile_gps'], $_SESSION['rental_days']);
                $price_cell = '<span class="paid">' . $_SESSION['currency_code'] . " " . $charge . ' for ' . $_SESSION['rental_days'] . ' days </span>';
            }
            $html = '<tr>
                        <td>
                            <img src="https://pingouincar.atracio.com:443/uploads/images/rentalOptions/66dacd49-0c57-4214-ab47-414ddc8a84e8.png" alt="Equipment" class="extra-image" >
                            <div class="extra-name">
                                MOBILE GPS
                            </div>
                            <div class="btn-tooltip">
                                <button class="pull-right btn btn-link tooltip-info" data-toggle="tooltip" data-placement="bottom" data-trigger="click" title="" data-original-title="Road Map">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="text-center" width="10%">
                            <select id="mobile_gps" class="extra_select" data-equipment-id="Mobile GPS" data-charge="' . $charge . '">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            </select>
                        </td>
                        <td width="25%" class="price-cell">' . $price_cell . '</td>
                        <td width="5%" id="charged"></td>
                    </tr>';
        } else {
            $html = '';
        }
        return $html;

    }
    public function premium_assist()
    {
        if (isset($_SESSION['extra_settings']['premium_assist'])) {
            if ($_SESSION['extra_settings']['premium_assist'] == 0) {
                $charge = 0;
                $price_cell = '<span class="free">Free</span>';
            } else {
                $charge = charge($_SESSION['extra_settings']['premium_assist'], $_SESSION['rental_days']);
                $price_cell = '<span class="paid">' . $_SESSION['currency_code'] . " " . $charge . ' for ' . $_SESSION['rental_days'] . ' days </span>';
            }
            $html = '<tr>
                        <td>
                            <img src="https://pingouincar.atracio.com:443/uploads/images/rentalOptions/2fdadea7-66e9-418d-91a8-9dd82528e9e8.png" alt="Equipment" class="extra-image" >
                            <div class="extra-name">
                                PREMIUM ROADSIDE ASSISTANCE
                            </div>
                            <div class="btn-tooltip">
                                <button class="pull-right btn btn-link tooltip-info" data-toggle="tooltip" data-placement="bottom" data-trigger="click" title="" data-original-title="Premium roadside assistance gives you a quicker service during a breakdown including towing, changing a flat tyre, or jumpstarting a battery.">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="text-center" width="10%">
                            <select id="premium_assist" class="extra_select" data-equipment-id="Premium Roadside Assistance" data-charge="' . $charge . '">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            </select>
                        </td>
                        <td width="25%" class="price-cell">' . $price_cell . '</td>
                        <td width="5%" id="charged"></td>
                    </tr>';
        } else {
            $html = '';
        }
        return $html;

    }
    public function sim_card()
    {
        if (isset($_SESSION['extra_settings']['sim_card'])) {
            if ($_SESSION['extra_settings']['sim_card'] == 0) {
                $charge = 0;
                $price_cell = '<span class="free">Free</span>';
            } else {
                $charge = charge($_SESSION['extra_settings']['sim_card'], $_SESSION['rental_days']);
                $price_cell = '<span class="paid">' . $_SESSION['currency_code'] . " " . $charge . ' for ' . $_SESSION['rental_days'] . ' days </span>';
            }
            $html = '<tr>
                        <td>
                            <img src="https://pingouincar.atracio.com:443/uploads/images/rentalOptions/300b6d33-04ef-490e-833c-ca1bb7390e26.png" alt="Equipment" class="extra-image" >
                            <div class="extra-name">
                                SIM CARD
                            </div>
                            <div class="btn-tooltip">
                                <button class="pull-right btn btn-link tooltip-info" data-toggle="tooltip" data-placement="bottom" data-trigger="click" title="" data-original-title="The Prepaid SIM card consists of the following - Rs. 87 airtime - 500 MB Data - 250 local SMS - Free and Unlimited Facebook">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="text-center" width="10%">
                            <select id="sim_card" class="extra_select" data-equipment-id="Sim Card" data-charge="' . $charge . '">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            </select>
                        </td>
                        <td width="25%" class="price-cell">' . $price_cell . '</td>
                        <td width="5%" id="charged"></td>
                    </tr>';
        } else {
            $html = '';
        }
        return $html;

    }
    public function theft_protection()
    {
        if (isset($_SESSION['extra_settings']['theft_protection'])) {
            if ($_SESSION['extra_settings']['theft_protection'] == 0) {
                $charge = 0;
                $price_cell = '<span class="free">Free</span>';
            } else {
                $charge = charge($_SESSION['extra_settings']['theft_protection'], $_SESSION['rental_days']);
                $price_cell = '<span class="paid">' . $_SESSION['currency_code'] . " " . $charge . ' for ' . $_SESSION['rental_days'] . ' days </span>';
            }
            $html = '<tr>
                        <td>
                            <img src="https://pingouincar.atracio.com:443/uploads/images/rentalOptions/a7761f74-3fca-42c7-b021-02f1b2812ac2.png" alt="Equipment" class="extra-image" >
                            <div class="extra-name">
                                THEFT PROTECTION
                            </div>
                            <div class="btn-tooltip">
                                <button class="pull-right btn btn-link tooltip-info" data-toggle="tooltip" data-placement="bottom" data-trigger="click" title="" data-original-title="If the car is stolen, all you’ll pay is the theft protection fee; not the full selling price of the car.">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="text-center" width="10%">
                            <select id="theft_protection" class="extra_select" data-equipment-id="Theft Protection" data-charge="' . $charge . '">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            </select>
                        </td>
                        <td width="25%" class="price-cell">' . $price_cell . '</td>
                        <td width="5%" id="charged"></td>
                    </tr>';
        } else {
            $html = '';
        }
        return $html;

    }
    public function wifi()
    {
        if (isset($_SESSION['extra_settings']['wifi'])) {
            if ($_SESSION['extra_settings']['wifi'] == 0) {
                $charge = 0;
                $price_cell = '<span class="free">Free</span>';
            } else {
                $charge = charge($_SESSION['extra_settings']['wifi'], $_SESSION['rental_days']);
                $price_cell = '<span class="paid">' . $_SESSION['currency_code'] . " " . $charge . ' for ' . $_SESSION['rental_days'] . ' days </span>';
            }
            $html = '<tr>
                        <td>
                            <img src="https://pingouincar.atracio.com:443/uploads/images/rentalOptions/fe4a6382-475d-443b-9505-f1a3f9b315f2.png" alt="Equipment" class="extra-image" >
                            <div class="extra-name">
                                WIFI HOTSPOT
                            </div>
                            <div class="btn-tooltip">
                                <button class="pull-right btn btn-link tooltip-info" data-toggle="tooltip" data-placement="bottom" data-trigger="click" title="" data-original-title="Road Map">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="text-center" width="10%">
                            <select id="wifi" class="extra_select" data-equipment-id="Wifi" data-charge="' . $charge . '">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            </select>
                        </td>
                        <td width="25%" class="price-cell">' . $price_cell . '</td>
                        <td width="5%" id="charged"></td>
                    </tr>';
        } else {
            $html = '';
        }
        return $html;

    }
    public function senior_driver()
    {
        if (isset($_SESSION['extra_settings']['senior_driver'])) {
            if ($_SESSION['extra_settings']['senior_driver'] == 0) {
                $charge = 0;
                $price_cell = '<span class="free">Free</span>';
            } else {
                $charge = charge($_SESSION['extra_settings']['senior_driver'], $_SESSION['rental_days']);
                $price_cell = '<span class="paid">' . $_SESSION['currency_code'] . " " . $charge . ' for ' . $_SESSION['rental_days'] . ' days </span>';
            }
            $html = '<tr>
                        <td>
                            <img src="https://pingouincar.atracio.com:443/uploads/images/rentalOptions/08203c64-e533-4fda-9290-02f0730413a2.png" alt="Equipment" class="extra-image" >
                            <div class="extra-name">
                                YOUNG & SENIOR DRIVER
                            </div>
                            <div class="btn-tooltip">
                                <button class="pull-right btn btn-link tooltip-info" data-toggle="tooltip" data-placement="bottom" data-trigger="click" title="" data-original-title="Road Map">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="text-center" width="10%" width="10%">
                            <select id="senior_driver" class="extra_select" data-equipment-id="Young & Senior Driver" data-charge="' . $charge . '">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            </select>
                        </td>
                        <td width="25%" class="price-cell">' . $price_cell . '</td>
                        <td width="5%" id="charged"></td>
                    </tr>';
        } else {
            $html = '';
        }
        return $html;

    }
}

$extras = new Extras();
