<?php
class FleetSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_menu_page(
            'Fleet Settings',
            'Fleet Settings',
            'manage_options',
            'fleet-setting-admin',
            array($this, 'create_admin_page')
        );

        add_submenu_page('fleet-setting-admin', "Sub Page Title", "Sub Menu Title", "manage_options", "sub_menu_slug", "sub_menu_slug_callback");

    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option('fleet_option_name');
        ?>
        <div class="wrap">
            <h1>Fleet Settings</h1>
            <form method="post" action="options.php">
            <?php
// This prints out all hidden setting fields
        settings_fields('fleet_option_group');
        do_settings_sections('fleet-setting-admin');
        submit_button();
        ?>
            </form>
        </div>
        <?php
}

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'fleet_option_group', // Option group
            'fleet_option_name', // Option name
            array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
            'fleet_api_settings', // ID
            'API Settings', // Title
            array($this, 'print_api_section_info'), // Callback
            'fleet-setting-admin' // Page
        );

        add_settings_section(
            'fleet_payment_settings', // ID
            'Payment Gateway Settings', // Title
            array($this, 'print_payment_section_info'), // Callback
            'fleet-setting-admin' // Page
        );

        add_settings_section(
            'fleet_page_settings', // ID
            'Fleet Pages', // Title
            array($this, 'print_page_section_info'), // Callback
            'fleet-setting-admin' // Page
        );

        add_settings_field(
            'api_token', // ID
            'API Token', // Title
            array($this, 'api_token_callback'), // Callback
            'fleet-setting-admin', // Page
            'fleet_api_settings' // Section ID
        );

        add_settings_field(
            'api_base_url',
            'API Base URL',
            array($this, 'api_base_url_callback'),
            'fleet-setting-admin',
            'fleet_api_settings'
        );

        add_settings_field(
            'mobipaid_api_key', // ID
            'API Key', // Title
            array($this, 'mobipaid_api_key_callback'), // Callback
            'fleet-setting-admin', // Page
            'fleet_payment_settings' // Section ID
        );

        add_settings_field(
            'search_form_page', // ID
            'Search Form Page', // Title
            array($this, 'search_form_page_callback'), // Callback
            'fleet-setting-admin', // Page
            'fleet_page_settings' // Section ID
        );

        add_settings_field(
            'vehicle_selection_page', // ID
            'Vehicle Selection Page', // Title
            array($this, 'vehicle_selection_page_callback'), // Callback
            'fleet-setting-admin', // Page
            'fleet_page_settings' // Section ID
        );

        add_settings_field(
            'extras_page', // ID
            'Extras Page', // Title
            array($this, 'extras_page_callback'), // Callback
            'fleet-setting-admin', // Page
            'fleet_page_settings' // Section ID
        );

        add_settings_field(
            'payment_page', // ID
            'Payment Page', // Title
            array($this, 'payment_page_callback'), // Callback
            'fleet-setting-admin', // Page
            'fleet_page_settings' // Section ID
        );

    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input)
    {
        $new_input = array();
        if (isset($input['api_token'])) {
            $new_input['api_token'] = absint($input['api_token']);
        }

        if (isset($input['api_base_url'])) {
            $new_input['api_base_url'] = sanitize_text_field($input['api_base_url']);
        }

        if (isset($input['mobipaid_api_key'])) {
            $new_input['mobipaid_api_key'] = sanitize_text_field($input['mobipaid_api_key']);
        }

        if (isset($input['search_form_page'])) {
            $new_input['search_form_page'] = sanitize_text_field($input['search_form_page']);
        }

        if (isset($input['vehicle_selection_page'])) {
            $new_input['vehicle_selection_page'] = sanitize_text_field($input['vehicle_selection_page']);
        }

        if (isset($input['extras_page'])) {
            $new_input['extras_page'] = sanitize_text_field($input['extras_page']);
        }

        if (isset($input['payment_page'])) {
            $new_input['payment_page'] = sanitize_text_field($input['payment_page']);
        }

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_api_section_info()
    {
        print 'Enter API settings below:';
    }

    public function print_payment_section_info()
    {
        print 'Enter mobipaid payment gateway settings below:';

    }

    public function print_page_section_info(){
        print 'Select Fleet pages where shortcode is pasted:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function api_token_callback()
    {
        printf(
            '<input type="text" id="api_token" name="fleet_option_name[api_token]" value="%s" />',
            isset($this->options['api_token']) ? esc_attr($this->options['api_token']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function api_base_url_callback()
    {
        printf(
            '<input type="text" id="api_base_url" name="fleet_option_name[api_base_url]" value="%s" />',
            isset($this->options['api_base_url']) ? esc_attr($this->options['api_base_url']) : ''
        );
    }

    public function mobipaid_api_key_callback()
    {
        printf(
            '<input type="text" id="mobipaid_api_key" name="fleet_option_name[mobipaid_api_key]" value="%s" />',
            isset($this->options['mobipaid_api_key']) ? esc_attr($this->options['mobipaid_api_key']) : ''
        );

    }

    public function search_form_page_callback()
    {
       /* printf(
            '<input type="text" id="search_form_page" name="fleet_option_name[search_form_page]" value="%s" />',
            isset($this->options['search_form_page']) ? esc_attr($this->options['search_form_page']) : ''
        );*/
         $html = '<select id="search_form_page" name="fleet_option_name[search_form_page]">';
        $html .= '<option selected="selected" disabled="disabled" value="">'.esc_attr( __( 'Select page' ) ).'</option>'; 
    
        $selected_page = $this->options['search_form_page'];
        $pages = get_pages(); 
        foreach ( $pages as $page ) {
            $option = '<option value="' . $page->ID . '" ';
            $option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
            $option .= '>';
            $option .= $page->post_title;
            $option .= '</option>';
            $html .= $option;
        }
   
        $html .= '</select>';
        $html .= '<p class="fleet-inst">Create a page with \'[pickup_drop_form]\' shortcode and select that page here.</p>';
        echo $html;
    }

    public function vehicle_selection_page_callback()
    {
       /* printf(
            '<input type="text" id="vehicle_selection_page" name="fleet_option_name[vehicle_selection_page]" value="%s" />',
            isset($this->options['vehicle_selection_page']) ? esc_attr($this->options['vehicle_selection_page']) : ''
        );*/
        $html = '<select id="vehicle_selection_page" name="fleet_option_name[vehicle_selection_page]">';
        $html .= '<option selected="selected" disabled="disabled" value="">'.esc_attr( __( 'Select page' ) ).'</option>'; 
    
        $selected_page = $this->options['vehicle_selection_page'];
        $pages = get_pages(); 
        foreach ( $pages as $page ) {
            $option = '<option value="' . $page->ID . '" ';
            $option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
            $option .= '>';
            $option .= $page->post_title;
            $option .= '</option>';
            $html .= $option;
        }
   
        $html .= '</select>';
        $html .= '<p class="fleet-inst">Create a page with \'[vehicle_selection]\' shortcode and select that page here.</p>';
        echo $html;
    }

    public function extras_page_callback()
    {
       /* printf(
            '<input type="text" id="extras_page" name="fleet_option_name[extras_page]" value="%s" />',
            isset($this->options['extras_page']) ? esc_attr($this->options['extras_page']) : ''
        );*/
        $html = '<select id="extras_page" name="fleet_option_name[extras_page]">';
        $html .= '<option selected="selected" disabled="disabled" value="">'.esc_attr( __( 'Select page' ) ).'</option>'; 
    
        $selected_page = $this->options['extras_page'];
        $pages = get_pages(); 
        foreach ( $pages as $page ) {
            $option = '<option value="' . $page->ID . '" ';
            $option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
            $option .= '>';
            $option .= $page->post_title;
            $option .= '</option>';
            $html .= $option;
        }
   
        $html .= '</select>';
        $html .= '<p class="fleet-inst">Create a page with \'[extras]\' shortcode and select that page here.</p>';
        echo $html;
    }

    public function payment_page_callback()
    {
       /* printf(
            '<input type="text" id="payment_page" name="fleet_option_name[payment_page]" value="%s" />',
            isset($this->options['payment_page']) ? esc_attr($this->options['payment_page']) : ''
        );*/
         $html = '<select id="payment_page" name="fleet_option_name[payment_page]">';
        $html .= '<option selected="selected" disabled="disabled" value="">'.esc_attr( __( 'Select page' ) ).'</option>'; 
    
        $selected_page = $this->options['payment_page'];
        $pages = get_pages(); 
        foreach ( $pages as $page ) {
            $option = '<option value="' . $page->ID . '" ';
            $option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
            $option .= '>';
            $option .= $page->post_title;
            $option .= '</option>';
            $html .= $option;
        }
   
        $html .= '</select>';
        $html .= '<p class="fleet-inst">Create a page with \'[fleet_payment]\' shortcode and select that page here.</p>';
        echo $html;
    }
}

if (is_admin()) {
    $fleet_settings_page = new FleetSettingsPage();
}
