=== Car Rental System (Native WordPress Plugin) ===
Author: KestutisIT
Purchase link: https://codecanyon.net/item/car-rental-system-native-wordpress-plugin/11758680
Website link: https://nativerental.com
Tags: bike rental, booking, bus rental, car booking system, car rental, car rental script, car reservation system, Fleet Management, rent a car, rental, truck rental, WordPress car, WordPress car rental, wp car manager, wp car rental system
Requires at least: 4.6
Tested up to: 5.2
Requires PHP: 5.6
Stable tag: trunk
License: CodeCanyon Split License
License URI: https://codecanyon.net/licenses

Native WordPress plugin for renting cars online

== Description ==

It’s a high quality, native and responsive WordPress plugin to rent a car, created by experienced Silicon Valley engineers. 100% of it’s code is written by using native WordPress functions, so it much faster and secure than other similar plugins. Also – we made it compatible with WordPress Multisite, WPML & Multi-language setup with native support for WordPress date, email & time settings.