<?php
defined( 'ABSPATH' ) or die( 'No script kiddies, please!' );
?>
<h1>
    <span>Tracking Settings</span>
</h1>
<form name="tracking_settings_form" action="<?=esc_url($trackingSettingsTabFormAction);?>" method="POST" id="tracking_settings_form">
    <table cellpadding="5" cellspacing="2" border="0" width="100%">
        <tr>
            <td width="20%"><strong>UA Event Tracking:</strong></td>
            <td width="80%">
                <select name="conf_universal_analytics_events_tracking" id="conf_universal_analytics_events_tracking">
                    <?=$arrTrackingSettings['select_universal_analytics_events_tracking'];?>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>UA Enhanced Ecommerce:</strong></td>
            <td>
                <select name="conf_universal_analytics_enhanced_ecommerce" id="conf_universal_analytics_enhanced_ecommerce">
                    <?=$arrTrackingSettings['select_universal_analytics_enhanced_ecommerce'];?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <br />
                <input type="submit" value="Update tracking settings" name="update_tracking_settings" style="cursor:pointer;"/>
            </td>
        </tr>
    </table>
</form>
<p>Please keep in mind that:</p>
<ol>
    <li>If you want to enable Universal Analytics event tracking or/and Enhanced Ecommerce,
        make sure you have standard Universal Analytics tracking code added to your site header
        or just after opening of &lt;body&gt; tag. Most themes has the header scripts part.<br />
        Default universal analytics tracking code looks like this:<br />
        <div style="font-family: 'Courier New', Courier, mono; font-size: 10px;color:#196601;line-height: 1.4em;">
            <pre>
&lt;script type=&quot;text/javascript&quot;&gt;
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-<strong>YOUR-TRACKING-CODE</strong>', 'auto');
    ga('send', 'pageview');
&lt;/script&gt;</pre>
        </div>
        Keep in mind that default universal analytics tracking code SHOULD NOT (!) have the following line:<br />
        <span style="font-family: 'Courier New', Courier, mono; font-size: 10px;color:#196601;">ga('require', 'ec');</span><br />
        The line above will be added automatically whenever Enhanced Analytics will be called.<br />
        Universal Analytics Event tracking will fire these onClick actions for new reservation:<br />
        <div style="font-family: 'Courier New', Courier, mono; font-size: 10px; color:#196601;line-height: 1.4em;">
            ga('send', 'event', 'Car Rental', 'Click', '1. Search for all cars');<br />
            ga('send', 'event', 'Car Rental', 'Click', '1. Search for single car');<br />
            ga('send', 'event', 'Car Rental', 'Click', '3. Continue to extras');<br />
            ga('send', 'event', 'Car Rental', 'Click', '4. Continue to summary');<br />
            ga('send', 'event', 'Car Rental', 'Click', '5. Confirm reservation');
        </div>
    </li>
    <li>With Enhanced Ecommerce we can track only those car models, which has &quot;Stock Keeping Unit&quot; (SKU) set.</li>
    <li>With Enhanced Ecommerce we can track only those extras, which has &quot;Stock Keeping Unit&quot; (SKU) set.</li>
</ol>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery("#tracking_settings_form").validate();
});
</script>