<?php
defined( 'ABSPATH' ) or die( 'No script kiddies, please!' );
?>
<script type="text/javascript">
// Add Enhanced commerce library code
ga('require', 'ec');
// Set currency to local currency code. The local currency must be specified in the ISO 4217 standard.
ga('set', 'currencyCode', '<?=esc_js($settings['conf_currency_code']);?>');

// Note: Do not translate values and do not add partner info to track well inter-language purchases
// The following code measures the impression (view) of a product in a list of search results:
ga('ec:addImpression', {            // Provide product details in an impressionFieldObject.
    'id': '<?=$itemModel['print_item_model_sku'];?>',                                     // Product ID (string).
    'name': '<?=($itemModel['print_manufacturer_name'].' '.$itemModel['print_item_model_name']);?>', // Product name (string).
    'category': '<?=$itemModel['print_class_name'];?>',                        // Product category (string).
    'brand': '<?=$itemModel['print_manufacturer_name'];?>',                        // Product brand (string).
    'list': 'View Details'                                                          // Product list (string).
});

// The product being viewed.
ga('ec:addProduct', {                 // Provide product details in an productFieldObject.
    'id': '<?=$itemModel['print_item_model_sku'];?>',                                     // Product ID (string).
    'name': '<?=($itemModel['print_manufacturer_name'].' '.$itemModel['print_item_model_name']);?>', // Product name (string).
    'category': '<?=$itemModel['print_class_name'];?>',                        // Product category (string).
    'brand': '<?=$itemModel['print_manufacturer_name'];?>'                        // Product brand (string).
});

ga('ec:setAction', 'detail');       // Detail action.

// Ecommerce data can only be sent with an existing hit, for example a pageview or event. If you use ecommerce commands
// but do not send any hits, or the hit is sent before the ecommerce command then the ecommerce data will not be sent.
// Note: we can't use 'pageview' hit here, because it is already sent in site headers and we don't want to count it twice
// But we still want to process these impressions, so we call non-interactive pageview
// Note 2: Do not translate events to track well inter-language events
ga('send', 'event', '<?=esc_js($extName);?> Enhanced Ecommerce', 'Add Product Detail with Impression', {'nonInteraction': true});
</script>