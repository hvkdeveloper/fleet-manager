<?php
/**
 * The style "default" of the Widget "Cars Search"
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.25
 */

$trx_addons_args = get_query_var('trx_addons_args_widget_cars_rent_search');
extract($trx_addons_args);

// Before widget (defined by themes)
trx_addons_show_layout($before_widget);
			
// Widget title if one was input (before and after defined by themes)
trx_addons_show_layout($title, $before_title, $after_title);
	
// Widget body
$form_style = $trx_addons_args['style'] = empty($trx_addons_args['style']) || trx_addons_is_inherit($trx_addons_args['style']) 
			? trx_addons_get_option('input_hover') 
			: $trx_addons_args['style'];
?><div
	<?php if (!empty($trx_addons_args['id'])) echo ' id="'.esc_attr($trx_addons_args['id']).'"'; ?>
	class="sc_form cars_rent_search cars_rent_search_<?php 
		echo esc_attr($trx_addons_args['type']);
		if (!empty($trx_addons_args['class'])) echo ' '.esc_attr($trx_addons_args['class']);
		if (!empty($trx_addons_args['align']) && !trx_addons_is_off($trx_addons_args['align'])) echo ' sc_align_'.esc_attr($trx_addons_args['align']);
		?>"<?php
	if (!empty($trx_addons_args['css'])) echo ' style="'.esc_attr($trx_addons_args['css']).'"'; 
?>>
	<form class="cars_rent_search_form sc_form_form sc_form_custom <?php if ($form_style != 'default') echo 'sc_input_hover_'.esc_attr($form_style); ?>" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post">

		<div class="<?php echo esc_attr(trx_addons_get_columns_wrap_class()) ?>">
			<div class="trx_addons_column-1_2"><?php
				// Pick up location
				trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
										'trx_addons_args_sc_form_field',
										array_merge($trx_addons_args, array(
													'labels'      => false,
													'field_name'  => 'pick_up_loc',
													'field_type'  => 'text',
													'field_req'   => true,
													'field_title' => __('Pick up location', 'trx_addons'),
													'field_value' => __('Manchester', 'trx_addons')
													))
									); ?>
			</div>
			<div class="trx_addons_column-1_2"><?php
				// Return location
				trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
										'trx_addons_args_sc_form_field',
										array_merge($trx_addons_args, array(
													'labels'      => false,
													'field_name'  => 'return_loc',
													'field_type'  => 'text',
													'field_req'   => true,
													'field_title' => __('Pick up location', 'trx_addons'),
													'field_value' => __('London', 'trx_addons')
													))
									); ?>
			</div>
			<div class="trx_addons_column-1_5"><?php			
				// Pick up date
				trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
										'trx_addons_args_sc_form_field',
										array_merge($trx_addons_args, array(
													'labels'      => false,
													'field_name'  => 'pick_up_date',
													'field_type'  => 'date',
													'field_req'   => true,
													'field_title' => __('Pick up date', 'trx_addons'),
													))
									); ?>
			</div>
			<div class="trx_addons_column-1_5"><?php
				// Pick up time
				trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
												'trx_addons_args_sc_form_field',
										array_merge($trx_addons_args, array(
													'labels'      => false,
													'field_name'  => 'pick_up_time',
													'field_type'  => 'text',
													'field_req'   => true,
													'field_title' => __('Pick up time', 'trx_addons'),
													'field_value' => '11:00am'
													))
									); ?>
			</div>
			<div class="trx_addons_column-1_5"><?php	
				// Return date
				trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
										'trx_addons_args_sc_form_field',
										array_merge($trx_addons_args, array(
													'labels'      => false,
													'field_name'  => 'return_date',
													'field_type'  => 'date',
													'field_req'   => true,
													'field_title' => __('Return date', 'trx_addons'),
													))
									); ?>
			</div>
			<div class="trx_addons_column-1_5"><?php	
				// Return time
				trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
										'trx_addons_args_sc_form_field',
										array_merge($trx_addons_args, array(
													'labels'      => false,
													'field_name'  => 'return_time',
													'field_type'  => 'text',
													'field_req'   => true,
													'field_title' => __('Return time', 'trx_addons'),
													'field_value' => '11:00am'
													))
									); ?>
			</div>
			<div class="trx_addons_column-1_5">
				<button class="cars_rent_search_button"><?php esc_attr_e('Search', 'trx_addons'); ?></button>
			</div>			
		</div>
		<div class="trx_addons_message_box sc_form_result"></div>
	</form>
	<div class="sc_cars_rent_search_results"></div>
</div><!-- /.sc_form -->
<?php

// After widget (defined by themes)
trx_addons_show_layout($after_widget);