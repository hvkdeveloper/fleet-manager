<?php
/**
 * Plugin support: WPML
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.38
 */

// Check if plugin installed and activated
if ( !function_exists( 'trx_addons_exists_nativerentalsystem' ) ) {
	function trx_addons_exists_nativerentalsystem() {
		return defined('TRX_ADDONS_VERSION');
	}
}


// One-click import support
//------------------------------------------------------------------------

// Check plugin in the required plugins
if ( !function_exists( 'trx_addons_nativerentalsystem_importer_required_plugins' ) ) {
    if (is_admin()) add_filter( 'trx_addons_filter_importer_required_plugins',	'trx_addons_nativerentalsystem_importer_required_plugins', 10, 2 );
    function trx_addons_nativerentalsystem_importer_required_plugins($not_installed='', $list='') {
        if (strpos($list, 'NativeRentalSystem')!==false && !trx_addons_exists_nativerentalsystem() )
            $not_installed .= '<br>' . esc_html__('Native Rental System', 'trx_addons');
        return $not_installed;
    }
}

// Set plugin's specific importer options
if ( !function_exists( 'trx_addons_nativerentalsystem_importer_set_options' ) ) {
    if (is_admin()) add_filter( 'trx_addons_filter_importer_options',	'trx_addons_nativerentalsystem_importer_set_options' );
    function trx_addons_nativerentalsystem_importer_set_options($options=array()) {
        if ( trx_addons_exists_nativerentalsystem() && in_array('NativeRentalSystem', $options['required_plugins']) ) {
            $options['additional_options'][] = 'wpmtst_%';
        }
        if (is_array($options['files']) && count($options['files']) > 0) {
            foreach ($options['files'] as $k => $v) {
                $options['files'][$k]['file_with_NativeRentalSystem'] = str_replace('name.ext', 'NativeRentalSystem.txt', $v['file_with_']);
            }
        }
        return $options;
    }
}

// Add checkbox to the one-click importer
if ( !function_exists( 'trx_addons_nativerentalsystem_importer_show_params' ) ) {
    if (is_admin()) add_action( 'trx_addons_action_importer_params',	'trx_addons_nativerentalsystem_importer_show_params', 10, 1 );
    function trx_addons_nativerentalsystem_importer_show_params($importer) {
        if ( trx_addons_exists_nativerentalsystem() && in_array('NativeRentalSystem', $importer->options['required_plugins']) ) {
            $importer->show_importer_params(array(
                'slug' => 'NativeRentalSystem',
                'title' => esc_html__('Import Native Rental System', 'trx_addons'),
                'part' => 0
            ));
        }
    }
}

// Import posts
if ( !function_exists( 'trx_addons_nativerentalsystem_importer_import' ) ) {
    if (is_admin()) add_action( 'trx_addons_action_importer_import',	'trx_addons_nativerentalsystem_importer_import', 10, 2 );
    function trx_addons_nativerentalsystem_importer_import($importer, $action) {
        if ( trx_addons_exists_nativerentalsystem() && in_array('NativeRentalSystem', $importer->options['required_plugins']) ) {
            if ( $action == 'import_NativeRentalSystem' ) {
                $importer->response['start_from_id'] = 0;
                $importer->import_dump('NativeRentalSystem', esc_html__('Native Rental System data', 'trx_addons'));
            }
        }
    }
}

// Display import progress
if ( !function_exists( 'trx_addons_nativerentalsystem_importer_import_fields' ) ) {
    if (is_admin()) add_action( 'trx_addons_action_importer_import_fields',	'trx_addons_nativerentalsystem_importer_import_fields', 10, 1 );
    function trx_addons_nativerentalsystem_importer_import_fields($importer) {
        if ( trx_addons_exists_nativerentalsystem() && in_array('NativeRentalSystem', $importer->options['required_plugins']) ) {
            $importer->show_importer_fields(array(
                    'slug'=>'NativeRentalSystem',
                    'title' => esc_html__('Native Rental System data', 'trx_addons')
                )
            );
        }
    }
}

// Export posts
if ( !function_exists( 'trx_addons_nativerentalsystem_importer_export' ) ) {
    if (is_admin()) add_action( 'trx_addons_action_importer_export',	'trx_addons_nativerentalsystem_importer_export', 10, 1 );
    function trx_addons_nativerentalsystem_importer_export($importer) {
        if ( trx_addons_exists_nativerentalsystem() && in_array('NativeRentalSystem', $importer->options['required_plugins']) ) {
            trx_addons_fpc($importer->export_file_dir('NativeRentalSystem.txt'), serialize( array(
                    "car_rental_benefits" => $importer->export_dump("car_rental_benefits"),
                    "car_rental_body_types" => $importer->export_dump("car_rental_body_types"),
                    "car_rental_bookings" => $importer->export_dump("car_rental_bookings"),
                    "car_rental_booking_options" => $importer->export_dump("car_rental_booking_options"),
                    "car_rental_closed_dates" => $importer->export_dump("car_rental_closed_dates"),
                    "car_rental_customers" => $importer->export_dump("car_rental_customers"),
                    "car_rental_discounts" => $importer->export_dump("car_rental_discounts"),
                    "car_rental_distances" => $importer->export_dump("car_rental_distances"),
                    "car_rental_emails" => $importer->export_dump("car_rental_emails"),
                    "car_rental_extras" => $importer->export_dump("car_rental_extras"),
                    "car_rental_features" => $importer->export_dump("car_rental_features"),
                    "car_rental_fuel_types" => $importer->export_dump("car_rental_fuel_types"),
                    "car_rental_invoices" => $importer->export_dump("car_rental_invoices"),
                    "car_rental_items" => $importer->export_dump("car_rental_items"),
                    "car_rental_item_features" => $importer->export_dump("car_rental_item_features"),
                    "car_rental_item_locations" => $importer->export_dump("car_rental_item_locations"),
                    "car_rental_locations" => $importer->export_dump("car_rental_locations"),
                    "car_rental_logs" => $importer->export_dump("car_rental_logs"),
                    "car_rental_manufacturers" => $importer->export_dump("car_rental_manufacturers"),
                    "car_rental_options" => $importer->export_dump("car_rental_options"),
                    "car_rental_payment_methods" => $importer->export_dump("car_rental_payment_methods"),
                    "car_rental_prepayments" => $importer->export_dump("car_rental_prepayments"),
                    "car_rental_price_groups" => $importer->export_dump("car_rental_price_groups"),
                    "car_rental_price_plans" => $importer->export_dump("car_rental_price_plans"),
                    "car_rental_settings" => $importer->export_dump("car_rental_settings"),
                    "car_rental_taxes" => $importer->export_dump("car_rental_taxes"),
                    "car_rental_transmission_types" => $importer->export_dump("car_rental_transmission_types"),

                ) )
            );
        }
    }
}

// Display exported data in the fields
if ( !function_exists( 'trx_addons_nativerentalsystem_importer_export_fields' ) ) {
    if (is_admin()) add_action( 'trx_addons_action_importer_export_fields',	'trx_addons_nativerentalsystem_importer_export_fields', 10, 1 );
    function trx_addons_nativerentalsystem_importer_export_fields($importer) {
        if ( trx_addons_exists_nativerentalsystem() && in_array('NativeRentalSystem', $importer->options['required_plugins']) ) {
            $importer->show_exporter_fields(array(
                    'slug'	=> 'NativeRentalSystem',
                    'title' => esc_html__('Native Rental System data', 'trx_addons')
                )
            );
        }
    }
}




// OCDI support
//------------------------------------------------------------------------

// Set plugin's specific importer options
if ( !function_exists( 'trx_addons_ocdi_nativerentalsystem_set_options' ) ) {
    if (is_admin()) add_filter( 'trx_addons_filter_ocdi_options', 'trx_addons_ocdi_nativerentalsystem_set_options' );
    function trx_addons_ocdi_nativerentalsystem_set_options($ocdi_options){
        $ocdi_options['import_NativeRentalSystem_file_url'] = 'NativeRentalSystem.txt';
        return $ocdi_options;
    }
}

// Export plugin's data
if ( !function_exists( 'trx_addons_ocdi_nativerentalsystem_export' ) ) {
    if (is_admin()) add_filter( 'trx_addons_filter_ocdi_export_files', 'trx_addons_ocdi_nativerentalsystem_export' );
    function trx_addons_ocdi_nativerentalsystem_export($output){
        $list = array();
        if (trx_addons_exists_nativerentalsystem() && in_array('NativeRentalSystem', trx_addons_ocdi_options('required_plugins'))) {
            // Get plugin data from database
            $options = array('wpmtst_%');
            $list = trx_addons_ocdi_export_options($options, $list);

            // Save as file
            $file_path = TRX_ADDONS_PLUGIN_OCDI . "export/NativeRentalSystem.txt";
            trx_addons_fpc(trx_addons_get_file_dir($file_path), serialize($list));

            // Return file path
            $output .= '<h4><a href="'. trx_addons_get_file_url($file_path).'" download>'.esc_html__('Native Rental System', 'trx_addons').'</a></h4>';
        }
        return $output;
    }
}

// Add plugin to import list
if ( !function_exists( 'trx_addons_ocdi_nativerentalsystem_import_field' ) ) {
    if (is_admin()) add_filter( 'trx_addons_filter_ocdi_import_fields', 'trx_addons_ocdi_nativerentalsystem_import_field' );
    function trx_addons_ocdi_nativerentalsystem_import_field($output){
        $list = array();
        if (trx_addons_exists_nativerentalsystem() && in_array('NativeRentalSystem', trx_addons_ocdi_options('required_plugins'))) {
            $output .= '<label><input type="checkbox" name="NativeRentalSystem" value="NativeRentalSystem">'. esc_html__( 'Native Rental System', 'trx_addons' ).'</label><br/>';
        }
        return $output;
    }
}

// Import plugin's data
if ( !function_exists( 'trx_addons_ocdi_nativerentalsystem_import' ) ) {
    if (is_admin()) add_action( 'trx_addons_action_ocdi_import_plugins', 'trx_addons_ocdi_nativerentalsystem_import', 10, 1 );
    function trx_addons_ocdi_nativerentalsystem_import( $import_plugins){
        if (trx_addons_exists_nativerentalsystem() && in_array('NativeRentalSystem', $import_plugins)) {
            trx_addons_ocdi_import_dump('NativeRentalSystem');
            echo esc_html__('Native Rental System import complete.', 'trx_addons') . "\r\n";
        }
    }
}
?>