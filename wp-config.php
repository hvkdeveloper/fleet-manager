<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define('FS_METHOD', 'direct');
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pinguin');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'G`%)nCVj95abVOOJy1TmS_bIZ/~ Rd|&<U; >`KT(*Dh%yCL.>2ubno+l! <%=jp');
define('SECURE_AUTH_KEY', '0M o;]{(MifZ7=OKW8k)x !%;=T0SsO6L`<aJ^JDTnM<1sS2WfoHl`.0}y=c-N+|');
define('LOGGED_IN_KEY', '&ni=<3h/#A+OJ+up*kb2Sy|l7(G=^)GeaI}D!P/k6Z6K5YQ77(0bjvd2WQ*E?Z`!');
define('NONCE_KEY', '5+Rtrty$v/ `kEH&z9Kn>EPs+Za&$Mf#iE$=B<|m7SDkOM8 *@9PXY&1H|3VCxbS');
define('AUTH_SALT', 'no<wEdd4?|*h,|A6AE4wx/4zew3P<gJqwz_3k%wqo6>#[*s~x3?>XisNtz cKj:!');
define('SECURE_AUTH_SALT', 'mag_;y,1-R0NGs_c|!{W #iadA*_r&$Nr<rjJHJ<jrY6}YDR8LV<G9/xm[qz2Bn/');
define('LOGGED_IN_SALT', '>QCZetW+K~]d[;ghH8#mX>]yJ,RADAU><gsZ)?yCab 9{?`pE}:=K{)$&?)18`/Z');
define('NONCE_SALT', ']Jr/O.jq~<g;$SAi`WQ,#N 1~#z]c5lkM*{A0#ld.Hl<<&:*;Eo]Hk7,X2Bbi?F_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
